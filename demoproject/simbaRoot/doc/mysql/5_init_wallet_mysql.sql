/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2018/6/28 8:48:50                            */
/*==============================================================*/


drop table if exists tradeAccount;

drop table if exists tradeChannelDetail;

drop table if exists tradeDetail;

drop table if exists tradeBalanceDetail;

drop table if exists tradePartyDetail;

drop table if exists tradeUser;

CREATE TABLE `tradeAccount` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tradeUserID` bigint(20) NOT NULL COMMENT '支付用户ID',
  `accountID` varchar(100) NOT NULL COMMENT '账号ID',
  `accountType` varchar(20) NOT NULL COMMENT '账户类型：部门资产账户/个人账户/渠道账号',
  `feeType` varchar(10) NOT NULL DEFAULT 'CNY' COMMENT '货币类型：人民币为CNY',
  `isAllowRecharge` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否允许充值：0不允许，1允许',
  `isAllowPay` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否允许支付：0不允许，1允许',
  `isActive` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否激活：0未激活，1激活，-1注销',
  `isFrozen` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否冻结：0冻结，1未冻结',
  `accountBalance` bigint(20) NOT NULL DEFAULT '0' COMMENT '账户当前余额',
  `availableBalance` bigint(20) NOT NULL DEFAULT '0' COMMENT '账户当前可用余额',
  `virtualBalance` bigint(20) NOT NULL DEFAULT '0' COMMENT '账户当前可用余额',
  `frozenBalance` bigint(20) NOT NULL DEFAULT '0' COMMENT '账户当前冻结余额',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `lastUpdateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_accountID` (`accountID`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COMMENT='支付账号';

CREATE TABLE `tradeBalanceDetail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tradeNo` bigint(20) DEFAULT NULL COMMENT '交易订单流水号',
  `balanceType` tinyint(4) DEFAULT NULL COMMENT '余额类型 1：真实充值余额 2：虚拟余额',
  `balanceAmount` bigint(20) DEFAULT '0' COMMENT '金额',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `AK_uk_detail` (`tradeNo`,`balanceType`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8;

CREATE TABLE `tradeChannelDetail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tradeAccountID` varchar(100) NOT NULL COMMENT '帐号ID',
  `channelID` bigint(20) DEFAULT NULL COMMENT '渠道ID',
  `orderCreateTime` datetime DEFAULT NULL COMMENT '渠道提交时间',
  `paymentTime` datetime DEFAULT NULL COMMENT '渠道支付时间',
  `orderNO` varchar(100) DEFAULT NULL COMMENT '渠道订单号',
  `openID` varchar(100) DEFAULT NULL COMMENT '用户的openID',
  `errorMsg` varchar(200) DEFAULT '' COMMENT '错误信息',
  `errorCode` varchar(20) DEFAULT '' COMMENT '错误代号',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `lastUpdateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1288 DEFAULT CHARSET=utf8mb4 COMMENT='交易的渠道信息';

CREATE TABLE `tradeDetail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tradeNO` bigint(20) NOT NULL COMMENT '交易流水号',
  `tradeType` varchar(30) NOT NULL COMMENT '记录交易类型：充值/消费',
  `tradeStatus` varchar(10) NOT NULL COMMENT '记录支付状态 SUCCESS/FAILED/FROZON',
  `orderNO` varchar(100) NOT NULL COMMENT '订单号',
  `orderName` varchar(100) NOT NULL COMMENT '订单名称',
  `orderDesc` varchar(200) DEFAULT '' COMMENT '订单描述',
  `orderAddress` varchar(200) DEFAULT '' COMMENT '订单地址',
  `feeType` varchar(10) NOT NULL DEFAULT 'CNY' COMMENT '货币类型',
  `originalAmount` bigint(20) NOT NULL DEFAULT '0' COMMENT '原始费用',
  `paymentAmount` bigint(20) NOT NULL DEFAULT '0' COMMENT '实际费用',
  `partyTradeUserID` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `counterpartyTradeUserID` bigint(20) DEFAULT NULL COMMENT '对手主题用户ID',
  `channelTradeUserID` bigint(20) DEFAULT NULL COMMENT '渠道用户ID',
  `tradePartyID` bigint(20) NOT NULL COMMENT '交易主体ID',
  `tradeCounterpartyID` bigint(20) NOT NULL COMMENT '交易对手ID',
  `tradeChannelID` bigint(20) DEFAULT NULL COMMENT '交易渠道ID',
  `tradeCreateTime` datetime NOT NULL COMMENT '请求支付时间',
  `tradePaymentTime` datetime NOT NULL COMMENT '支付创建时间',
  `tradePaymentDate` date DEFAULT NULL COMMENT '支付创建日期',
  `createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `lastUpdateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tradeNO` (`tradeNO`),
  UNIQUE KEY `uk_orderNO` (`orderNO`,`tradeType`)
) ENGINE=InnoDB AUTO_INCREMENT=1541 DEFAULT CHARSET=utf8mb4 COMMENT='交易详情信息';

CREATE TABLE `tradePartyDetail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tradeUserID` bigint(20) NOT NULL COMMENT '支付用户ID',
  `partyName` varchar(100) NOT NULL COMMENT '主体名称：买家姓名/部门名称',
  `partyType` varchar(20) NOT NULL COMMENT '主体类型：个人/公司部门',
  `tradeAccountID` varchar(100) NOT NULL COMMENT '账户ID',
  `ip` varchar(16) DEFAULT '' COMMENT '交易的设备IP信息（对手主体可选填）',
  `mobileNumber` varchar(11) DEFAULT '' COMMENT '用户的电话信息（对手主体可选填）',
  `device` varchar(10) DEFAULT '' COMMENT '手机的平台： IOS/Android（ 对手主体可选填）',
  `noticeMail` varchar(255) NOT NULL COMMENT '通知的邮箱',
  `location` varchar(200) DEFAULT '' COMMENT '当时的位置信息（对手主体可选填）',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3081 DEFAULT CHARSET=utf8mb4 COMMENT='交易主体';

CREATE TABLE `tradeUser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT '用户/部门名称',
  `type` varchar(20) NOT NULL COMMENT '用户类型：PERSON/CHANNEL/DEPARTMENT',
  `isAllowPay` tinyint(4) NOT NULL COMMENT '是否允许支付：0不允许，1允许',
  `isActive` tinyint(4) NOT NULL COMMENT '状态：1：激活，0：未激活，-1：注销',
  `payPassword` varchar(128) NOT NULL COMMENT '支付密码 ',
  `payPhone` varchar(11) NOT NULL COMMENT '支付手机',
  `payEmail` varchar(200) NOT NULL COMMENT '支付邮箱',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `lastUpdateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tradeUser` (`type`,`isActive`,`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COMMENT='钱包用户信息';
