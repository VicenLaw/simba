/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2018/12/3 星期一 8:28:37                        */
/*==============================================================*/


drop table if exists realTimeMessage;

/*==============================================================*/
/* Table: realTimeMessage                                       */
/*==============================================================*/
create table realTimeMessage
(
   id                   bigint not null auto_increment,
   appid                varchar(64) comment '应用id',
   userId               varchar(64) not null comment '用户ID',
   message              text not null comment '内容',
   createTime           datetime not null comment '时间',
   status               int not null comment '状态',
   availTime            datetime comment '有效期',
   reachTime            datetime comment '到达时间',
   callbackUrl          varchar(256) comment '回调url',
   callbackResult       int comment '回调结果',
   primary key (id),
   key AK_Key_status (status),
   key AK_Key_availTime (availTime),
   key AK_Key_userId (userId),
   key AK_Key_appId (appid),
   key AK_Key_createTime (createTime)
);

alter table realTimeMessage comment '实时推送记录';

