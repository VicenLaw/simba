/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/1/22 星期二 11:07:19                       */
/*==============================================================*/


drop table if exists appPush;

drop table if exists huaweiPush;

drop table if exists jPush;

drop table if exists oppoPush;

drop table if exists vivoPush;

drop table if exists xiaomiPush;

/*==============================================================*/
/* Table: appPush                                               */
/*==============================================================*/
create table appPush
(
   id                   bigint not null auto_increment,
   name                 varchar(128) comment '应用名称',
   code                 varchar(128) comment '应用编号',
   createTime           datetime comment '创建时间',
   primary key (id),
   unique key AK_Key_Code (code)
);

alter table appPush comment 'App推送';

/*==============================================================*/
/* Table: huaweiPush                                            */
/*==============================================================*/
create table huaweiPush
(
   id                   bigint not null auto_increment,
   appPushId            bigint comment '应用推送id',
   appId                varchar(64) comment '华为应用id',
   appSecret            varchar(64) comment '华为应用秘钥',
   title                varchar(32) comment '通知标题',
   appPackage           varchar(128) comment '应用包名',
   primary key (id),
   key AK_Key_PushId (appPushId)
);

alter table huaweiPush comment '华为推送';

/*==============================================================*/
/* Table: jPush                                                 */
/*==============================================================*/
create table jPush
(
   id                   bigint not null auto_increment,
   appPushId            bigint comment '应用推送id',
   appKey               varchar(128) comment '极光推送Key',
   appSecret            varchar(128) comment '极光推送Secret',
   appProduct           int comment '是否生产环境',
   primary key (id),
   key AK_Key_PushId (appPushId)
);

alter table jPush comment '极光推送';

/*==============================================================*/
/* Table: oppoPush                                              */
/*==============================================================*/
create table oppoPush
(
   id                   bigint not null auto_increment,
   appPushId            bigint comment '应用推送id',
   appKey               varchar(64) comment 'oppo应用key',
   appSecret            varchar(64) comment 'oppo应用秘钥',
   title                varchar(64) comment '通知标题',
   subTitle             varchar(64) comment '通知子标题',
   primary key (id),
   key AK_Key_PushId (appPushId)
);

alter table oppoPush comment 'Oppo推送';

/*==============================================================*/
/* Table: vivoPush                                              */
/*==============================================================*/
create table vivoPush
(
   id                   bigint not null auto_increment,
   appPushId            bigint comment '应用推送id',
   appId                varchar(64) comment 'vivo应用id',
   appKey               varchar(64) comment 'vivo应用key',
   appSecret            varchar(64) comment 'vivo引用秘钥',
   title                varchar(64) comment '通知标题',
   primary key (id),
   key AK_Key_PushId (appPushId)
);

alter table vivoPush comment 'Vivo推送';

/*==============================================================*/
/* Table: xiaomiPush                                            */
/*==============================================================*/
create table xiaomiPush
(
   id                   bigint not null auto_increment,
   appPushId            bigint comment '应用推送id',
   appSecret            varchar(128) comment '小米秘钥',
   title                varchar(32) comment '通知标题',
   appPackage           varchar(128) comment '应用包名',
   primary key (id),
   key AK_Key_PushId (appPushId)
);

alter table xiaomiPush comment '小米推送';

