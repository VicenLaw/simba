package com.simba;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.simba.framework.util.common.PropertyEncryptUtil;

public class Config {

	public static void main(String[] args) throws IOException {
		// 需要加密的配置文件名称
		String[] configFiles = new String[] { "application.properties", "application-dev.properties", "application-test.properties", "application-prod.properties", "application-demo.properties" };
		dealConfigFile(configFiles);
	}

	/**
	 * 处理配置文件
	 * 
	 * @param configFiles
	 * @throws IOException
	 */
	private static void dealConfigFile(String[] configFiles) throws IOException {
		String path = Config.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		File project = new File(path);
		File root = project.getParentFile().getParentFile();
		String resourcesDir = root.getAbsolutePath() + "/src/main/resources/";
		dealConfigFile(configFiles, resourcesDir);
	}

	/**
	 * 处理配置文件
	 * 
	 * @param configFiles
	 * @param resourcesDir
	 * @throws IOException
	 */
	private static void dealConfigFile(String[] configFiles, String resourcesDir) throws IOException {
		List<String> configFileList = new ArrayList<>(configFiles.length);
		for (String configFile : configFiles) {
			configFileList.add(resourcesDir + configFile);
		}
		PropertyEncryptUtil.encrypt(configFileList);
	}

}
