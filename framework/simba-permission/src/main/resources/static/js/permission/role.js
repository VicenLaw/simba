var Role = {
	"clearPermission": function(name) {
		layer.confirm('确实要清空权限吗？', function(index) {
			layer.close(index);
			$.ajax({
				type: "post",
				url: contextPath + "/role/clearPermission",
				async: true,
				data: {
					roleName: name
				},
				dataType: "json",
				success: function(data) {
					if(data.code == 200) {
						//parent.showSuccessInfo("清空权限成功");
						UTUI.succMsg("清空权限成功");
					} else {
						//parent.showInfo(data.msg);
						UTUI.promMsg("请选择要删除的记录");
					}
				}
			});
		});
	},

	"toAdd": function() {
		window.self.location.href = contextPath + "/role/toAdd";
	},

	"batchDelete": function() {
		var ids = new Array();
		$("input[name='role']").each(function() {
			if(true == $(this).is(':checked')) {
				ids.push($(this).val());
			}
		});
		if(ids.length == 0) {
			//parent.showInfo("请选择要删除的角色");
			UTUI.promMsg("请选择要删除的角色");
			return false;
		}
		layer.confirm('确实要删除吗？', function(index) {
			layer.close(index);
			$.ajax({
				type: "post",
				url: contextPath + "/role/batchDelete",
				data: {
					"roleNames": ids.join(",")
				},
				async: true,
				dataType: "json",
				success: function(data) {
					if(data.code == 200) {
						Role.toList();
					} else {
						//parent.showInfo(data.msg);
						UTUI.promMsg(data.msg);
					}
				}
			});
		});
	},

	"toUpdate": function(name) {
		window.self.location.href = contextPath + "/role/toUpdate?name=" + name;
	},

	"deleteRole": function(name) {
		layer.confirm('确实要删除吗？', function(index) {
			layer.close(index);
			$.ajax({
				type: "post",
				url: contextPath + "/role/batchDelete",
				data: {
					"roleNames": name
				},
				async: true,
				dataType: "json",
				success: function(data) {
					if(data.code == 200) {
						Role.toList();
					} else {
						//parent.showInfo(data.msg);
						UTUI.promMsg(data.msg);
					}
				}
			});
		});
	},

	"toAssignPermission": function(name) {
		layer.open({
			title:"分配权限",
		    type: 2,
		    area: ['90%','90%'],
		    shadeClose : false,
			shade : 0.8,
		    content: contextPath + "/role/toAssignPermission?name=" + name
		});
	},

	"assignPermission": function() {
		var permissionids = $("#permissionids").val();
		if(!permissionids) {
			//showInfo("请选择要分配的权限");
			UTUI.promMsg("请选择要分配的权限");
			return false;
		}
		$.ajax({
			type: "post",
			url: contextPath + "/role/assignPermission",
			async: true,
			dataType: "json",
			data: {
				"permissionID": permissionids,
				"roleName": $("#roleName").val()
			},
			success: function(data) {
				if(data.code == 200) {
					Role.closeLayerDialog();
				} else {
					UTUI.failMsg(data.msg);
				}
			}
		});
	},

	"initSelectPermissionTree": function(ids, texts) {
		TreeViewUtil.initMultiTree("tree", contextPath + "/permission/getPermissionTree", function(data) {
			var id = data.id;
			var permissionids = $("#permissionids").val();
			if(permissionids == "") {
				$("#permissionids").val(id);
			} else {
				$("#permissionids").val(permissionids + "," + id);
			}
		}, function(data) {
			var id = data.id;
			var permissionids = $("#permissionids").val();
			if(permissionids == id) {
				$("#permissionids").val("");
			} else {
				var idArray = permissionids.split(",");
				var newIDArray = new Array();
				for(var i = 0; i < idArray.length; i++) {
					if(id != idArray[i]) {
						newIDArray.push(idArray[i]);
					}
				}
				$("#permissionids").val(newIDArray.join(","));
			}
		}, function() {
			if(!!ids && !!texts) {
				TreeViewUtil.checkTreeNode("tree", ids, texts);
			}
		});
	},

	"toList": function() {
		window.self.location.href = contextPath + "/role/list";
	},

	"checkForm": function() {
		var name = $("#name").val();
		if(!name) {
			//parent.showInfo("名称不能为空");
			UTUI.promMsg("名称不能为空");
			return false;
		}
		return true;
	},
	"closeLayerDialog": function() {
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	    parent.layer.close(index); //再执行关闭
	},
	"end": null

};