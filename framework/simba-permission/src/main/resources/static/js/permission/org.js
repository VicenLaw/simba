var Org = {

	"exportAllPermission": function() {
		window.self.location.href = contextPath + "/org/exportAllPermission";
	},

	"clearRole": function(id) {
		layer.confirm('确实要清空角色吗？', function(index) {
			layer.close(index);
			$.ajax({
				type: "post",
				url: contextPath + "/org/clearRole",
				async: true,
				data: {
					id: id
				},
				dataType: "json",
				success: function(data) {
					if(data.code == 200) {
						//parent.showSuccessInfo("清空角色成功");
						UTUI.succMsg("清空角色成功");
					} else {
						//parent.showInfo(data.msg);
						UTUI.promMsg("data.msg");
					}
				}
			});
		});
	},

	"toAssignRole": function(id) {
		//top.showModal("分配角色", contextPath + "/org/toAssignRole?id=" + id, 700);
		layer.open({
			title:"分配角色",
		    type: 2,
		    area: ['90%','90%'],
		    shadeClose : false,
			shade : 0.8,
		    content: contextPath + "/org/toAssignRole?id=" + id
		});
	},

	"assignRole": function() {
		var ids = new Array();
		$("input[name='role']").each(function() {
			if(true == $(this).is(':checked')) {
				ids.push($(this).val());
			}
		});
		if(ids.length == 0) {
			//showInfo("请选择要分配的角色");
			UTUI.promMsg("请选择要分配的角色");
			return false;
		}
		$.ajax({
			type: "post",
			url: contextPath + "/org/assignRole",
			data: {
				"roleName": ids.join(","),
				"orgID": $("#orgID").val()
			},
			async: true,
			dataType: "json",
			success: function(data) {
				if(data.code == 200) {
					//top.hideModal();
					Org.closeLayerDialog();
				} else {
					//showInfo(data.msg);
					UTUI.promMsg(data.msg);
				}
			}
		});
	},

	"initSelectOrgTree": function(id, text) {
		TreeViewUtil.initTree("tree", contextPath + "/org/getOrgTree", function(data) {
			$("#parentName").val(data.text);
			$("#parentID").val(data.id);
			$('#tree').fadeOut();
		}, function() {
			if(!!id && !!text) {
				TreeViewUtil.selectTreeNode("tree", id, text);
			}
		});
	},

	"initOrgTree": function(id, text) {
		TreeViewUtil.initTree("tree", contextPath + "/org/getOrgTree", function(data) {
			$("#parentName").val(data.text);
			$("#parentID").val(data.id);
			Org.initOrgList();
		}, function() {
			if(!!id && !!text) {
				TreeViewUtil.selectTreeNode("tree", id, text);
			}
		});
	},

	"initOrgList": function() {
		$.ajax({
			type: "get",
			url: contextPath + "/org/getOrgList",
			data: {
				"parentID": $("#parentID").val()
			},
			async: true,
			dataType: "html",
			success: function(html) {
				$("#table").find("tbody").html(html);
				CheckBox.init();
				setTimeout("CheckBox.bindCheckAll();", 1000);
			}
		});
	},

	"toAdd": function() {
		window.self.location.href = contextPath + "/org/toAdd?parentID=" + $("#parentID").val();
	},

	"toUpdate": function(id) {
		window.self.location.href = contextPath + "/org/toUpdate?id=" + id;
	},

	"deleteOrg": function(id) {
		layer.confirm('确实要删除吗？', function(index) {
			layer.close(index);
			$.ajax({
				type: "post",
				url: contextPath + "/org/delete",
				data: {
					"id": id
				},
				async: true,
				dataType: "json",
				success: function(data) {
					if(data.code == 200) {
						Org.initOrgList();
						Org.initOrgTree($("#parentID").val(), $("#parentName").val());
					} else {
						//parent.showInfo(data.msg);
						UTUI.promMsg(data.msg);
					}
				}
			});
		});
	},

	"batchDelete": function() {
		var ids = new Array();
		$("input[name='org']").each(function() {
			if(true == $(this).is(':checked')) {
				ids.push($(this).val());
			}
		});
		if(ids.length == 0) {
			//parent.showInfo("请选择要删除的机构");
			UTUI.promMsg("请选择要删除的机构");
			return false;
		}
		layer.confirm('确实要删除吗？', function(index) {
			layer.close(index);
			$.ajax({
				type: "post",
				url: contextPath + "/org/batchDelete",
				data: {
					"ids": ids.join(",")
				},
				async: true,
				dataType: "json",
				success: function(data) {
					if(data.code == 200) {
						Org.initOrgList();
						Org.initOrgTree($("#parentID").val(), $("#parentName").val());
					} else {
						//parent.showInfo(data.msg);
						UTUI.promMsg(data.msg);
					}
				}
			});
		});
	},

	"toList": function() {
		window.self.location.href = contextPath + "/org/list?parentID=" + $("#parentID").val();
	},

	"checkForm": function() {
		var text = $("#text").val();
		if(!text) {
			//parent.showInfo("机构名称不能为空");
			UTUI.promMsg("机构名称不能为空");
			return false;
		}
		var parentID = $("#parentID").val();
		if(!parentID) {
			//parent.showInfo("父机构不能为空");
			UTUI.promMsg("父机构不能为空");
			return false;
		}
		var orderNo = $("#orderNo").val();
		if(!orderNo) {
			//parent.showInfo("排序不能为空");
			UTUI.promMsg("排序不能为空");
			return false;
		}
		for(var i = 0; i < 4; i++) {
			orderNo = orderNo.replace(",", "");
		}
		$("#orderNo").val(orderNo);
		return true;
	},
	
	"closeLayerDialog": function() {
		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	    parent.layer.close(index); //再执行关闭
	},
	"end": null
};