package com.simba.permission.controller;

import com.google.common.base.Strings;
import com.simba.framework.util.code.EncryptUtil;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.permission.controller.vo.UserNoExtVo;
import com.simba.permission.model.User;
import com.simba.permission.model.UserExt;
import com.simba.permission.model.UserExtDesc;
import com.simba.permission.model.UserOrg;
import com.simba.permission.service.UserOrgService;
import com.simba.permission.service.UserService;
import com.simba.registry.util.RegistryUtil;
import com.simba.util.SessionUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/user")
public class UserApiController {

  @Autowired
  private UserService userService;

  @Autowired
  private UserOrgService userOrgService;

  @Value("${key:test}")
  private String key;

  @ResponseBody
  @RequestMapping("/list")
  public JsonResult list(Long orgID, Pager pager) {
    if (orgID == null) {
      orgID = -1L;
    }
    List<UserOrg> userOrgList = userOrgService.pageBy("orgID", orgID, pager);
    List<UserNoExtVo> userList = new ArrayList<UserNoExtVo>(userOrgList.size());
    for (UserOrg userOrg : userOrgList) {
      User user = userService.get(userOrg.getUserAccount());
      userList.add(new UserNoExtVo(user.getAccount(), user.getName()));
    }

    return new JsonResult(userList, "获取用户列表成功");
  }

  private void assertAccount(String account, String msg) {
    String adminUserName = RegistryUtil.get("administrator.username");
    String un = EncryptUtil.md5(account + key);

    if (adminUserName.equals(un)) {
      throw new RuntimeException(Strings.isNullOrEmpty(msg) ? "该用户已被占用" : msg);
    }
  }

  @ResponseBody
  @RequestMapping("/add")
  public JsonResult add(User user, Integer[] orgID, HttpServletRequest request) {
    assertAccount(user.getAccount(), null);
    if (orgID == null || orgID.length == 0) {
      orgID = new Integer[]{-1};
    }
    UserExt userExt = new UserExt();
    Map<String, String> extMap = new HashMap<>();
    userExt.setExtMap(extMap);
    userExt.setUserAccount(user.getAccount());
    Map<String, String> descMap = UserExtDesc.getAllDesc();
    descMap.keySet().forEach((key) -> {
      extMap.put(key, request.getParameter(key));
    });
    List<UserOrg> userOrgList = new ArrayList<UserOrg>();
    for (Integer org : orgID) {
      UserOrg userOrg = new UserOrg();
      userOrg.setOrgID(org);
      userOrg.setUserAccount(user.getAccount());
      userOrgList.add(userOrg);
    }
    userService.add(user, userExt, userOrgList);
    return new JsonResult("", "增加成功");
  }

  @ResponseBody
  @RequestMapping("/update")
  public JsonResult update(User user, Integer[] orgID, HttpServletRequest request) {
    assertAccount(user.getAccount(), "该用户已存在");
    if (orgID == null || orgID.length == 0) {
      orgID = new Integer[]{-1};
    }
    UserExt userExt = new UserExt();
    Map<String, String> extMap = new HashMap<>();
    userExt.setExtMap(extMap);
    userExt.setUserAccount(user.getAccount());
    Map<String, String> descMap = UserExtDesc.getAllDesc();
    descMap.keySet().forEach((key) -> {
      extMap.put(key, request.getParameter(key));
    });
    List<UserOrg> userOrgList = new ArrayList<UserOrg>();
    for (Integer org : orgID) {
      UserOrg userOrg = new UserOrg();
      userOrg.setOrgID(org);
      userOrg.setUserAccount(user.getAccount());
      userOrgList.add(userOrg);
    }
    userService.update(user, userExt, userOrgList);
    return new JsonResult("", "更新成功");
  }

  @ResponseBody
  @RequestMapping("/batchDelete")
  public JsonResult batchDelete(String[] accounts) {
    List<String> accountList = Arrays.asList(accounts);
    accountList.forEach(t -> {
      assertAccount(t, "该用户不可被删除");
    });
    userService.batchDelete(accountList);
    return new JsonResult("", "批量删除成功");
  }

  @ResponseBody
  @RequestMapping("/delete")
  public JsonResult delete(String account) {
    assertAccount(account, "该用户不可被操作");
    userService.delete(account);
    return new JsonResult("", "删除成功");
  }

  @ResponseBody
  @RequestMapping("/resetPwd")
  public JsonResult resetPwd(String account) {
    assertAccount(account, "该用户不可被操作");
    userService.resetPwd(account);
    return new JsonResult("", "重置密码成功");
  }

  @ResponseBody
  @RequestMapping("/modifyPwd")
  public JsonResult modifyPwd(HttpServletRequest request, String oldPwd, String newPwd,
      String confirmPwd) {
    if (!confirmPwd.equals(newPwd)) {
      throw new RuntimeException("确认密码和新密码不一致");
    }
    if (oldPwd.equals(newPwd)) {
      throw new RuntimeException("旧密码和新密码不能一样");
    }
    User user = SessionUtil.getUser(request.getSession());
    if (user == null) {
      throw new RuntimeException("用户未登录");
    }
    assertAccount(user.getAccount(), "该用户不能修改密码");
    userService.updatePwd(user.getAccount(), oldPwd, newPwd);
    return new JsonResult("", "修改密码成功");
  }

}
