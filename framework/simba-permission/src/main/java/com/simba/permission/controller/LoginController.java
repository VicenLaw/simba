package com.simba.permission.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import java.util.Map;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simba.CaptchaUtil;
import com.simba.cache.Redis;
import com.simba.exception.LoginException;
import com.simba.framework.session.SessionService;
import com.simba.framework.util.code.EncryptUtil;
import com.simba.framework.util.common.ServerUtil;
import com.simba.framework.util.date.DateUtil;
import com.simba.framework.util.json.JsonResult;
import com.simba.limit.annotation.RateLimit;
import com.simba.model.constant.ConstantData;
import com.simba.model.constant.SimbaRedisKey;
import com.simba.permission.model.Org;
import com.simba.permission.model.OrgExt;
import com.simba.permission.model.OrgRole;
import com.simba.permission.model.Permission;
import com.simba.permission.model.Role;
import com.simba.permission.model.User;
import com.simba.permission.model.UserExt;
import com.simba.permission.service.OrgRoleService;
import com.simba.permission.service.RoleService;
import com.simba.permission.service.UserService;
import com.simba.registry.util.RegistryUtil;
import com.simba.service.OperLogService;
import com.simba.util.SessionUtil;

/**
 * 登陆控制器
 * 
 * @author caozj
 * 
 */
@Controller
public class LoginController {

	private static final Log logger = LogFactory.getLog(LoginController.class);

	@Value("${key:test}")
	private String key;

	@Value("${page.index:index}")
	private String indexPage;

	@Value("${page.login:login}")
	private String loginPage;

	@Value("${page.index.long.title:管理系统}")
	private String longTitle;

	@Value("${page.index.short.title:系统}")
	private String shortTitle;

	@Value("${page.index.copyright:}")
	private String copyright;

	@Value("${info.version:2.0.0}")
	private String infoVersion;// 版本

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private OrgRoleService orgRoleService;

	@Autowired
	private OperLogService operLogService;

	@Autowired
	private Redis redisUtil;

	@Value("${system.user.lock.time:30}")
	private String lockTime;

	@Value("${system.user.max.login.fail.count:3}")
	private String maxLoginFail;

	private int lockTimeSeconds;

	private int maxLoginFailCount;

	@Autowired
	private SessionService sessionService;

	@PostConstruct
	private void init() {
		indexPage = StringUtils.defaultIfEmpty(indexPage, "index");
		loginPage = StringUtils.defaultIfEmpty(loginPage, "login");
		longTitle = StringUtils.defaultIfEmpty(longTitle, "管理系统");
		shortTitle = StringUtils.defaultIfEmpty(shortTitle, "系统");
		infoVersion = StringUtils.defaultIfEmpty(infoVersion, "2.0.0");
		if ("@project.version@".equals(infoVersion)) {
			infoVersion = "2.0.0";
		}
		if (StringUtils.isEmpty(copyright)) {
			copyright = "<strong>Copyright &copy; 2016-" + DateUtil.date2String(new Date(), "yyyy") + " .</strong> All rights reserved.";
		}
		maxLoginFailCount = NumberUtils.toInt(maxLoginFail, 3);
		lockTimeSeconds = 60 * NumberUtils.toInt(lockTime, 30);
	}

	/**
	 * 进入登陆界面
	 * 
	 * @return
	 */
	@RateLimit(limit = 10)
	@RequestMapping("/login/toLogin")
	public String toLogin(HttpServletRequest request, ModelMap model) {
		if (SessionUtil.isLogin(request.getSession())) {
			putIndexModel(model, request);
			return indexPage;
		}
		String captchaEnabled = RegistryUtil.get("login.captcha.enabled");
		model.put("captchaEnabled", captchaEnabled);
		return loginPage;
	}

	private void putIndexModel(ModelMap model, HttpServletRequest request) {
		String scheme = request.getScheme();
		String serverName = request.getServerName();
		int port = request.getServerPort();
		String path = request.getContextPath();
		String basePath = scheme + "://" + serverName + ":" + port + path;
		model.put("basePath", basePath);
		model.put("shortTitle", shortTitle);
		model.put("longTitle", longTitle);
		model.put("infoVersion", infoVersion);
		model.put("copyright", copyright);
	}

	/**
	 * 登陆
	 * 
	 * @param account
	 * @param password
	 * @param request
	 * @return
	 */
	@RateLimit(limit = 30)
	@ResponseBody
	@RequestMapping("/api/login")
	public JsonResult login(String account, String password, HttpServletRequest request) {
		if (checkAccountFrozed(account, request)) {
			throw new LoginException("您的账号已经被冻结，请耐心等待解冻后访问");
		}
		String captchaEnabled = RegistryUtil.get("login.captcha.enabled");
		if ("true".equals(captchaEnabled) && !checkCaptcha(request)) {
			throw new LoginException("验证码错误");
		} else if (StringUtils.isEmpty(account) || StringUtils.isEmpty(password)) {
			throw new LoginException("用户名和密码不能为空");
		} else if (checkAccount(account, password, request.getSession())) {
			String sid = (String) request.getAttribute(ConstantData.SESSIONID_COOKIE_NAME);
			String key = "Simba:String:Login:" + account;
			String loginedSid = redisUtil.getString(key);
			String description = "登录";
			if (StringUtils.isNotEmpty(loginedSid) && !StringUtils.equals(loginedSid, sid)) {
				description = "登录并踢下同账号已登录用户";
				sessionService.removeSession(loginedSid);
			}
			redisUtil.setString(key, sid);
			operLogService.add(request, description, true);
			clearAccountLoginFail(account, request);
			Map<String, Object> map = new HashMap<>();
			boolean isAdmin = checkAdmin(account, password);

			if(isAdmin) {
				map.put("name", "admin");
				map.put("account", "admin");
			}else {
				User user= userService.get(account);
				map.put("name", user.getName());
				map.put("account", user.getAccount());
			}
			return new JsonResult(map, "登录成功");
		} else {
			recordAccountLoginFail(account, request);
			throw new LoginException("用户名或者密码错误");
		}
	}

	/**
	 * restful version 登陆
	 *
	 * @param userName
	 * @param password
	 * @param model
	 * @param request
	 * @return
	 */
	@RateLimit(limit = 3)
	@RequestMapping("/login/login")
	public String login(String userName, String password, ModelMap model, HttpServletRequest request) {
		if (SessionUtil.isLogin(request.getSession())) {
			putIndexModel(model, request);
			return indexPage;
		}
		if (checkAccountFrozed(userName, request)) {
			model.put("errMsg", "您的账号已经被冻结，请耐心等待解冻后访问");
			return loginPage;
		}
		String view = null;
		String captchaEnabled = RegistryUtil.get("login.captcha.enabled");
		if ("true".equals(captchaEnabled) && !checkCaptcha(request)) {
			view = loginPage;
			model.put("errMsg", "验证码错误");
		} else if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
			view = loginPage;
			model.put("errMsg", "用户名和密码不能为空");
		} else if (checkAccount(userName, password, request.getSession())) {
			String sid = (String) request.getAttribute(ConstantData.SESSIONID_COOKIE_NAME);
			String key = "Simba:String:Login:" + userName;
			String loginedSid = redisUtil.getString(key);
			String description = "登录";
			if (StringUtils.isNotEmpty(loginedSid) && !StringUtils.equals(loginedSid, sid)) {
				description = "登录并踢下同账号已登录用户";
				sessionService.removeSession(loginedSid);
			}
			redisUtil.setString(key, sid);
			operLogService.add(request, description, true);
			putIndexModel(model, request);
			view = indexPage;
			clearAccountLoginFail(userName, request);
		} else {
			view = loginPage;
			model.put("errMsg", "用户名或者密码错误");
			recordAccountLoginFail(userName, request);
		}
		model.put("userName", userName);
		model.put("password", password);
		model.put("captchaEnabled", captchaEnabled);
		return view;
	}

	/**
	 * 清理登录失败次数
	 * 
	 * @param userName
	 * @param request
	 */
	private void clearAccountLoginFail(String userName, HttpServletRequest request) {
		String ip = ServerUtil.getProxyIp(request);
		String key = getLoginFailKey(userName, ip);
		redisUtil.removeString(key);
	}

	/**
	 * 记录账号登录失败
	 * 
	 * @param userName
	 * @param request
	 */
	private void recordAccountLoginFail(String userName, HttpServletRequest request) {
		String ip = ServerUtil.getProxyIp(request);
		String key = getLoginFailKey(userName, ip);
		long loginFailCount = redisUtil.getAutoId(key);
		redisUtil.expireString(key, 24 * 60 * 60);
		if (loginFailCount >= maxLoginFailCount) {
			// 冻结账号
			String frozedAccountKey = getAccountFrozedKey(userName, ip);
			redisUtil.setString(frozedAccountKey, "true", lockTimeSeconds);
		}
	}

	private String getLoginFailKey(String userName, String ip) {
		return SimbaRedisKey.systemUserLoginFail + ip + ":" + userName + ":" + DateUtil.date2String(new Date(), "yyyyMMdd");
	}

	/**
	 * 检查账号是否被冻结，超过则禁用一段时间
	 * 
	 * @param userName
	 * @param request
	 */
	private boolean checkAccountFrozed(String userName, HttpServletRequest request) {
		String ip = ServerUtil.getProxyIp(request);
		String key = getAccountFrozedKey(userName, ip);
		String locked = redisUtil.getString(key);
		if (StringUtils.isNotEmpty(locked)) {
			logger.error("您的账号[" + userName + "][" + ip + "]已经被冻结，请耐心等待解冻后访问");
			return true;
		}
		return false;
	}

	private String getAccountFrozedKey(String userName, String ip) {
		return SimbaRedisKey.systemUserLocked + ip + ":" + userName;
	}

	/**
	 * 检查验证码是否正确
	 * 
	 * @param request
	 * @return
	 */
	private boolean checkCaptcha(HttpServletRequest request) {
		return CaptchaUtil.checkCaptcha(request.getSession(), request.getParameter("captcha"));
	}

	/**
	 * 退出
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/login/logout")
	public String logout(HttpServletRequest request, ModelMap model) {
		operLogService.add(request, "退出", false);
		SessionUtil.clearSession(request.getSession());
		String captchaEnabled = RegistryUtil.get("login.captcha.enabled");
		model.put("captchaEnabled", captchaEnabled);
		return loginPage;
	}

	/**
	 * restful version 退出
	 *
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/api/logout")
	public JsonResult logout(HttpServletRequest request) {
		operLogService.add(request, "退出", false);
		SessionUtil.clearSession(request.getSession());
		return new JsonResult("登出成功");
	}

	/**
	 * 检查账号密码是否正确
	 * 
	 * @param userName
	 * @param password
	 * @param session
	 * @return
	 */
	private boolean checkAccount(String userName, String password, HttpSession session) {
		boolean isAdmin = checkAdmin(userName, password);
		if (isAdmin) {
			User user = new User();
			user.setAccount(userName);
			user.setName("超级管理员");
			SessionUtil.setUser(session, user);
			SessionUtil.setAdmin(session);
			return true;
		}
		boolean exist = userService.checkUser(userName, password);
		if (!exist) {
			return false;
		}
		User user = userService.get(userName);
		UserExt userExt = userService.getUserExt(userName);
		List<Org> orgList = userService.listOrgByUser(userName);
		List<OrgExt> orgExtList = userService.listOrgExtByUser(userName);
		List<Role> roleList = userService.listRoleByAccount(userName);
		orgList.forEach((org) -> {
			List<OrgRole> list = orgRoleService.listBy("orgID", org.getId());
			list.forEach((OrgRole orgRole) -> {
				Role role = roleService.get(orgRole.getRoleName());
				if (role != null) {
					roleList.add(role);
				}
			});
		});
		List<Permission> permissionList = new ArrayList<Permission>();
		for (Role role : roleList) {
			List<Permission> list = roleService.listByRole(role.getName());
			permissionList.addAll(list);
		}
		SessionUtil.setUser(session, user);
		SessionUtil.setUserExt(session, userExt);
		SessionUtil.setRoles(session, roleList);
		SessionUtil.setPermissions(session, permissionList);
		SessionUtil.setOrgs(session, orgList);
		SessionUtil.setOrgExts(session, orgExtList);
		return true;
	}

	/**
	 * 验证用户名密码是否为超级管理员
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	private boolean checkAdmin(String userName, String password) {
		String adminUserName = RegistryUtil.get("administrator.username");
		String adminPassword = RegistryUtil.get("administrator.password");
		String un = EncryptUtil.md5(userName + key);
		if (!adminUserName.equals(un)) {
			return false;
		}
		String md = EncryptUtil.md5(password + key);
		if (!md.equals(adminPassword)) {
			return false;
		}
		return true;
	}
}
