package com.simba.permission.controller.vo;

public class UserNoExtVo {

  private String account;
  private String name;

  public UserNoExtVo() {
  }

  public UserNoExtVo(String account, String name) {
    this.account = account;
    this.name = name;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("UserNoExtVo{");
    sb.append("account='").append(account).append('\'');
    sb.append(", name='").append(name).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
