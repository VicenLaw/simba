package com.simba.model;

import java.io.Serializable;
import java.util.Date;

import com.simba.annotation.DescAnnotation;

/**
 * 文件版本
 * 
 * @author lilei
 *
 */
@DescAnnotation(desc = "文件版本管理")
public class FileVersion implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 自增主键ID
	 */
	private int id;

	/**
	 * 版本号
	 */
	@DescAnnotation(desc = "版本号")
	private String version;

	/**
	 * 类型ID
	 */
	@DescAnnotation(desc = "类型ID")
	private int typeId;

	/**
	 * 文件大小
	 */
	@DescAnnotation(desc = "文件大小")
	private double fileSize;
	/**
	 * 文件路径
	 */
	@DescAnnotation(desc = "文件路径")
	private String fileUrl;
	/**
	 * 文件描述
	 */
	@DescAnnotation(desc = "文件描述")
	private String description;

	/**
	 * 时间
	 */
	@DescAnnotation(desc = "时间")
	private Date createTime;

	/**
	 * 扩展属性
	 */
	@DescAnnotation(desc = "扩展属性")
	private String extProps;

	/////////////// 扩展///////////////////
	/**
	 * 类型名称
	 */
	private String type;
	
	private int isMinVersion;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public double getFileSize() {
		return fileSize;
	}

	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getExtProps() {
		return extProps;
	}

	public void setExtProps(String extProps) {
		this.extProps = extProps;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getIsMinVersion() {
		return isMinVersion;
	}

	public void setIsMinVersion(int isMinVersion) {
		this.isMinVersion = isMinVersion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "FileVersion [id=" + id + ", version=" + version + ", typeId=" + typeId + ", fileSize=" + fileSize
				+ ", fileUrl=" + fileUrl + ", description=" + description + ", createTime=" + createTime + ", extProps="
				+ extProps + ", type=" + type + ", isMinVersion=" + isMinVersion + "]";
	}
	

	
}
