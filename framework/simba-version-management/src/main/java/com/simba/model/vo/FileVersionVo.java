package com.simba.model.vo;

import java.io.Serializable;
import java.util.Date;

import com.simba.annotation.DescAnnotation;

/**
 * 文件版本
 * 
 * @author lilei
 *
 */
@DescAnnotation(desc = "文件版本管理")
public class FileVersionVo implements Serializable {

	/**
	 * 版本号
	 */
	@DescAnnotation(desc = "版本号")
	private String version;

	/**
	 * 类型ID
	 */
	@DescAnnotation(desc = "类型ID")
	private int typeId;

	/**
	 * 文件大小
	 */
	@DescAnnotation(desc = "文件大小")
	private double fileSize;
	/**
	 * 文件路径
	 */
	@DescAnnotation(desc = "文件路径")
	private String fileUrl;
	/**
	 * 文件描述
	 */
	@DescAnnotation(desc = "文件描述")
	private String description;

	/**
	 * 时间
	 */
	@DescAnnotation(desc = "时间")
	private String createTime;

	/**
	 * 扩展属性
	 */
	@DescAnnotation(desc = "扩展属性")
	private String extProps;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public double getFileSize() {
		return fileSize;
	}

	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getExtProps() {
		return extProps;
	}

	public void setExtProps(String extProps) {
		this.extProps = extProps;
	}

	@Override
	public String toString() {
		return "FileVersionVo [version=" + version + ", typeId=" + typeId + ", fileSize=" + fileSize + ", fileUrl="
				+ fileUrl + ", description=" + description + ", createTime=" + createTime + ", extProps=" + extProps
				+ "]";
	}
	
	

}
