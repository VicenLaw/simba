# GRatelimit

#### 项目介绍
分布式限流器



#### 软件架构
1. 纯注解无侵入
2. 限流最大粒度为单个接口, 可根据IP限流
3. Redis+lua
4. 分2种算法实现限流: 令牌桶 和 时间窗口



#### 例子

```java
/* 令牌桶算法: QPS:100 最大令牌数: 10000 */
@Controller
@ResponseBody
@RateLimitToken(maxBurst = 10000, rate = 100)
public class Test1Controller {

    @RequestMapping("test1")
    public String test1() {
        return "OK1";
    }

    @RequestMapping("test2")
    public String test2() {
        return "OK2";
    }

}
```

