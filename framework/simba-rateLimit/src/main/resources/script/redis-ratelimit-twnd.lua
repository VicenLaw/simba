local key = KEYS[1];
local limit = tonumber(ARGV[1]);
local interval = tonumber(ARGV[2]);
local nowTime = tonumber(ARGV[3]);
local leftEdgeTime = nowTime - interval * 1000;

redis.call('zadd', key, nowTime, nowTime);
redis.call('zremrangeByScore', key, 0, leftEdgeTime);
redis.call('expire', key, interval + 1);
local count = tonumber(redis.call('zcard', key));
if count < limit then
    return 'true';
else
    return 'false';
end