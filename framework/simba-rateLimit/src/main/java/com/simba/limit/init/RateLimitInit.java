package com.simba.limit.init;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.framework.util.applicationcontext.ApplicationContextInit;
import com.simba.limit.algorithm.TimeWindowAlgorithm;
import com.simba.limit.algorithm.TokenBucketAlgorithm;

/**
 * 限流器初始化类
 * 
 * @author caozhejun
 *
 */
@Component
public class RateLimitInit implements ApplicationContextInit {

	private static final Log logger = LogFactory.getLog(RateLimitInit.class);

	@Autowired
	private TimeWindowAlgorithm timeWindowAlgorithm;

	@Autowired
	private TokenBucketAlgorithm tokenBucketAlgorithm;

	@Override
	public void init() {
		try {
			timeWindowAlgorithm.init();
			tokenBucketAlgorithm.init();
			logger.info("限流器初始化完成");
		} catch (IOException e) {
			logger.error("限流器初始化发生异常", e);
		}
	}

	@Override
	public int sort() {
		return 100;
	}

}
