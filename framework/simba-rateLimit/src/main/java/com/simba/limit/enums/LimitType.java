package com.simba.limit.enums;

/**
 * Created by shuoGG on 2018/10/8
 */
public enum LimitType {
	ALL, IP, CUSTOM
}
