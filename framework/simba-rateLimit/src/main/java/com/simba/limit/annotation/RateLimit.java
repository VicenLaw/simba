package com.simba.limit.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.simba.limit.enums.LimitType;

/**
 * Created by shuoGG on 2018/9/29 0029
 *
 * 普通限流器
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RateLimit {

	LimitType type() default LimitType.IP;

	long limit() default 10;

	long intervalSec() default 1;
}
