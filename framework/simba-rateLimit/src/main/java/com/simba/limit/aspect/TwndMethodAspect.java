package com.simba.limit.aspect;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.exception.BussException;
import com.simba.framework.util.common.ServerUtil;
import com.simba.framework.util.request.RequestUtil;
import com.simba.limit.algorithm.TimeWindowAlgorithm;
import com.simba.limit.annotation.RateLimit;
import com.simba.limit.aspect.util.RateKeyGenerator;

/**
 * Created by shuoGG on 2018/9/29 0029
 */
@Aspect
@Component
public class TwndMethodAspect {

	@Autowired
	private TimeWindowAlgorithm algorithm;

	@Autowired
	private RateKeyGenerator keyGen;

	@Pointcut("@annotation(rateLimit)")
	public void annotationPointcut(RateLimit rateLimit) {
	}

	@Before("annotationPointcut(rateLimit)")
	public void doBefore(JoinPoint joinPoint, RateLimit rateLimit) throws IOException {
		String key = keyGen.getKey(rateLimit.type(), joinPoint);
		key = key + ":twnd";
		boolean isOk = algorithm.checkAllow(key, rateLimit.limit(), rateLimit.intervalSec());
		if (!isOk) {
			throw new BussException("客户端IP" + ServerUtil.getProxyIp(RequestUtil.getRequest()) + "访问太频繁");
		}
	}

}
