package com.simba.limit.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.simba.limit.enums.LimitType;

/**
 * Created by shuoGG on 2018/9/29 0029
 *
 * 令牌桶限流器
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RateLimitToken {

	LimitType type() default LimitType.IP;

	/**
	 * 桶上限
	 */
	long maxBurst() default 1000;

	/**
	 * 每秒产生的令牌数
	 */
	float rate() default 100f;

}
