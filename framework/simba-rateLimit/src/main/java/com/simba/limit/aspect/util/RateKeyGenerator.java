package com.simba.limit.aspect.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.framework.util.request.RequestUtil;
import com.simba.limit.enums.LimitType;

/**
 * Created by shuoGG on 2018/10/9
 */
@Component
public class RateKeyGenerator {

    private static final Log logger = LogFactory.getLog(RateKeyGenerator.class);

    @Value("${spring.application.name:Simba}")
    private String appName;

    private static final String CUSTOM_REQ_KEY = "CUSTOM_REQ_KEY";

    public String getKey(LimitType type, JoinPoint joinPoint) {
        StringBuilder key = new StringBuilder();
        HttpServletRequest request = RequestUtil.getRequest();
        key.append("rateLimit:").append(appName).append(":");
        switch (type) {
            case ALL:
                key.append(getMethodSign(joinPoint));
                break;
            case IP:
                key.append(getMethodSign(joinPoint)).append(getRemoteIp(request));
                break;
            case CUSTOM:
                key.append(getMethodSign(joinPoint)).append(getCustomKey(request));
                break;
            default:
        }
        return key.toString();
    }


    @SuppressWarnings("rawtypes")
    private String getMethodSign(JoinPoint joinPoint) {
        StringBuilder key = new StringBuilder();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        key.append(joinPoint.getTarget().getClass().getName());
        key.append(":");
        key.append(signature.getMethod().getName());
        key.append(":");
        Class[] parameterTypes = signature.getParameterTypes();
        for (Class clazz : parameterTypes) {
            key.append(clazz.getName());
        }
        return key.toString();
    }

    private String getRemoteIp(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    logger.error("获取IP地址发生异常", e);
                }
                assert inet != null;
                ipAddress = inet.getHostAddress();
            }
        }
        if (ipAddress != null && ipAddress.length() > 15) {
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;
    }

    private String getCustomKey(HttpServletRequest request) {
        StringBuilder key = new StringBuilder();
        if (request.getAttribute(CUSTOM_REQ_KEY) != null) {
            key.append(CUSTOM_REQ_KEY).append(request.getAttribute(CUSTOM_REQ_KEY));
        } else {
            throw new RuntimeException("request的Attribute里面没有CUSTOM_REQ_KEY对应的K-V");
        }
        return key.toString();
    }

}
