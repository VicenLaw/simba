package com.simba.limit.algorithm;


import com.simba.cache.RedisUtil;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import redis.clients.jedis.Jedis;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shuoGG on 2018/9/30
 * <p>
 * 令牌桶算法
 */
@Component
public class TokenBucketAlgorithm {

	@Autowired
	private RedisUtil redisUtil;

	private static final String SCRIPT_NAME = "script/redis-ratelimit-bucket.lua";

	private Object sha = null;

	/**
	 * 初始化
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void init() throws FileNotFoundException, IOException {
		try (Jedis jedis = redisUtil.getJedis()) {
			InputStream file = new ClassPathResource(SCRIPT_NAME).getInputStream();
			String script = IOUtils.toString(file);
			sha = jedis.scriptLoad(script);
		}
	}

	public boolean checkAllow(String key, long maxBurst, float rate) throws IOException {
		try (Jedis jedis = redisUtil.getJedis()) {
			List<String> keys = new ArrayList<>();
			keys.add(key);
			List<String> argS = new ArrayList<>();
			argS.add(Long.toString(1));
			argS.add(Long.toString(System.currentTimeMillis()));
			argS.add(Long.toString(maxBurst));
			argS.add(Float.toString(rate));
			Object ret = jedis.evalsha(sha.toString(), keys, argS);
			return "true".equals(ret);
		}
	}

}
