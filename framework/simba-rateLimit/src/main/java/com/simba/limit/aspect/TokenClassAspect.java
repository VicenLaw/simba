package com.simba.limit.aspect;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.exception.BussException;
import com.simba.framework.util.common.ServerUtil;
import com.simba.framework.util.request.RequestUtil;
import com.simba.limit.algorithm.TokenBucketAlgorithm;
import com.simba.limit.annotation.RateLimitToken;
import com.simba.limit.aspect.util.RateKeyGenerator;

/**
 * Created by shuoGG on 2018/9/29 0029
 */
@Aspect
@Component
public class TokenClassAspect {

	@Autowired
	private TokenBucketAlgorithm algorithm;

	@Autowired
	private RateKeyGenerator keyGen;

	@Pointcut("@within(rateLimitToken)")
	public void annotationPointcut(RateLimitToken rateLimitToken) {
	}

	@Before("@within(rateLimitToken)")
	public void doBefore(JoinPoint joinPoint, RateLimitToken rateLimitToken) throws IOException {
		String key = keyGen.getKey(rateLimitToken.type(), joinPoint);
		key = key + ":tokenBucket";
		boolean isOk = algorithm.checkAllow(key, rateLimitToken.maxBurst(), rateLimitToken.rate());
		if (!isOk) {
			throw new BussException("客户端IP" + ServerUtil.getProxyIp(RequestUtil.getRequest()) + "访问太频繁");
		}
	}

}
