package com.simba.framework.util.json;

import com.simba.framework.util.json.JsonResult;

/**
 * 分页返回对象
 * 
 * @author liusheng
 *
 */
public class PageResult {

	private int code = JsonResult.successCode;
	private String msg = "查询成功";// 提示
	private long totalRecords = 0;// 总记录数

	private Object data;

	public PageResult() {
	}

	public PageResult(Object data, Long totalRecords) {
		this.msg = "查询成功";
		this.totalRecords = totalRecords;
		this.data = data;
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
