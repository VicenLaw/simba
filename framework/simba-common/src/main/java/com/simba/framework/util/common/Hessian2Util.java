package com.simba.framework.util.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.caucho.hessian.io.SerializerFactory;
import com.simba.exception.SimbaException;

/**
 * Hessian2序列化/反序列化工具
 *
 * Created by shuoGG on 2018/10/18
 */
public class Hessian2Util {

	private static final Log logger = LogFactory.getLog(Hessian2Util.class);

	private SerializerFactory serializerFactory;

	private Hessian2Util() {
		serializerFactory = SerializerFactory.createDefault();
	}

	private static class Hessian2UtilHolder {
		private static final Hessian2Util instance = new Hessian2Util();
	}

	public static Hessian2Util getInstance() {
		return Hessian2UtilHolder.instance;
	}

	public byte[] serialize(Object obj) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		Hessian2Output out = new Hessian2Output(bytes);
		out.setSerializerFactory(serializerFactory);
		try {
			out.writeObject(obj);
			out.close();
		} catch (IOException e) {
			logger.error("Hessian2序列化对象错误", e);
			throw new SimbaException("Hessian2序列化对象错误");
		}
		return bytes.toByteArray();
	}

	public Object deserialize(byte[] data) {
		Hessian2Input in = new Hessian2Input(new ByteArrayInputStream(data));
		in.setSerializerFactory(serializerFactory);
		Object obj;
		try {
			obj = in.readObject();
			in.close();
		} catch (IOException e) {
			logger.error("Hessian2反序列化对象错误", e);
			throw new SimbaException("Hessian2反序列化对象错误");
		}
		return obj;
	}
}
