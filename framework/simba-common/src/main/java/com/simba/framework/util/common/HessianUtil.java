package com.simba.framework.util.common;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Hessian1序列化/反序列化工具
 *
 * Created by shuoGG on 2018/10/18
 */
public class HessianUtil {

    public static byte[] serialize(Object obj) {
        if (obj == null) throw new NullPointerException();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        HessianOutput ho = new HessianOutput(os);
        try {
            ho.writeObject(obj);
        } catch (IOException e) {
            throw new RuntimeException("IO错误", e);
        }
        return os.toByteArray();
    }

    public static Object deserialize(byte[] data) {
        if (data == null) throw new NullPointerException();
        ByteArrayInputStream is = new ByteArrayInputStream(data);
        HessianInput hi = new HessianInput(is);
        try {
            return hi.readObject();
        } catch (IOException e) {
            throw new RuntimeException("IO错误", e);
        }
    }
}
