package com.simba.apppush.vivo.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.apppush.vivo.model.AccessTokenRequest;
import com.simba.apppush.vivo.model.AccessTokenResponse;
import com.simba.cache.Redis;
import com.simba.exception.SimbaException;
import com.simba.framework.util.code.EncryptUtil;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.okhttp.util.OkHttpClientUtil;

/**
 * Vivo accesstoken工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class VivoAccessTokenUtil {

	private static final String key = "Simba:String:Vivo:AccessToken:";

	@Autowired
	private Redis redisUtil;

	@Value("${vivo.app.id:}")
	private String appid;

	@Value("${vivo.app.key:}")
	private String appKey;

	@Value("${vivo.app.secret:}")
	private String appSecret;

	private boolean enable = false;

	public boolean isEnable() {
		return enable;
	}

	private String redisKey;

	public VivoAccessTokenUtil() {

	}

	public VivoAccessTokenUtil(String appid, String appKey, String appSecret) {
		this.appid = appid;
		this.appKey = appKey;
		this.appSecret = appSecret;
	}

	@PostConstruct
	public void init() {
		if (StringUtils.isNotEmpty(appid) && StringUtils.isNotEmpty(appKey) && StringUtils.isNotEmpty(appSecret)) {
			redisKey = key + appid;
			enable = true;
		}
	}

	/**
	 * 请求Vivo accesstoken
	 * 
	 * @throws IOException
	 */
	public void requestAccessToken() throws IOException {
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		AccessTokenRequest request = new AccessTokenRequest();
		request.setAppId(NumberUtils.toInt(appid));
		request.setAppKey(appKey);
		long now = System.currentTimeMillis();
		request.setTimestamp(now);
		String sign = EncryptUtil.md5(appid + appKey + now + appSecret);
		request.setSign(sign);
		String json = FastJsonUtil.toJson(request);
		String res = OkHttpClientUtil.getInstance().postJsonWithHeaders(VivoConstantData.tokenUrl, json, headers);
		AccessTokenResponse response = FastJsonUtil.toObject(res, AccessTokenResponse.class);
		String token = response.getAuthToken();
		if (StringUtils.isEmpty(token)) {
			throw new SimbaException("vivo App 推送获取accesstoken异常:" + response.getResult() + "," + response.getDesc());
		}
		redisUtil.setString(redisKey, token);
	}

	/**
	 * 获取accesstoken
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getAccessToken() throws IOException {
		String token = redisUtil.getString(redisKey);
		if (StringUtils.isEmpty(token)) {
			this.requestAccessToken();
			return this.getAccessToken();
		}
		return token;
	}
}
