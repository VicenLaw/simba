package com.simba.apppush.vivo.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.simba.apppush.vivo.exception.VivoException;
import com.simba.apppush.vivo.util.VivoAccessTokenUtil;

/**
 * vivo App推送工具类的Aspect Aop工具类，用来处理accesstoken过期无效
 * 
 * @author caozhejun
 *
 */
@Aspect
@Component
@ConditionalOnProperty(value = "vivo.access.token.job.enable", havingValue = "true")
public class VivoAppPushUtilAspect {

	private static final Log logger = LogFactory.getLog(VivoAppPushUtilAspect.class);

	private static final int code = 10000;

	@Autowired
	private VivoAccessTokenUtil vivoAccessTokenUtil;

	@Around(value = "execution(* com.simba.apppush.vivo.util.VivoAppPushUtil.*(..))")
	public Object dealAccessToken(ProceedingJoinPoint pjd) throws Throwable {
		// 获取目标方法签名
		String signature = pjd.getSignature().toString();
		logger.info("调用vivo App推送工具类:" + signature);
		long now = System.currentTimeMillis();
		Object result = null;
		try {
			result = pjd.proceed();
		} catch (VivoException e) {
			if (e.getErrcode() == code) {
				logger.info("vivo accesstoken 过期,重新获取accesstoken");
				// 重新获取accesstoken
				vivoAccessTokenUtil.requestAccessToken();
				result = pjd.proceed();
			} else {
				throw e;
			}
		}
		long consumeTime = System.currentTimeMillis() - now;
		logger.info("执行" + signature + "消耗的时间为" + consumeTime + "毫秒");
		return result;
	}
}
