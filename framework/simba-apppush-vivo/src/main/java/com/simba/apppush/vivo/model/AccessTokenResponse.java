package com.simba.apppush.vivo.model;

/**
 * 推送鉴权响应对象
 * 
 * @author caozhejun
 *
 */
public class AccessTokenResponse {

	/**
	 * 接口调用是否成功的状态码0 成功，非0 失败
	 */
	private int result;

	/**
	 * 文字描述接口调用情况
	 */
	private String desc;

	/**
	 * 当鉴权成功时才会有该字段， 推送消息时， 需要提供 authToken，有效期默认为1 天，过期后无法使用
	 */
	private String authToken;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessTokenResponse [result=");
		builder.append(result);
		builder.append(", desc=");
		builder.append(desc);
		builder.append(", authToken=");
		builder.append(authToken);
		builder.append("]");
		return builder.toString();
	}

}
