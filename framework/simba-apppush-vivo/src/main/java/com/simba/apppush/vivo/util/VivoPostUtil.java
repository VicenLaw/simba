package com.simba.apppush.vivo.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.simba.apppush.vivo.exception.VivoException;
import com.simba.apppush.vivo.model.VivoBaseResult;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.okhttp.util.OkHttpClientUtil;

/**
 * vivo提交post请求工具类
 * 
 * @author caozhejun
 *
 */
public class VivoPostUtil {

	/**
	 * 提交postjson请求
	 * 
	 * @param token
	 * @param url
	 * @param json
	 * @return
	 * @throws IOException
	 */
	public static VivoBaseResult postJson(String token, String url, String json) throws IOException {
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("authToken", token);
		String res = OkHttpClientUtil.getInstance().postJsonWithHeaders(VivoConstantData.singleMessageUrl, json, headers);
		VivoBaseResult response = FastJsonUtil.toObject(res, VivoBaseResult.class);
		if (response.getResult() != 0) {
			throw new VivoException("发送Vivo App推送发生异常", response.getResult(), response.getDesc());
		}
		return response;
	}

}
