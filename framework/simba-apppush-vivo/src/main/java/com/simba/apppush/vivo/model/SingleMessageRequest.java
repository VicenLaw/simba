package com.simba.apppush.vivo.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 单推请求对象
 * 
 * @author caozhejun
 *
 */
public class SingleMessageRequest {

	/**
	 * 应用订阅PUSH 服务器得到的id 长度23 个 字符（regId，alias 两者需一个不为空，当两 个不为空时，取regId）
	 */
	private String regId;

	/**
	 * 别名长度不超过40 字符（regId，alias 两者 需一个不为空，当两个不为空时，取regId）
	 */
	private String alias;

	/**
	 * 通知类型1:无，2:响铃，3:振动，4:响铃和振 动
	 */
	private int notifyType = 4;

	/**
	 * 通知标题（用于通知栏消息） 最大20 个汉 字（一个汉字等于两个英文字符，即最大不 超过40 个英文字符）
	 */
	private String title;

	/**
	 * 通知内容（用于通知栏消息） 最大50 个汉 字（一个汉字等于两个英文字符，即最大不 超过100 个英文字符）
	 */
	private String content;

	/**
	 * 消息保留时长单位：秒，取值至少60 秒， 最长7 天。当值为空时，默认一天
	 */
	private int timeToLive = 86400;

	/**
	 * 点击跳转类型1：打开APP 首页2：打开链 接3：自定义4:打开app 内指定页面
	 */
	private int skipType = 1;

	/**
	 * 跳转内容跳转类型为2 时，跳转内容最大 1000 个字符，跳转类型为3 或4 时，跳转内 容最大1024 个字符
	 */
	private String skipContent;

	/**
	 * 网络方式-1：不限，1：wifi 下发送，不填 默认为-1
	 */
	private int networkType = -1;

	/**
	 * 客户端自定义键值对自定义key 和Value 键 值对个数不能超过10 个，且长度不能超过 1024 字符, key 和Value
	 * 键值对总长度不能 超过1024 字符。
	 */
	private Map<String, String> clientCustomMap = new HashMap<>();

	/**
	 * 高级特性
	 */
	private Map<String, String> extra = new HashMap<>();

	/**
	 * 用户请求唯一标识最大64 字符
	 */
	private String requestId;

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public int getNotifyType() {
		return notifyType;
	}

	public void setNotifyType(int notifyType) {
		this.notifyType = notifyType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(int timeToLive) {
		this.timeToLive = timeToLive;
	}

	public int getSkipType() {
		return skipType;
	}

	public void setSkipType(int skipType) {
		this.skipType = skipType;
	}

	public String getSkipContent() {
		return skipContent;
	}

	public void setSkipContent(String skipContent) {
		this.skipContent = skipContent;
	}

	public int getNetworkType() {
		return networkType;
	}

	public void setNetworkType(int networkType) {
		this.networkType = networkType;
	}

	public Map<String, String> getClientCustomMap() {
		return clientCustomMap;
	}

	public void setClientCustomMap(Map<String, String> clientCustomMap) {
		this.clientCustomMap = clientCustomMap;
	}

	public Map<String, String> getExtra() {
		return extra;
	}

	public void setExtra(Map<String, String> extra) {
		this.extra = extra;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SingleMessageRequest [regId=");
		builder.append(regId);
		builder.append(", alias=");
		builder.append(alias);
		builder.append(", notifyType=");
		builder.append(notifyType);
		builder.append(", title=");
		builder.append(title);
		builder.append(", content=");
		builder.append(content);
		builder.append(", timeToLive=");
		builder.append(timeToLive);
		builder.append(", skipType=");
		builder.append(skipType);
		builder.append(", skipContent=");
		builder.append(skipContent);
		builder.append(", networkType=");
		builder.append(networkType);
		builder.append(", clientCustomMap=");
		builder.append(clientCustomMap);
		builder.append(", extra=");
		builder.append(extra);
		builder.append(", requestId=");
		builder.append(requestId);
		builder.append("]");
		return builder.toString();
	}

}
