package com.simba.apppush.vivo.util;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.apppush.vivo.model.ListPayloadRequest;
import com.simba.apppush.vivo.model.PushAllRequest;
import com.simba.apppush.vivo.model.PushListRequest;
import com.simba.apppush.vivo.model.SingleMessageRequest;
import com.simba.apppush.vivo.model.VivoBaseResult;
import com.simba.framework.util.json.FastJsonUtil;

/**
 * vivo App推送工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class VivoAppPushUtil {

	@Autowired
	private VivoAccessTokenUtil vivoAccessTokenUtil;

	@Value("${vivo.app.title:}")
	private String title;

	public VivoAppPushUtil() {

	}

	public VivoAppPushUtil(VivoAccessTokenUtil vivoAccessTokenUtil, String title) {
		this.vivoAccessTokenUtil = vivoAccessTokenUtil;
		this.title = title;
	}

	/**
	 * 发送App推送
	 * 
	 * @param userId
	 * @param content
	 * @return
	 * @throws IOException
	 */
	public VivoBaseResult send(String userId, String content) throws IOException {
		SingleMessageRequest request = new SingleMessageRequest();
		request.setAlias(userId);
		request.setContent(content);
		request.setTitle(title);
		return this.singleSend(request);
	}

	/**
	 * 单播
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public VivoBaseResult singleSend(SingleMessageRequest request) throws IOException {
		return VivoPostUtil.postJson(getToken(), VivoConstantData.singleMessageUrl, FastJsonUtil.toJson(request));

	}

	/**
	 * 保存群推消息公共体
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public VivoBaseResult saveListPayLoad(ListPayloadRequest request) throws IOException {
		return VivoPostUtil.postJson(getToken(), VivoConstantData.saveListPayloadUrl, FastJsonUtil.toJson(request));
	}

	/**
	 * 批量推送用
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public VivoBaseResult pushList(PushListRequest request) throws IOException {
		return VivoPostUtil.postJson(getToken(), VivoConstantData.pushListUrl, FastJsonUtil.toJson(request));

	}

	/**
	 * 全量发送
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public VivoBaseResult pushAll(PushAllRequest request) throws IOException {
		return VivoPostUtil.postJson(getToken(), VivoConstantData.pushAllUrl, FastJsonUtil.toJson(request));

	}

	private String getToken() throws IOException {
		return vivoAccessTokenUtil.getAccessToken();
	}
}
