package com.simba.apppush.vivo.model;

public class VivoBaseResult {

	/**
	 * 接口调用是否成功的状态码0 成功，非0 失败
	 */
	private int result;

	/**
	 * 文字描述接口调用情况
	 */
	private String desc;

	/**
	 * 任务编号
	 */
	private String taskId;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VivoBaseResult [result=");
		builder.append(result);
		builder.append(", desc=");
		builder.append(desc);
		builder.append(", taskId=");
		builder.append(taskId);
		builder.append("]");
		return builder.toString();
	}

}
