package com.simba.apppush.vivo.model;

import java.util.Set;

/**
 * 批量推送用户请求对象
 * 
 * @author caozhejun
 *
 */
public class PushListRequest {

	/**
	 * regId 列表个数大于等于2，小于等于1000， regId 长度23 个字符（regIds，aliases 两者需
	 * 一个不为空，两个不为空，取regIds）
	 */
	private Set<String> regIds;

	/**
	 * 别名列表个数大于等于2，小于等于1000， 长度不超过40 字符（regIds，aliases 两者需 一个不为空，两个不为空，取regIds）
	 */
	private Set<String> aliases;

	/**
	 * 公共消息任务号，取saveListPayload 返回的 taskId
	 */
	private String taskId;

	/**
	 * 请求唯一标识，最大64 字符
	 */
	private String requestId;

	public Set<String> getRegIds() {
		return regIds;
	}

	public void setRegIds(Set<String> regIds) {
		this.regIds = regIds;
	}

	public Set<String> getAliases() {
		return aliases;
	}

	public void setAliases(Set<String> aliases) {
		this.aliases = aliases;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PushListRequest [regIds=");
		builder.append(regIds);
		builder.append(", aliases=");
		builder.append(aliases);
		builder.append(", taskId=");
		builder.append(taskId);
		builder.append(", requestId=");
		builder.append(requestId);
		builder.append("]");
		return builder.toString();
	}

}
