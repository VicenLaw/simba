package com.simba.apppush.vivo.util;

/**
 * vivo常量类
 * 
 * @author caozhejun
 *
 */
public interface VivoConstantData {

	/**
	 * vivo访问域名
	 */
	String vivoDomain = "https://api-push.vivo.com.cn";

	/**
	 * 推送鉴权接口
	 */
	String tokenUrl = vivoDomain + "/message/auth";

	/**
	 * 单推接口
	 */
	String singleMessageUrl = vivoDomain + "/message/send";

	/**
	 * 保存群推消息公共体接口
	 */
	String saveListPayloadUrl = vivoDomain + "/message/saveListPayload";

	/**
	 * 批量推送用户接口
	 */
	String pushListUrl = vivoDomain + "/message/pushToList";

	/**
	 * 全量发送接口
	 */
	String pushAllUrl = vivoDomain + "/message/all";

}
