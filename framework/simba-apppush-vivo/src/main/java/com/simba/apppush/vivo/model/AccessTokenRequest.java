package com.simba.apppush.vivo.model;

/**
 * 推送鉴权请求对象
 * 
 * @author caozhejun
 *
 */
public class AccessTokenRequest {

	/**
	 * 用户申请推送业务时生成的appId
	 */
	private int appId;

	/**
	 * 用户申请推送业务时获得的appKey
	 */
	private String appKey;

	/**
	 * Unix 时间戳做签名用，单位：毫秒，且在 vivo 服务器当前utc 时间戳前后十分钟区间 内
	 */
	private long timestamp;

	/**
	 * 签名使用MD5 算法，字符串trim 后拼接（ appId+appKey+timestamp+appSecret ） ， 然 后通过MD5
	 * 加密得到的值（字母小写）
	 */
	private String sign;

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessTokenRequest [appId=");
		builder.append(appId);
		builder.append(", appKey=");
		builder.append(appKey);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append(", sign=");
		builder.append(sign);
		builder.append("]");
		return builder.toString();
	}

}
