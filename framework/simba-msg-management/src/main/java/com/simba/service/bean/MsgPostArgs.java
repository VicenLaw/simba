package com.simba.service.bean;

/**
 * Created by linshuo on 2017/12/4.
 */
public class MsgPostArgs {

	/**
	 * 手机列表
	 */
	private String mobile;
	/**
	 * 时间戳+projectKey加密后的密文
	 */
	private String cipherText;
	/**
	 * 项目id
	 */
	private String projectId;

	/**
	 * 项目编码(项目id如果不为空，以id为准)
	 */
	private String projectCode;
	/**
	 * 时间戳
	 */
	private long timeStamp;
	/**
	 * 模板id
	 */
	private String templateSelfId;
	/**
	 * 插入模板中的值
	 */
	private String values;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCipherText() {
		return cipherText;
	}

	public void setCipherText(String cipherText) {
		this.cipherText = cipherText;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTemplateSelfId() {
		return templateSelfId;
	}

	public void setTemplateSelfId(String templateSelfId) {
		this.templateSelfId = templateSelfId;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MsgPostArgs [mobile=");
		builder.append(mobile);
		builder.append(", cipherText=");
		builder.append(cipherText);
		builder.append(", projectId=");
		builder.append(projectId);
		builder.append(", projectCode=");
		builder.append(projectCode);
		builder.append(", timeStamp=");
		builder.append(timeStamp);
		builder.append(", templateSelfId=");
		builder.append(templateSelfId);
		builder.append(", values=");
		builder.append(values);
		builder.append("]");
		return builder.toString();
	}

}
