package com.simba.service;

import java.util.Map;

import com.simba.model.MsgProject;
import com.simba.model.ShortMessage;
import com.simba.service.bean.MsgPostArgs;

/**
 * Created by linshuo on 2017/12/21.
 */
public interface SendMsgService {

	void sendSimply(String mobile, String selfTemplateId, Map<String, String> params, MsgProject project);

	void sendPure(String mobile, String selfTemplateId, Map<String, String> params);

	void checkAndSend(MsgPostArgs msgPostArgs, String ip);

	ShortMessage resend(long id);

}
