package com.simba.ip.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.cache.Redis;
import com.simba.framework.util.http.HttpClientUtil;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.ip.model.IpInfo;
import com.simba.ip.model.IpResult;

/**
 * ip地址查询地理信息工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class IpUtil {

	private static final Log logger = LogFactory.getLog(IpUtil.class);

	@Autowired
	private Redis redisUtil;

	private static final int timeout = 60 * 60;

	private static final String url = "http://ip.taobao.com/service/getIpInfo.php?ip=";

	private static final String[] innerIps = new String[] { "127", "192", "172" };

	/**
	 * 检查ip地址是否有效
	 * 
	 * @param ip
	 * @return
	 */
	public boolean check(String ip) {
		Pattern pattern = Pattern.compile(
				"\\b((?!\\d\\d\\d)\\d+|1\\d\\d|2[0-4]\\d|25[0-5])\\.((?!\\d\\d\\d)\\d+|1\\d\\d|2[0-4]\\d|25[0-5])\\.((?!\\d\\d\\d)\\d+|1\\d\\d|2[0-4]\\d|25[0-5])\\.((?!\\d\\d\\d)\\d+|1\\d\\d|2[0-4]\\d|25[0-5])\\b");
		Matcher m = pattern.matcher(ip);
		return m.matches();
	}

	/**
	 * 根据ip地址查询地址位置信息
	 * 
	 * @param ip
	 * @return
	 */
	public IpInfo getAdress(String ip) {
		if (!check(ip)) {
			logger.error("IP[" + ip + "]无效地址");
			return new IpInfo();
		}
		for (String innerIp : innerIps) {
			if (ip.startsWith(innerIp)) {
				return new IpInfo();
			}
		}
		String key = "ip_" + ip;
		IpResult result = (IpResult) redisUtil.get(key);
		if (result == null) {
			try {
				String resp = HttpClientUtil.get(url + ip);
				logger.info("根据IP" + ip + "获取地理信息返回:" + resp);
				if (StringUtils.isNotEmpty(resp)) {
					result = FastJsonUtil.toObject(resp, IpResult.class);
					redisUtil.set(key, result, timeout);
				} else {
					result = new IpResult();
				}
			} catch (Exception e) {
				logger.error("根据IP" + ip + "获取地理信息发生异常", e);
				return new IpInfo();
			}
		}
		return result.getData();
	}
}
