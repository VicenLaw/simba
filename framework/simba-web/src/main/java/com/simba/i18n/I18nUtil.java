package com.simba.i18n;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * 国际化工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class I18nUtil {

	@Autowired
	private MessageSource messageSource;

	public String getMessage(Locale locale, String code) {
		return messageSource.getMessage(code, new Object[] {}, locale);
	}

	public String getMessage(Locale locale, Object[] args, String code) {
		return messageSource.getMessage(code, args, locale);
	}

	public String getMessage(Object[] args, String code) {
		return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
	}

	public String getMessage(String code) {
		return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
	}

}
