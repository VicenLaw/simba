package com.simba.i18n;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.framework.util.applicationcontext.ApplicationContextInit;

/**
 * 国际化初始化完成之后执行
 * 
 * @author caozhejun
 *
 */
@Component
public class I18nInit implements ApplicationContextInit {

	private static final Log logger = LogFactory.getLog(I18nInit.class);

	@Autowired
	private I18nUtil i18nUtil;

	@Override
	public void init() {
		logger.info("服务器默认语言为:" + i18nUtil.getMessage("simba.language"));
	}

	@Override
	public int sort() {
		return 10000;
	}

}
