package com.simba.i18n;

import com.simba.framework.util.applicationcontext.ApplicationContextUtil;

/**
 * 国际化Exception，message会自动根据key从配置文件中获取
 * 
 * @author caozhejun
 *
 */
public class I18nException extends RuntimeException {

	private static final long serialVersionUID = -2320399934788651830L;

	private int code = 400;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public I18nException(String message) {
		super(getI18nMessage(message));
	}

	public I18nException(String message, int code) {
		super(getI18nMessage(message));
		this.code = code;
	}

	private static String getI18nMessage(String message) {
		I18nUtil i18nUtil = ApplicationContextUtil.getBean(I18nUtil.class);
		return i18nUtil.getMessage(message);
	}
}
