package com.simba.i18n;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import com.simba.model.constant.ConstantData;

/**
 * 国际化配置类
 *
 * @author caozhejun
 *
 */
@Component
public class I18NConfig {

	private static final Log logger = LogFactory.getLog(I18NConfig.class);

	@Value("${i18n.properties.dirs:}")
	private String i18nDirs;

	@Value("${i18n.default.language:}")
	private String language;

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}

	@Primary
	@Bean
	public MessageSource messageSource() {
		List<String> i18nDirList = new ArrayList<>();
		// i18n为国际化配置文件的目录,messages为国际化配置文件的前缀
		i18nDirList.add("classpath:i18n/messages");
		i18nDirList.add("classpath:simba/i18n/messages");
		if (StringUtils.isNotEmpty(i18nDirs)) {
			String[] i18ns = i18nDirs.split(",");
			for (String i18n : i18ns) {
				if (StringUtils.isNotEmpty(i18n)) {
					i18nDirList.add(i18n.trim() + "/messages");
				}
			}
		}
		int length = i18nDirList.size();
		String[] baseNames = new String[length];
		for (int i = 0; i < length; i++) {
			baseNames[i] = i18nDirList.get(i);
		}
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames(baseNames);
		messageSource.setDefaultEncoding(ConstantData.DEFAULT_CHARSET);
		return messageSource;
	}

	/**
	 * 自定义内部国际化解析类
	 * 
	 * @author caozhejun
	 *
	 */
	public class InnerLocaleResolver extends AcceptHeaderLocaleResolver {

		@Override
		public Locale resolveLocale(HttpServletRequest request) {
			String lang = request.getParameter("lang");
			Locale locale = null;
			if (StringUtils.isNotEmpty(lang)) {
				try {
					locale = LocaleUtils.toLocale(lang);
				} catch (Exception e) {
					logger.error("国际化获取语言发生异常", e);
				}
			}
			return locale != null ? locale : super.resolveLocale(request);
		}

		@Override
		public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

		}
	}

	@Bean
	public LocaleResolver localeResolver() {
		Locale locale = null;
		if (StringUtils.isEmpty(language)) {
			locale = Locale.CHINA;
		} else {
			locale = new Locale(language);
		}
		InnerLocaleResolver localeResolver = new InnerLocaleResolver();
		localeResolver.setDefaultLocale(locale);
		return localeResolver;
	}

}
