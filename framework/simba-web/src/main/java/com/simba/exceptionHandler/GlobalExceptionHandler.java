package com.simba.exceptionHandler;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.simba.exception.BussException;
import com.simba.exception.ErrorCodeException;
import com.simba.exception.ForbidException;
import com.simba.exception.LoginException;
import com.simba.framework.util.common.ExceptionUtil;
import com.simba.framework.util.common.ServerUtil;
import com.simba.framework.util.common.SystemUtil;
import com.simba.framework.util.json.JsonResult;
import com.simba.i18n.I18nException;
import com.simba.registry.util.RegistryUtil;
import com.simba.util.EmailUtil;

/**
 * 统一异常处理
 * 
 * @author caozhejun
 *
 */
@ControllerAdvice
class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Log logger = LogFactory.getLog(GlobalExceptionHandler.class);

	public static final String DEFAULT_ERROR_VIEW = "error";

	@Value("${page.login:login}")
	private String loginPage;

	@Value("${page.error:error/error}")
	private String errorPage;

	@Value("${page.forbid:error/forbid}")
	private String forbidPage;

	@Resource
	private TaskExecutor taskExecutor;

	@Autowired
	private EmailUtil emailUtil;

	@PostConstruct
	private void init() {
		loginPage = StringUtils.defaultIfEmpty(loginPage, "login");
		errorPage = StringUtils.defaultIfEmpty(errorPage, "error/error");
		forbidPage = StringUtils.defaultIfEmpty(forbidPage, "error/forbid");
	}

	@ExceptionHandler(value = Throwable.class)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		logger.error("发生异常", e);
		dealExceptionAlarmAsync(e, req);
		ModelAndView model = new ModelAndView();
		if (isJsonException(req)) {
			dealJsonDataException(req, e, model);
		} else if (e instanceof LoginException) {
			model.addObject("top", true);
			model.setViewName(loginPage);
		} else if (e instanceof ForbidException) {
			model.setViewName(forbidPage);
		} else if (e instanceof BussException) {
			model.setViewName("error/business");
			model.addObject("message", e.getMessage());
		} else if (e instanceof ConstraintViolationException) {
			ConstraintViolationException ex = (ConstraintViolationException) e;
			String errMsg = ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage).findFirst().get();
			model.setViewName(errorPage);
			model.addObject("message", errMsg);
		} else {
			model.setViewName(errorPage);
			model.addObject("message", ExceptionUtil.getStackTrace(e));
		}
		return model;
	}

	protected void dealJsonDataException(HttpServletRequest req, Exception e, ModelAndView model) {
		model.setViewName("message");
		String message = null;
		if (e instanceof ErrorCodeException) {
			message = new JsonResult(e.getMessage(), ((ErrorCodeException) e).getCode()).toJson();
		} else if (e instanceof I18nException) {
			message = new JsonResult(e.getMessage(), ((I18nException) e).getCode()).toJson();
		} else if (e instanceof ConstraintViolationException) {
			ConstraintViolationException ex = (ConstraintViolationException) e;
			String errMsg = ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage).findFirst().get();
			message = new JsonResult(errMsg, 400).toJson();
		} else {
			message = new JsonResult(e.getMessage(), 400).toJson();
		}
		String callback = req.getParameter("callback");
		String jsonp = req.getParameter("jsonp");
		if (StringUtils.isNotEmpty(callback)) {
			message = callback + "(" + message + ")";
		} else if (StringUtils.isNotEmpty(jsonp)) {
			message = jsonp + "(" + message + ")";
		}
		model.addObject("message", message);
	}

	/**
	 * 处理异常警报邮件
	 * 
	 * @param e
	 * @param req
	 */
	protected void dealExceptionAlarmAsync(Exception e, HttpServletRequest req) {
		taskExecutor.execute(() -> {
			try {
				dealExceptionAlarm(e, req);
			} catch (UnknownHostException e1) {
				logger.error("发送异常邮件发生异常", e1);
			}
		});
	}

	/**
	 * 处理异常警报邮件
	 * 
	 * @param e
	 * @param req
	 * @throws UnknownHostException
	 */
	protected void dealExceptionAlarm(Exception e, HttpServletRequest req) throws UnknownHostException {
		boolean alarmAnabled = true;
		if (!"true".equals(RegistryUtil.get("alarm.exception.email.enabled"))) {
			alarmAnabled = false;
		}
		if (!alarmAnabled) {
			return;
		}
		String alarmEmail = RegistryUtil.get("alarm.email");
		String includeExceptionClass = RegistryUtil.get("alarm.exception");
		String excludeExceptionClass = RegistryUtil.get("alarm.exclude.exception");
		List<String> includeExceptionClassList = null;
		List<String> excludeExceptionClassList = null;
		List<String> alarmEmailList = null;
		if (StringUtils.isEmpty(includeExceptionClass) || "all".equalsIgnoreCase(includeExceptionClass)) {
			includeExceptionClassList = new ArrayList<>(0);
		} else {
			String[] classes = includeExceptionClass.split(",");
			includeExceptionClassList = new ArrayList<>(classes.length);
			for (String className : classes) {
				if (StringUtils.isNotEmpty(className)) {
					includeExceptionClassList.add(className);
				}
			}
		}
		if (StringUtils.isEmpty(excludeExceptionClass) || "none".equalsIgnoreCase(excludeExceptionClass)) {
			excludeExceptionClassList = new ArrayList<>(0);
		} else {
			String[] classes = excludeExceptionClass.split(",");
			excludeExceptionClassList = new ArrayList<>(classes.length);
			for (String className : classes) {
				if (StringUtils.isNotEmpty(className)) {
					excludeExceptionClassList.add(className);
				}
			}
		}
		if (StringUtils.isEmpty(alarmEmail)) {
			alarmEmailList = new ArrayList<>(0);
		} else {
			String[] emails = alarmEmail.split(",");
			alarmEmailList = new ArrayList<>(emails.length);
			for (String email : emails) {
				if (StringUtils.isNotEmpty(email)) {
					alarmEmailList.add(email);
				}
			}
		}
		if (alarmAnabled && alarmEmailList.size() > 0) {
			String name = e.getClass().getName();
			if (includeExceptionClassList.size() == 0 || includeExceptionClassList.contains(name)) {
				if (excludeExceptionClassList.size() == 0 || !excludeExceptionClassList.contains(name)) {
					sendExceptionEmail(e, alarmEmailList, req);
				}
			}
		}
	}

	/**
	 * 发送异常邮件
	 * 
	 * @param e
	 * @param alarmEmailList
	 * @param req
	 * @throws UnknownHostException
	 */
	protected void sendExceptionEmail(Exception e, List<String> alarmEmailList, HttpServletRequest req) throws UnknownHostException {
		String message = ExceptionUtil.getStackTrace(e);
		String projectName = RegistryUtil.get("spring.application.name");
		String machineName = SystemUtil.getMachineName();
		String serverIp = SystemUtil.getIpAddress();
		String clientIp = ServerUtil.getProxyIp(req);
		String title = "系统发生异常[项目编号:" + projectName + "][机器名:" + machineName + "][服务器IP地址:" + serverIp + "][客户端IP:" + clientIp + "][异常消息:" + e.getMessage() + "]";
		alarmEmailList.forEach((email) -> {
			emailUtil.send(email, title, message);
		});
	}

	/**
	 * 判断是否是json格式请求，如果是json格式请求，返回json格式错误信息
	 * 
	 * @param request
	 * @return
	 */
	protected boolean isJsonException(HttpServletRequest request) {
		boolean isJson = false;
		if (request.getParameter("json") != null || request.getParameter("jsonp") != null || (request.getHeader("Accept") != null && request.getHeader("Accept").indexOf("json") > -1
				|| request.getRequestURI().endsWith(".json") || request.getRequestURI().endsWith(".jsonp"))) {
			isJson = true;
		}
		return isJson;
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.error("发生异常", ex);
		String errMsg = ex.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).findFirst().get();
		return handleExceptionInternal(ex, new JsonResult(null, "参数无效" + ":" + errMsg, 10000), headers, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.error("发生异常", ex);
		String errMsg = ex.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage).findFirst().get();
		return handleExceptionInternal(ex, new JsonResult(null, "参数绑定异常" + ":" + errMsg, 10001), headers, status, request);
	}

}