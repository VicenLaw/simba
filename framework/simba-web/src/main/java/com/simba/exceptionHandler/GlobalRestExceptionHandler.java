package com.simba.exceptionHandler;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

/**
 * 统一异常处理
 * 
 * @author caozhejun
 *
 */
@RestControllerAdvice
class GlobalRestExceptionHandler extends GlobalExceptionHandler {

	private static final Log logger = LogFactory.getLog(GlobalRestExceptionHandler.class);

	@ExceptionHandler(value = Throwable.class)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		logger.error("发生异常", e);
		dealExceptionAlarmAsync(e, req);
		ModelAndView model = new ModelAndView();
		dealJsonDataException(req, e, model);
		return model;
	}

}