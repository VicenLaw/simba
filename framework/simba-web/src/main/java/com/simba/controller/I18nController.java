package com.simba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simba.framework.util.json.JsonResult;
import com.simba.i18n.I18nUtil;

/**
 * 国际化Controller
 * 
 * @author caozhejun
 *
 */
@RestController
@RequestMapping("/api/i18n")
public class I18nController {

	@Autowired
	private I18nUtil i18nUtil;

	@GetMapping("/get")
	public JsonResult get(String code) {
		return new JsonResult(i18nUtil.getMessage(code));
	}
}
