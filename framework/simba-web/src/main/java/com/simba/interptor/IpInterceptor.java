package com.simba.interptor;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.simba.exception.ForbidException;
import com.simba.framework.util.common.PathUtil;
import com.simba.framework.util.common.ServerUtil;
import com.simba.framework.util.request.RequestUtil;
import com.simba.registry.util.RegistryUtil;

/**
 * IP拦截器
 * 
 * @author caozj
 */
public class IpInterceptor implements HandlerInterceptor {

	private static final Log logger = LogFactory.getLog(IpInterceptor.class);

	public IpInterceptor() {
		super();
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String excludeUrl = RegistryUtil.get("ip.interceptor.exclude.url");
		String excludeIp = RegistryUtil.get("ip.interceptor.exclude.ip");
		String[] urls = excludeUrl.split(",");
		Set<String> excludeUrls = new HashSet<>(urls.length);
		for (String url : urls) {
			if (StringUtils.isNotEmpty(url)) {
				excludeUrls.add(url.trim());
			}
		}
		String[] ips = excludeIp.split(",");
		Set<String> excludeIps = new HashSet<>(ips.length);
		for (String ip : ips) {
			if (StringUtils.isNotEmpty(ip)) {
				excludeIps.add(ip.trim());
			}
		}
		logger.debug("可以访问的ip为" + excludeIp + ",url为" + excludeUrl);
		String requestUri = RequestUtil.getRequestUri(request);
		for (String url : excludeUrls) {
			if (PathUtil.match(requestUri, url)) {
				return true;
			}
		}
		String ip = ServerUtil.getProxyIp(request);
		for (String exIp : excludeIps) {
			if (exIp.equals(ip)) {
				return true;
			}
		}
		throw new ForbidException("您没有权限访问,所以不能访问" + requestUri + ",ip:" + ip);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

}
