package com.simba.interptor;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.simba.common.EnvironmentUtil;
import com.simba.exception.ForbidException;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.framework.util.common.PathUtil;
import com.simba.framework.util.common.ServerUtil;
import com.simba.framework.util.request.RequestUtil;

/**
 * 允许访问拦截器
 * 
 * @author caozj
 */
public class AllowInterceptor implements HandlerInterceptor {

	private static final Log logger = LogFactory.getLog(AllowInterceptor.class);

	public AllowInterceptor() {
		super();
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		EnvironmentUtil environmentUtil = ApplicationContextUtil.getBean(EnvironmentUtil.class);
		String includeUrl = environmentUtil.get("allow.interceptor.include.url");
		if (StringUtils.isEmpty(includeUrl)) {
			return true;
		}
		String[] urls = includeUrl.split(",");
		Set<String> includeUrls = new HashSet<>(urls.length);
		for (String url : urls) {
			if (StringUtils.isNotEmpty(url)) {
				includeUrls.add(url.trim());
			}
		}
		logger.debug("允许访问的url为" + includeUrl);
		String requestUri = RequestUtil.getRequestUri(request);
		for (String url : includeUrls) {
			if (PathUtil.match(requestUri, url)) {
				return true;
			}
		}
		throw new ForbidException(ServerUtil.getProxyIp(request) + "不能访问" + requestUri);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

}
