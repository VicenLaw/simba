package com.simba.interptor;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.simba.exception.ForbidException;
import com.simba.framework.util.common.ServerUtil;
import com.simba.framework.util.request.RequestUtil;
import com.simba.registry.util.RegistryUtil;

/**
 * IP黑名单拦截器
 * 
 * @author caozj
 */
public class BlackInterceptor implements HandlerInterceptor {

	private static final Log logger = LogFactory.getLog(BlackInterceptor.class);

	public BlackInterceptor() {
		super();
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String denyIp = RegistryUtil.get("black.interceptor.deny.ips");
		if (StringUtils.isEmpty(denyIp)) {
			return true;
		}
		String[] ips = denyIp.split(",");
		Set<String> denyIps = new HashSet<>(ips.length);
		for (String ip : ips) {
			if (StringUtils.isNotEmpty(ip)) {
				denyIps.add(ip.trim());
			}
		}
		logger.debug("禁止访问的黑名单为" + denyIp);
		String requestUri = RequestUtil.getRequestUri(request);
		String ip = ServerUtil.getProxyIp(request);
		if (denyIps.contains(ip)) {
			throw new ForbidException("您已经进入黑名单,所以不能访问" + requestUri + ",ip:" + ip);
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

}
