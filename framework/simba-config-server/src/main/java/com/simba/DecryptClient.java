package com.simba;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

/**
 * 加密客户端
 * 
 * @author 杨恩雄
 *
 */
public class DecryptClient {

	/**
	 * 使用加密和解密端点的时候，请注释掉这个依赖：spring-boot-starter-security
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory
				.getILoggerFactory();
		loggerContext.getLogger("root").setLevel(Level.toLevel("INFO"));
		// 创建默认的HttpClient
		CloseableHttpClient httpclient = HttpClients.createDefault();
		// 调用 POST 方法请求服务
		HttpPost httpMethod = new HttpPost("http://127.0.0.1:7001/decrypt");
		HttpEntity entity = new StringEntity(
				"1c96518bf3dab930e54ed6d72919b46e27aa986703fc0402d58a8cd1948e1e15",
				Consts.UTF_8);
		httpMethod.setEntity(entity);
		// 获取响应
		HttpResponse response = httpclient.execute(httpMethod);
		// 根据 响应解析出字符串
		System.out.println(EntityUtils.toString(response.getEntity()));
	}
}
