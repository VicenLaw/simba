package com.simba.framework.util.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.simba.framework.util.code.AESUtil;
import com.simba.framework.util.code.EncryptUtil;
import com.simba.model.constant.ConstantData;

/**
 * 配置文件的值解密
 * 
 * @author caozhejun
 *
 */
public class PropertyDecodeUtil {

	private static final Log logger = LogFactory.getLog("配置文件解密");

	/**
	 * 解密
	 * 
	 * @param value
	 * @return
	 */
	public static String resolveValue(String value) {
		int length = ConstantData.MISPLIT.length();
		String result = value.substring(length);
		int index = result.indexOf(ConstantData.MISPLIT);
		if (index == -1) {
			return value;
		}
		String key = result.substring(0, index);
		key = ConstantData.PREFIX + key + ConstantData.SUFFIX;
		key = EncryptUtil.md5(key);
		String text = result.substring(index + length);
		try {
			value = AESUtil.decrypt(text, key);
		} catch (Exception e) {
			logger.error("*********配置文件解密发生异常**********" + value + "," + key + "," + text, e);
		}
		return value;
	}
}
