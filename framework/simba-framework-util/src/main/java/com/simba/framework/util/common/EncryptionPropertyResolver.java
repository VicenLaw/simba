package com.simba.framework.util.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.simba.model.constant.ConstantData;
import com.ulisesbocchio.jasyptspringboot.EncryptablePropertyResolver;

/**
 * 配置文件解密实现类
 * 
 * @author caozhejun
 *
 */
@Component("encryptablePropertyResolver")
public class EncryptionPropertyResolver implements EncryptablePropertyResolver {

	@Override
	public String resolvePropertyValue(String value) {
		if (StringUtils.isEmpty(value)) {
			return value;
		}
		value = value.trim();
		if (value.startsWith(ConstantData.MISPLIT)) {
			return PropertyDecodeUtil.resolveValue(value);
		}
		return value;
	}

}
