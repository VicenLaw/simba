package com.simba.framework.util.common;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.simba.framework.util.code.AESUtil;
import com.simba.framework.util.code.EncryptUtil;
import com.simba.framework.util.data.RandomUtil;
import com.simba.framework.util.file.PropertiesUtil;
import com.simba.model.constant.ConstantData;

/**
 * 配置文件加密工具类
 * 
 * @author caozhejun
 *
 */
public class PropertyEncryptUtil {

	private static final Log logger = LogFactory.getLog("配置文件加密");

	private static Set<String> ignoreSet = new HashSet<>();

	private static Set<String> ignorePatternSet = new HashSet<>();

	static {
		ignoreSet.add("spring.output.ansi.enabled");
		ignoreSet.add("server.port");
		ignoreSet.add("spring.datasource.initialSize");
		ignoreSet.add("spring.datasource.minIdle");
		ignoreSet.add("spring.datasource.maxActive");
		ignoreSet.add("spring.datasource.maxWait");
		ignoreSet.add("spring.datasource.timeBetweenEvictionRunsMillis");
		ignoreSet.add("spring.datasource.minEvictableIdleTimeMillis");
		ignoreSet.add("spring.redis.timeout");
		ignoreSet.add("hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds");
		ignoreSet.add("ribbin.ReadTimeout");
		ignoreSet.add("ribbon.ConnectTimeout");
		ignoreSet.add("ribbon.MaxAutoRetries");
		ignoreSet.add("server.session.timeout");
		ignoreSet.add("spring.redis.database");
		ignoreSet.add("spring.redis.port");
		ignoreSet.add("spring.rabbitmq.port");
		ignoreSet.add("spring.elasticsearch.jest.read-timeout");
		ignoreSet.add("logging.level.*");
		ignoreSet.add("spring.http.encoding.charset");
		ignoreSet.add("server.tomcat.uri-encoding");
		ignoreSet.add("spring.freemarker.charset");
		ignoreSet.add("spring.profiles.active");
		ignoreSet.add("server.context-path");
		ignoreSet.add("spring.http.encoding.force");
		ignoreSet.add("spring.application.name");
		ignoreSet.add("spring.cloud.config.failFast");
		ignoreSet.add("spring.freemarker.settings.number_format");
		ignoreSet.add("spring.http.multipart.maxFileSize");
		ignoreSet.add("spring.http.multipart.maxRequestSize");
		ignoreSet.add("feign.compression.request.enabled");
		ignoreSet.add("feign.compression.response.enabled");
		ignoreSet.add("feign.compression.request.min-request-size");
		ignoreSet.add("groovy.source.encoding");
		ignoreSet.add("file.encoding");
		ignoreSet.add("spring.freemarker.settings.number_format");
		ignoreSet.add("spring.freemarker.cache");
		ignoreSet.add("eureka.instance.prefer-ip-address");
		ignoreSet.add("info.version");
		ignoreSet.add("spring.sleuth.sampler.percentage");
		ignoreSet.add("spring.mail.properties.mail.smtp.socketFactory.class");
		ignoreSet.add("spring.mail.properties.mail.smtp.socketFactory.port");
		ignoreSet.add("spring.mail.properties.mail.smtp.auth");
		ignoreSet.add("spring.mail.properties.mail.smtp.starttls.enable");
		ignoreSet.add("spring.mail.properties.mail.smtp.starttls.required");
		ignoreSet.add("spring.mail.default-encoding");
		ignoreSet.add("spring.redis.pool.max-active");
		ignoreSet.add("spring.redis.pool.max-wait");
		ignoreSet.add("spring.redis.pool.max-idle");
		ignoreSet.add("spring.redis.pool.min-idle");
		ignoreSet.add("spring.freemarker.check-template-location");
		ignoreSet.add("spring.freemarker.content-type");
		ignoreSet.add("spring.freemarker.expose-request-attributes");
		ignoreSet.add("spring.freemarker.expose-session-attributes");
		ignoreSet.add("spring.freemarker.request-context-attribute");
		ignoreSet.add("spring.freemarker.prefer-file-system-access");
		ignoreSet.add("management.security.enabled");
		ignoreSet.add("distribute.type");
		ignoreSet.add("logging.file");
		ignoreSet.add("dbType");
		ignoreSet.add("object.storage");
		ignoreSet.add("session.expired.time");
		ignoreSet.add("org.ext");
		ignoreSet.add("user.ext");
		ignoreSet.add("aliyun.msg.sign");
		ignoreSet.add("code.generate.package");
		ignoreSet.add("project.name");
		ignoreSet.add("aliyun.msg.sign");

		ignoreSet.add("project.");
		ignorePatternSet.add("custom.");
		ignorePatternSet.add("page.");
		ignorePatternSet.add("swagger.");

	}

	/**
	 * 增加不需要加密的key
	 * 
	 * @param key
	 */
	public static void addIgnoreKey(String key) {
		ignoreSet.add(key);
	}

	/**
	 * 增加不需要加密的key
	 * 
	 * @param keys
	 */
	public static void addIgnoreKey(String[] keys) {
		for (String key : keys) {
			ignoreSet.add(key);
		}
	}

	/**
	 * 增加不需要以key开头的加密
	 * 
	 * @param key
	 */
	public static void addIgnoreStartKey(String key) {
		ignorePatternSet.add(key);
	}

	/**
	 * 增加不需要以key开头的加密
	 * 
	 * @param keys
	 */
	public static void addIgnoreStartKey(String[] keys) {
		for (String key : keys) {
			ignorePatternSet.add(key);
		}
	}

	/**
	 * 加密配置文件
	 * 
	 * @param fileNames
	 * @throws IOException
	 */
	public static void encrypt(List<String> fileNames) throws IOException {
		for (String fileName : fileNames) {
			if (new File(fileName).exists()) {
				encrypt(fileName);
			} else {
				logger.error("配置文件不存在" + fileName);
			}
		}
	}

	/**
	 * 加密配置文件
	 * 
	 * @param fileName
	 * @throws IOException
	 */
	public static void encrypt(String fileName) throws IOException {
		Map<String, String> propertiesMap = PropertiesUtil.readFileByOrder(fileName);
		Map<String, String> resultMap = new LinkedHashMap<>(propertiesMap.size());
		propertiesMap.forEach((key, value) -> {
			value = value.trim();
			if (StringUtils.isNotEmpty(value) && !ignoreKey(key) && !value.startsWith(ConstantData.MINGSPLIT) && !value.startsWith(ConstantData.MISPLIT)) {
				value = ConstantData.MINGSPLIT + RandomUtil.random32Chars() + ConstantData.MINGSPLIT + value;
			}
			if (value.startsWith(ConstantData.MINGSPLIT)) {
				value = encryptValue(value);
			}
			resultMap.put(key, value);
		});
		PropertiesUtil.saveFileByOrder(fileName, resultMap);
		logger.info("配置文件" + fileName + "加密完成");
	}

	/**
	 * 判断是否需要忽略加密的key
	 * 
	 * @param key
	 * @return
	 */
	private static boolean ignoreKey(String key) {
		boolean ignore = ignoreSet.contains(key);
		if (ignore) {
			return ignore;
		}
		for (String ignorePattern : ignorePatternSet) {
			if (key.startsWith(ignorePattern)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 加密字符串
	 * 
	 * @param value
	 * @return
	 */
	private static String encryptValue(String value) {
		int length = ConstantData.MINGSPLIT.length();
		String result = value.substring(length);
		int index = result.indexOf(ConstantData.MINGSPLIT);
		if (index == -1) {
			return value;
		}
		String key = result.substring(0, index);
		String orginalKey = key;
		key = ConstantData.PREFIX + key + ConstantData.SUFFIX;
		key = EncryptUtil.md5(key);
		String text = result.substring(index + length);
		try {
			value = AESUtil.encrypt(text, key);
			value = ConstantData.MISPLIT + orginalKey + ConstantData.MISPLIT + value;
		} catch (Exception e) {
			logger.error("==============配置文件加密发生异常==============" + value);
		}
		return value;
	}

}
