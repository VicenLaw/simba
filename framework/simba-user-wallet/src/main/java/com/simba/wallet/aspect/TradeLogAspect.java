package com.simba.wallet.aspect;

import com.simba.framework.util.json.JsonResult;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 日志切面
 *
 * @author zhangfenghua
 */
@Aspect
@Component
public class TradeLogAspect {

    ThreadLocal<Long> startTime = new ThreadLocal<>();
    private Logger logger = Logger.getLogger(getClass());

    @Pointcut("execution(public * com.simba.wallet.pay..*.*(..))")
    public void tradeLog() {
    }

    @Before("tradeLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        startTime.set(System.currentTimeMillis());

        logger.info("[WALLET] invoke method: " + joinPoint.getSignature()
            .getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("[WALLET] ARGS : " + Arrays.toString(joinPoint.getArgs()));

    }

    @AfterReturning(returning = "ret", pointcut = "tradeLog()")
    public void doAfterReturning(Object ret) throws Throwable {
        StringBuffer rsp = new StringBuffer();
        if(ret instanceof JsonResult) {
            JsonResult nRet = (JsonResult) ret;
            rsp.append("code: "+ nRet.getCode()).append(", data: "+ nRet.getData()).append(", msg: "+ nRet.getMsg());
        }else {
            rsp.append(ret);
        }
        logger.info("[WALLET] response : " + rsp);
        logger.info("[WALLET] time speed : " + (System.currentTimeMillis() - startTime.get
            ()));
    }

    @AfterThrowing(pointcut = "tradeLog()", throwing = "ex")
    public void doAfterThrowingRuntimeException(RuntimeException ex) {
        logger.error("[WALLET] occur exception: " + ex);
    }

}

