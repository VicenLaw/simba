package com.simba.wallet.util;

import com.relops.snowflake.Snowflake;
import com.simba.framework.util.common.UUIDUtil;
import com.simba.framework.util.date.DateUtil;
import com.simba.wallet.model.TradeAccount;
import com.simba.wallet.model.TradeUser;
import com.simba.wallet.util.Constants.AccountActiveStatus;
import com.simba.wallet.util.Constants.AccountFrozenStatus;
import com.simba.wallet.util.Constants.AccountType;
import com.simba.wallet.util.Constants.TradePayment;
import com.simba.wallet.util.Constants.TradeRechargement;
import com.simba.wallet.util.Constants.TradeUserType;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CommonUtil {

    public static String transToCNYType(Long amount) {
        return transToCNYTypeWithouUnit(amount) + "元";
    }

    public static String transToCNYTypeWithouUnit(Long amount) {
        DecimalFormat fmt = new DecimalFormat("0.00");
        return fmt.format(amount * 1.0 / 100);
    }

    public static String transToCNYType(Long amount, boolean addMinus) {
        DecimalFormat fmt = new DecimalFormat("0.00");
        if (addMinus) {
            amount = amount * -1;
        }
        return fmt.format(amount * 1.0 / 100);
    }

    public static Long CNYToLong(String amount) throws ParseException {
        DecimalFormat fmt = new DecimalFormat("0.00");
        return (long) (fmt.parse(amount).doubleValue() * 100);
    }

    public static AccountActiveStatus checkAccountActive(TradeAccount tradeAccount) {
        return AccountActiveStatus.get(tradeAccount.getIsActive());

    }

    public static AccountFrozenStatus checkAccountFrozen(TradeAccount tradeAccount) {
        return AccountFrozenStatus.get(tradeAccount.getIsFrozen());
    }

    public static AccountActiveStatus checkTradeUserActive(TradeUser tradeUser) {
        return AccountActiveStatus.get(tradeUser.getIsActive());
    }

    public static TradePayment checkTradeUserPayment(TradeUser tradeUser) {
        return TradePayment.get(tradeUser.getIsAllowPay());
    }

    public static TradePayment checkAccountPayment(TradeAccount tradeAccount) {

        return TradePayment.get(tradeAccount.getIsAllowPay());
    }

    private static TradeRechargement checkAccountRechargement(TradeAccount tradeAccount) {
        return TradeRechargement.get(tradeAccount.getIsAllowRecharge());
    }

    public static String getUserStatus(TradeUser tradeUser) {
        return checkTradeUserActive(tradeUser) == AccountActiveStatus.ACTIVE ? "已激活" : "已注销";
    }

    public static String getAccountStatus(TradeAccount tradeAccount) {
        AccountActiveStatus activeStatus = checkAccountActive(tradeAccount);
        AccountFrozenStatus frozenStatus = checkAccountFrozen(tradeAccount);
        if (activeStatus == AccountActiveStatus.ACTIVE) {
            return frozenStatus == AccountFrozenStatus.FROZEN ? "已冻结" : "账户正常";
        } else {
            return "已注销";
        }
    }

    public static AccountType getAccountType(TradeUserType tradeUserType) {
        return AccountType.get(tradeUserType);
    }

    public static AccountType getAccountType(String accountType) {
        return AccountType.get(accountType);
    }

    public static Map<String, String> fmtBalance(Map<String, Object> balanceMap) {
        Map<String, String> map = new HashMap<String, String>();
        if (balanceMap.get("accountBalance") != null) {
            map.put("accountBalance", CommonUtil
                .transToCNYType(Long.parseLong(balanceMap.get("accountBalance").toString())));
        }
        if (balanceMap.get("availableBalance") != null) {
            map.put("availableBalance", CommonUtil
                .transToCNYType(Long.parseLong(balanceMap.get("availableBalance").toString())));
        }
        if (balanceMap.get("virtualBalance") != null) {
            map.put("virtualBalance", CommonUtil
                .transToCNYType(Long.parseLong(balanceMap.get("virtualBalance").toString())));
        }
        if (balanceMap.get("frozenBalance") != null) {
            map.put("frozenBalance", CommonUtil
                .transToCNYType(Long.parseLong(balanceMap.get("frozenBalance").toString())));
        }
        return map;
    }


    public static String generateOrderNO() {
        return "r" + UUIDUtil.get();
    }

    public static void checkWalletAutority(TradeUser tradeUser, TradeAccount tradeAccount,
        TradeUserType tradeUserType) {
        if (checkAccountActive(tradeAccount) != AccountActiveStatus.ACTIVE
            || checkTradeUserActive(tradeUser) != AccountActiveStatus.ACTIVE) {
            if (tradeUserType == TradeUserType.PERSION) {
                throw ErrConfig.INVALID_WALLET_USER;
            } else {
                throw ErrConfig.WALLET_UNAVAILABLE;
            }
        }

        if (checkAccountFrozen(tradeAccount) == AccountFrozenStatus.FROZEN) {
            if (tradeUserType == TradeUserType.PERSION) {
                throw ErrConfig.WALLET_FROZEN;
            } else {
                throw ErrConfig.WALLET_UNAVAILABLE;
            }
        }

        if (tradeUserType == TradeUserType.PERSION) {
            if (checkTradeUserPayment(tradeUser) == TradePayment.NOTALLOWPAY
                || checkAccountPayment(tradeAccount) == TradePayment.NOTALLOWPAY) {
                throw ErrConfig.WALLET_NOT_ALLOWPAY;
            }
            if (checkAccountRechargement(tradeAccount) == TradeRechargement.NOTALLOWRECHARGE) {
                {
                    throw ErrConfig.WALLET_NOT_ALLOWRECHARGE;
                }
            }
        }
    }

    public static Date getGmtDate(String gmtDate) {
        if (gmtDate == null) {
            return DateUtil.str2Date("1979-01-01", DateUtil.DAY_FORMAT, new Date());
        }
        Date date = null;
        try {
            date = DateUtil.str2Date(gmtDate, "yyyy-MM-dd HH:mm:ss");
        } catch (Exception e) {
            date = DateUtil.str2Date(gmtDate, "yyyyMMddHHmmss");
        }
        return date;
    }

    public static long currTimeMillis() {
        return System.currentTimeMillis();
    }

    public static class TradeNoGenerator {

        public static Snowflake snowflake = null;

        public static long gen(Integer nodeId) {
            if (snowflake == null) {
                synchronized (TradeNoGenerator.class) {
                    if (snowflake == null) {
                        snowflake = new Snowflake(nodeId);
                    }
                }
            }
            return snowflake.next();
        }
    }

}
