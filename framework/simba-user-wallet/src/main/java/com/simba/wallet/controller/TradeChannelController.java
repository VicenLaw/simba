package com.simba.wallet.controller;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.wallet.model.vo.TradeChannelVO;
import com.simba.wallet.service.TradeAccountService;
import com.simba.wallet.util.CommonUtil;
import com.simba.wallet.util.Constants.ChannelType;
import com.simba.wallet.util.Constants.TradeUserType;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 渠道信息控制器
 *
 * @author caozj
 */
@Controller
@RequestMapping("/tradeChannel")
public class TradeChannelController {

    private static final Log logger = LogFactory.getLog(TradeChannelController.class);


    @Autowired
    private TradeAccountService tradeAccountService;

    @RequestMapping("/list")
    public String list() {
        return "tradeChannel/list";
    }

    @RequestMapping("/getList")
    public String getList(Pager pager, ModelMap model) {
        ChannelType[] channelTypes = ChannelType.values();
        List<TradeChannelVO> tradeChannelVOList = new ArrayList<>(channelTypes.length);
        for (ChannelType channel : channelTypes) {
            String accountStatus = StringUtils.EMPTY;
            try {
                accountStatus = CommonUtil.getAccountStatus(tradeAccountService.get(channel.getId(),
                    TradeUserType.CHANNEL));
            } catch (Exception e) {
                logger.error("获取钱包账号状态发生异常", e);
            }
            TradeChannelVO vo = new TradeChannelVO();
            vo.setId(channel.getId());
            vo.setType(channel.name());
            vo.setAccountStatus(accountStatus);
            tradeChannelVOList.add(vo);
        }
        model.put("list", tradeChannelVOList);
        return "tradeChannel/table";
    }

    @ResponseBody
    @RequestMapping("/count")
    public JsonResult count() {
        return new JsonResult(ChannelType.values().length, "", 200);

    }
}
