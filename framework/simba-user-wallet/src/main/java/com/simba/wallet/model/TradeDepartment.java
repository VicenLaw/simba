package com.simba.wallet.model;
/***********************************************************************
 * Module: TradeDepartment.java Author: zhangfenghua Purpose: Defines the Class TradeDepartment
 ***********************************************************************/

import java.util.Date;


public class TradeDepartment {


    private long id;

    /**
     * 部门编号
     */
    private String deptNO;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDeptNO() {
        return deptNO;
    }

    public void setDeptNO(String deptNO) {
        this.deptNO = deptNO;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    @Override
    public String toString() {
        return "TradeDepartment{" + "deptNO='" + deptNO + '\'' + ", deptName='" + deptName + '\''
            + ", createTime=" + createTime + ", lastUpdateTime=" + lastUpdateTime + '}';
    }
}
