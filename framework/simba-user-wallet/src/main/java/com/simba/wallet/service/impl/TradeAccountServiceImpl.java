package com.simba.wallet.service.impl;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.wallet.dao.TradeAccountDao;
import com.simba.wallet.model.TradeAccount;
import com.simba.wallet.model.TradeUser;
import com.simba.wallet.model.form.TradeAccountSearchForm;
import com.simba.wallet.service.TradeAccountService;
import com.simba.wallet.service.TradeUserService;
import com.simba.wallet.util.CommonUtil;
import com.simba.wallet.util.Constants.AccountActiveStatus;
import com.simba.wallet.util.Constants.AccountFrozenStatus;
import com.simba.wallet.util.Constants.AccountType;
import com.simba.wallet.util.Constants.FeeType;
import com.simba.wallet.util.Constants.TradeUserType;
import com.simba.wallet.util.ErrConfig;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 支付账号 Service实现类
 *
 * @author caozj
 */
@Service
@Transactional
public class TradeAccountServiceImpl implements TradeAccountService {

    @Autowired
    private TradeAccountDao tradeAccountDao;

    @Autowired
    private TradeUserService tradeUserDao;

    @Override
    public Long add(TradeAccount tradeAccount) {
        return tradeAccountDao.add(tradeAccount);
    }

    @Override
    public void delete(Long id) {
        tradeAccountDao.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> page(Pager page) {
        return tradeAccountDao.page(page);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> page(Pager page, TradeAccountSearchForm tradeAccountSearchForm) {
        return tradeAccountDao.page(page);
    }

    @Override
    @Transactional(readOnly = true)
    public Long count() {
        return tradeAccountDao.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Long countBy(String field, Object value) {
        return tradeAccountDao.countBy(field, value);
    }

    @Override
    public void deleteBy(String field, Object value) {
        tradeAccountDao.deleteBy(field, value);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> listAll() {
        return tradeAccountDao.listAll();
    }

    @Override
    public void update(TradeAccount tradeAccount) {
        tradeAccountDao.update(tradeAccount);
    }

    @Override
    public void batchDelete(List<Long> idList) {
        for (Long id : idList) {
            this.delete(id);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public TradeAccount getBy(String field, Object value) {
        return tradeAccountDao.getBy(field, value);
    }

    @Override
    @Transactional(readOnly = true)
    public TradeAccount getByAnd(String field1, Object value1, String field2, Object value2) {
        return tradeAccountDao.getByAnd(field1, value1, field2, value2);
    }

    @Override
    @Transactional(readOnly = true)
    public TradeAccount getByOr(String field1, Object value1, String field2, Object value2) {
        return tradeAccountDao.getByOr(field1, value1, field2, value2);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> listBy(String field, Object value) {
        return tradeAccountDao.listBy(field, value);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> listByAnd(String field1, Object value1, String field2,
        Object value2) {
        return tradeAccountDao.listByAnd(field1, value1, field2, value2);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> listByOr(String field1, Object value1, String field2, Object value2) {
        return tradeAccountDao.listByOr(field1, value1, field2, value2);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> pageBy(String field, Object value, Pager page) {
        return tradeAccountDao.pageBy(field, value, page);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> pageByAnd(String field1, Object value1, String field2, Object value2,
        Pager page) {
        return tradeAccountDao.pageByAnd(field1, value1, field2, value2, page);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TradeAccount> pageByOr(String field1, Object value1, String field2, Object value2,
        Pager page) {
        return tradeAccountDao.pageByOr(field1, value1, field2, value2, page);
    }

    @Override
    public void deleteByAnd(String field1, Object value1, String field2, Object value2) {
        tradeAccountDao.deleteByAnd(field1, value1, field2, value2);
    }

    @Override
    public void deleteByOr(String field1, Object value1, String field2, Object value2) {
        tradeAccountDao.deleteByOr(field1, value1, field2, value2);
    }

    @Override
    @Transactional(readOnly = true)
    public Long countByAnd(String field1, Object value1, String field2, Object value2) {
        return tradeAccountDao.countByAnd(field1, value1, field2, value2);
    }

    @Override
    @Transactional(readOnly = true)
    public Long countByOr(String field1, Object value1, String field2, Object value2) {
        return tradeAccountDao.countByOr(field1, value1, field2, value2);
    }

    private String generateAccountID(AccountType accountType) {
        String accountID = accountType.getValue();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        accountID += now.format(formatter);
        Random random = new Random();

        return accountID + random.ints(100000, 999999).limit(1).findFirst().getAsInt()
            + StringUtils.EMPTY;
    }

    /**
     * 开通账户 包括smart用户，部门，渠道
     *
     * @param userID 用户id
     * @param name 用户名称
     * @param password 已加密的密码
     * @param payPhone 移动电话
     * @param payEmail 邮箱
     * @param tradeUserType 用户类型
     * @param isAllowPay 是否允许支付
     * @param isAllowRecharge 是否允许充值
     * @param isActive 是否激活
     */
    @Override
    public JsonResult openAccount(long userID, String name, String password, String payPhone,
        String payEmail, TradeUserType tradeUserType, int isAllowPay, int isAllowRecharge,
        int isActive) throws Exception {

        String accountID = "";
        AccountType accountType = null;

        if (tradeUserType.equals(TradeUserType.PERSION)) {
            accountID = generateAccountID(AccountType.PERSIONAL_ACCOUNT);
            accountType = AccountType.PERSIONAL_ACCOUNT;
        } else if (tradeUserType.equals(TradeUserType.DEPARTMENT)) {
            accountID = generateAccountID(AccountType.COMPANY_ACCOUNT);
            accountType = AccountType.COMPANY_ACCOUNT;
        } else if (tradeUserType.equals(TradeUserType.CHANNEL)) {
            accountID = generateAccountID(AccountType.CHANNEL_ACCOUNT);
            accountType = AccountType.CHANNEL_ACCOUNT;
        }
        Long tradeUserID = 0L;

        // 检查数据库是否存在该记录
        long userCount = tradeUserDao
            .countByAnd("userID", userID, "type", tradeUserType.getValue(), "isActive",
                AccountActiveStatus.ACTIVE.getValue());
        Date now = new Date();
        if (userCount == 0) {
            TradeUser tradeUser = new TradeUser();
            tradeUser.setUserID(userID);

            tradeUser.setIsAllowPay(isAllowPay);
            tradeUser.setIsActive(isActive);
            tradeUser.setName(name);
            tradeUser.setType(tradeUserType.getValue());
            tradeUser.setPayEmail(payEmail);
            tradeUser.setPayPhone(payPhone);
            tradeUser.setPayEmail(payEmail);
            tradeUser.setCreateTime(now);
            tradeUser.setLastUpdateTime(now);
            tradeUser.setPayPassword(password);

            tradeUserID = tradeUserDao.add(tradeUser);
            if (tradeUserID <= 0) {
                throw ErrConfig.OPEN_WALLET_ACCOUNT_FAILED;
            }
        }
        // 检查数据库是否存在该记录
        Long accountCount = tradeAccountDao.countByAnd("tradeUserID", tradeUserID, "isActive",
            AccountActiveStatus.ACTIVE.getValue());
        if (accountCount == 0) {
            TradeAccount tradeAccount = new TradeAccount();
            tradeAccount.setAccountID(accountID);
            tradeAccount.setAccountType(accountType.getValue());
            tradeAccount.setAccountBalance(0);
            tradeAccount.setAvailableBalance(0);
            tradeAccount.setFeeType(FeeType.CNY.name());
            tradeAccount.setFrozenBalance(0);
            tradeAccount.setIsActive(isActive);
            tradeAccount.setIsAllowPay(isAllowPay);
            tradeAccount.setIsAllowRecharge(isAllowRecharge);
            tradeAccount.setIsFrozen(AccountFrozenStatus.NOTFROZEN.getValue());
            tradeAccount.setTradeUserID(tradeUserID);
            tradeAccount.setLastUpdateTime(now);
            tradeAccount.setCreateTime(now);
            Long tradeAccountID = tradeAccountDao.add(tradeAccount);
            if (tradeAccountID <= 0) {
                throw ErrConfig.OPEN_WALLET_ACCOUNT_FAILED;
            }
        }

        return new JsonResult("", "钱包功能开通成功", 200);
    }


    @Override
    public JsonResult frozeAccount(long userID, TradeUserType userType) {
        // 手机验证码 根据userID获取注册时的手机号
        TradeAccount tradeAccount = tradeAccountDao.get(userID, userType);
        tradeAccount.setIsFrozen(AccountFrozenStatus.FROZEN.getValue());
        tradeAccountDao.update(tradeAccount);
        return new JsonResult();
    }

    @Override
    public void frozeAccount(long tradeUserID) {
        // 手机验证码 根据userID获取注册时的手机号
        TradeAccount tradeAccount = tradeAccountDao.get(tradeUserID);
        tradeAccount.setIsFrozen(AccountFrozenStatus.FROZEN.getValue());
        tradeAccountDao.update(tradeAccount);
    }

    @Override
    public void closeAccount(long tradeUserID) {
        TradeUser tradeUser = tradeUserDao.get(tradeUserID);
        TradeAccount tradeAccount = tradeAccountDao.get(tradeUserID);
        tradeUser.setIsActive(CommonUtil.currTimeMillis());
        tradeUserDao.update(tradeUser);
        if (tradeAccount.getAccountBalance() == 0) {
            tradeAccount.setIsActive(CommonUtil.currTimeMillis());
            tradeAccountDao.update(tradeAccount);
        } else {
            throw ErrConfig.BALANCE_NOT_NONE;
        }
    }

    @Override
    public void activeAccount(long tradeUserID) {
        TradeAccount tradeAccount = tradeAccountDao.get(tradeUserID);
        tradeAccount.setIsFrozen(AccountFrozenStatus.NOTFROZEN.getValue());
        tradeAccountDao.update(tradeAccount);

    }

    @Override
    public JsonResult closeAccount(long userID, TradeUserType userType) {
        TradeUser tradeUser = tradeUserDao.get(userID, userType);
        TradeAccount tradeAccount = tradeAccountDao.get(tradeUser.getId());
        // 因此不能使用-1，是因为数据库的唯一索引使用了userAccount 和 isActive,
        tradeUser.setIsActive(CommonUtil.currTimeMillis());
        tradeUserDao.update(tradeUser);
        if (tradeAccount.getAccountBalance() == 0) {
            //同tradeUser setIsActive值设置
            tradeAccount.setIsActive(CommonUtil.currTimeMillis());
            tradeAccountDao.update(tradeAccount);
        } else {
            throw ErrConfig.BALANCE_NOT_NONE;
        }
        return new JsonResult();
    }

    @Override
    public JsonResult activeAccount(long userID, TradeUserType userType) {
        TradeAccount tradeAccount = tradeAccountDao.get(userID, userType);
        tradeAccount.setIsFrozen(AccountFrozenStatus.NOTFROZEN.getValue());
        tradeAccountDao.update(tradeAccount);
        return new JsonResult();
    }

    @Override
    @Transactional(readOnly = true)
    public TradeAccount get(Long userID, TradeUserType userType) {
        return tradeAccountDao.get(userID, userType);
    }

    @Override
    @Transactional(readOnly = true)
    public TradeAccount get(Long tradeUserID) {
        return tradeAccountDao.get(tradeUserID);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> getBalance(AccountType accountType) {
        return tradeAccountDao.getBalance(accountType);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> getBalance(Long tradeUserID, AccountType accountType) {
        return tradeAccountDao.getBalance(tradeUserID, accountType);
    }

}
