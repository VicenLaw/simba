package com.simba.wallet.controller;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.wallet.model.vo.TradeDepartmentVO;
import com.simba.wallet.service.TradeAccountService;
import com.simba.wallet.util.CommonUtil;
import com.simba.wallet.util.Constants.InnerAccount;
import com.simba.wallet.util.Constants.TradeUserType;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 收款部门控制器
 *
 * @author caozj
 */
@Controller
@RequestMapping("/tradeDepartment")
public class TradeDepartmentController {

    private static final Log logger = LogFactory.getLog(TradeDepartmentController.class);

    @Autowired
    private TradeAccountService tradeAccountService;

    @RequestMapping("/list")
    public String list() {
        return "tradeDepartment/list";
    }

    @RequestMapping("/getList")
    public String getList(Pager pager, ModelMap model) {
        InnerAccount[] innerAccounts = InnerAccount.values();
        List<TradeDepartmentVO> tradeDepartmentVOList = new ArrayList<>(innerAccounts.length);
        for (InnerAccount dept : innerAccounts) {
            String accountStatus = "";
            try {
                accountStatus = CommonUtil.getAccountStatus(
                    tradeAccountService.get(dept.getId(), TradeUserType.DEPARTMENT));
            } catch (Exception e) {
                logger.error("获取钱包账号状态发生异常", e);
            }
            TradeDepartmentVO vo = new TradeDepartmentVO();
            vo.setId(dept.getId());
            vo.setDeptName(dept.name());
            vo.setDeptNO(dept.name());
            vo.setAccountStatus(accountStatus);
            tradeDepartmentVOList.add(vo);
        }
        model.put("list", tradeDepartmentVOList);
        return "tradeDepartment/table";
    }

    @ResponseBody
    @RequestMapping("/count")
    public JsonResult count() {
        return new JsonResult(InnerAccount.values().length, "", 200);
    }

    @RequestMapping("/toAdd")
    public String toAdd() {
        return "tradeDepartment/add";
    }

}
