package com.simba.wallet.pay.callbacktrade;

import com.simba.framework.util.json.JsonResult;
import com.simba.wallet.util.Constants.TradeStatus;
import java.util.Date;

public interface CallbackTradeInterface {

    /**
     * 回调接口开始交易
     */
    JsonResult startTrade(long userID, String ip, String location, String orderNO, String orderName,
        String orderDesc, String orderAddress, long originalAmount, long paymentAmount,
        Date channelStartTime);

    /**
     * 回调接口结束交易
     */
    JsonResult finishTrade(String orderNO, String channelOrderNO, String openID, Date
        channelStartTime, Date channelPaymentTime, String channelErrorMsg, String channelErrorCode,
        long paymentAmount, TradeStatus tradeStatus);

}
