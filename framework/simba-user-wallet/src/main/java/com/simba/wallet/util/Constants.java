package com.simba.wallet.util;

import com.simba.exception.BussException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Constants {

    public enum InnerAccount {
        REFUND_ACCOUNT(1), RECHARGE_ACCOUNT(2), CONSUME_ACCOUNT(3), REWARD_ACCOUNT(4);
        static Map<Long, InnerAccount> map = new HashMap<>();

        static {
            for (InnerAccount account : InnerAccount.values()) {
                map.put(account.getId(), account);
            }
        }

        long id;

        InnerAccount(long id) {
            this.id = id;
        }

        public static InnerAccount get(long id) {
            InnerAccount innerAccount = map.get(id);
            if (innerAccount == null) {
                throw new BussException("invalid Inner Account ID");
            }
            return innerAccount;
        }

        public long getId() {
            return id;
        }
    }

    public enum AccountActiveStatus {
        //注销时取值为当前毫秒数
        ACTIVE(1), CLOSED(-1);

        private static Map<Long, AccountActiveStatus> map = new HashMap<>();

        static {
            Arrays.stream(AccountActiveStatus.values()).forEach(t -> {
                map.put(t.value, t);
            });
        }

        //        private String name;
        private long value;

        AccountActiveStatus(int value) {
            this.value = value;
        }

        public static boolean isActive(long status) {
            return map.get(status) == ACTIVE ? true : false;
        }

        public static AccountActiveStatus get(long status) {
            AccountActiveStatus t = map.get(status);
            if (t == null) {
                throw ErrConfig.INVALID_ACCOUNT_ACTIVE_TYPE;
            }
            return t;
        }

        public long getValue() {
            return value;
        }

    }

    public enum AccountFrozenStatus {
        FROZEN(1), NOTFROZEN(0);

        private static Map<Integer, AccountFrozenStatus> map = new HashMap<>();

        static {
            Arrays.stream(AccountFrozenStatus.values()).forEach(t -> {
                map.put(t.value, t);
            });
        }

        private int value;

        AccountFrozenStatus(int value) {
            this.value = value;
        }

        public static boolean isFrozen(int status) {
            return map.get(status) == FROZEN ? true : false;
        }

        public static AccountFrozenStatus get(int status) {
            AccountFrozenStatus t = map.get(status);
            if (t == null) {
                throw ErrConfig.INVALID_ACCOUNT_FROZEN_TYPE;
            }
            return t;
        }

        public int getValue() {
            return value;
        }
    }

    public enum TradePayment {
        ALLOWPAY(1), NOTALLOWPAY(0);
        private static Map<Integer, TradePayment> map = new HashMap<>();

        static {
            Arrays.stream(TradePayment.values()).forEach(t -> {
                map.put(t.value, t);
            });
        }

        private int value;

        TradePayment(int value) {
            this.value = value;
        }

        public static boolean isAllowPay(int status) {
            return map.get(status) == ALLOWPAY ? true : false;
        }

        public static TradePayment get(int status) {
            TradePayment t = map.get(status);
            if (t == null) {
                throw ErrConfig.INVALID_TRADE_PAYMENT_TYPE;
            }
            return t;
        }

        public int getValue() {
            return value;
        }
    }

    public enum TradeRechargement {
        ALLOWRECHARGE(1), NOTALLOWRECHARGE(0);
        private static Map<Integer, TradeRechargement> map = new HashMap<>();

        static {
            Arrays.stream(TradeRechargement.values()).forEach(t -> {
                map.put(t.value, t);
            });
        }

        private int value;

        TradeRechargement(int value) {
            this.value = value;
        }

        public static boolean isAllowRecharge(int status) {
            return map.get(status) == ALLOWRECHARGE ? true : false;
        }

        public static TradeRechargement get(int status) {
            TradeRechargement t = map.get(status);
            if (t == null) {
                throw ErrConfig.INVALID_TRADE_RECHARGEMENT_TYPE;
            }
            return t;
        }

        public int getValue() {
            return value;
        }

    }

    public enum AccountType {

        PERSIONAL_ACCOUNT("P"),

        COMPANY_ACCOUNT("D"),

        CHANNEL_ACCOUNT("C");

        private static Map<String, AccountType> map = new HashMap<>();

        static {
            Arrays.stream(AccountType.values()).forEach(t -> {
                map.put(t.value, t);
            });
        }

        private String value;

        AccountType(String value) {
            this.value = value;
        }

        public static AccountType get(String type) {
            AccountType t = map.get(type);
            if (t == null) {
                throw ErrConfig.INVALID_ACCOUNT_TYPE;
            }
            return t;
        }

        public static AccountType get(TradeUserType type) {
            if (type == TradeUserType.PERSION) {
                return PERSIONAL_ACCOUNT;
            }
            if (type == TradeUserType.DEPARTMENT) {
                return COMPANY_ACCOUNT;
            }
            if (type == TradeUserType.CHANNEL) {
                return CHANNEL_ACCOUNT;
            }
            throw ErrConfig.INVALID_TRADEUSER_TYPE;
        }

        public String getValue() {
            return value;
        }

    }


    public enum ChannelType {

        WXPAY(1), ALIPAY(2);
        static Map<Long, ChannelType> map = new HashMap<>();

        static {
            for (ChannelType account : ChannelType.values()) {
                map.put(account.getId(), account);
            }
        }

        long id;

        ChannelType(long id) {
            this.id = id;
        }

        public static ChannelType get(long id) {
            ChannelType channelType = map.get(id);
            if (channelType == null) {
                throw new BussException("invalid Channel Type");
            }
            return channelType;
        }

        public long getId() {
            return id;
        }
    }

    public enum FeeType {
        CNY;
    }

    public enum TradeStatus {

        INPROCESS, FAILED, SUCCESS, OVERTIME, UNKNOWNSTATUS;

    }

    public enum TradeType {

        RECHARGE, CONSUME, REFUND, REWARD, NEWREGISTERREWARD;

    }

    public enum TradeUserType {

        PERSION("P"), CHANNEL("C"), DEPARTMENT("D");

        private static Map<String, TradeUserType> mapping = new HashMap<>();

        static {
            for (TradeUserType type : TradeUserType.values()) {
                mapping.put(type.value, type);
            }
        }

        private String value;


        TradeUserType(String value) {
            this.value = value;
        }

        public static TradeUserType get(String tradeUserType) {
            TradeUserType type = mapping.get(tradeUserType);
            if (type == null) {
                throw ErrConfig.INVALID_TRADEUSER_TYPE;
            }
            return type;
        }

        public String getValue() {
            return value;
        }
    }

    public enum BalanceType {

        REALBALANCE(1), VIRTUALBALANCE(2);

        private static Map<String, BalanceType> mapping = new HashMap<>();

        static {
            for (BalanceType type : BalanceType.values()) {
                mapping.put(type.value + "", type);
            }
        }

        private int value;

        BalanceType(int value) {
            this.value = value;
        }

        public static BalanceType get(String tradeUserType) {
            BalanceType type = mapping.get(tradeUserType);
            if (type == null) {
                throw ErrConfig.INVALID_TRADBALANCE_TYPE;
            }
            return type;
        }

        public int getValue() {
            return value;
        }
    }


}
