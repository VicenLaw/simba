package com.simba.wallet.controller;

import com.google.common.base.Strings;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.wallet.model.TradeAccount;
import com.simba.wallet.model.TradeUser;
import com.simba.wallet.model.vo.TradeAccountVO;
import com.simba.wallet.service.TradeAccountService;
import com.simba.wallet.service.TradeUserService;
import com.simba.wallet.util.CommonUtil;
import com.simba.wallet.util.Constants.AccountActiveStatus;
import com.simba.wallet.util.Constants.AccountType;
import com.simba.wallet.util.Constants.ChannelType;
import com.simba.wallet.util.Constants.InnerAccount;
import com.simba.wallet.util.Constants.TradeUserType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 支付账号控制器
 *
 * @author caozj
 */
@Controller
@RequestMapping("/tradeAccount")
public class TradeAccountController {

    @Autowired
    private TradeAccountService tradeAccountService;

    @Autowired
    private TradeUserService tradeUserService;

    @RequestMapping("/doSearch")
    public String getSmartUserAccount(String mobile,
        ModelMap model) {
        if(Strings.isNullOrEmpty(mobile)) {
            return "redirect:getList?accountType=P";
        }
        TradeUser tradeUser = tradeUserService
            .getByAnd("payPhone", mobile, "type",
                TradeUserType.PERSION.getValue(), "isActive",
                AccountActiveStatus.ACTIVE.getValue());
        TradeAccount tradeAccount = null;
        if (tradeUser != null) {
            tradeAccount =
                tradeAccountService.get(tradeUser.getUserID(), TradeUserType.PERSION);
        }
        List<TradeAccountVO> tradeAccountVOList = new ArrayList<>();
        model.put("list", tradeAccountVOList);
        if (tradeAccount != null) {
            tradeAccountVOList.add(new TradeAccountVO(tradeAccount, tradeUser));
            Map<String, Object> balanceMap = tradeAccountService
                .getBalance(tradeUser.getId(), AccountType.PERSIONAL_ACCOUNT);
            model.putAll(CommonUtil.fmtBalance(balanceMap));
        }
        model.put("showSummery", true);
        return "tradeAccount/table";
    }

    @RequestMapping("/departmentList")
    public String departmentList(ModelMap model) {
        model.put("accountType", AccountType.COMPANY_ACCOUNT.getValue());
        return "tradeAccount/departmentList";
    }

    @RequestMapping("/smartUserList")
    public String smartUserList(ModelMap model) {
        model.put("accountType", AccountType.PERSIONAL_ACCOUNT.getValue());
        return "tradeAccount/smartUserList";
    }

    @RequestMapping("/channelList")
    public String channelList(ModelMap model) {
        model.put("accountType", AccountType.CHANNEL_ACCOUNT.getValue());
        return "tradeAccount/channelList";
    }

    @RequestMapping("/getList")
    public String getList(Pager page, String accountType, ModelMap model) {
        model.putAll(getList(page, CommonUtil.getAccountType(accountType)));
        return "tradeAccount/table";
    }

    public Map<String, Object> getList(Pager page, AccountType accountType) {
        List<TradeAccount> list = null;
        if (page != null) {
            list = tradeAccountService
                .pageByAnd("accountType", accountType.getValue(), "isActive", 1, page);
        } else {
            list =
                tradeAccountService.listByAnd("accountType", accountType.getValue(), "isActive", 1);
        }
        List<TradeAccountVO> tradeAccountVOList = new ArrayList<>();
        Map<String, Object> result = new HashMap<>();
        for (TradeAccount tradeAccount : list) {
            TradeUser tradeUser = tradeUserService.get(tradeAccount.getTradeUserID());
            if (tradeUser != null) {
                TradeAccountVO tradeAccountVO = new TradeAccountVO(tradeAccount, tradeUser);
                tradeAccountVOList.add(tradeAccountVO);
            }
        }
        result.put("list", tradeAccountVOList);
        Map<String, Object> balanceMap = tradeAccountService.getBalance(accountType);
        result.putAll(CommonUtil.fmtBalance(balanceMap));
        boolean showSummery = false;
        if (accountType != AccountType.COMPANY_ACCOUNT) {
            showSummery = true;
        }
        result.put("showSummery", showSummery);
        return result;
    }

    @ResponseBody
    @RequestMapping("/count")
    public JsonResult count(String accountType) {
        Long count = tradeAccountService.countByAnd("accountType", accountType, "isActive", 1);
        return new JsonResult(count, "", 200);
    }

    /**
     * 开通个人支付账户
     *
     * @param name
     * @param password
     * @param payPhone
     * @param payEmail
     * @param session
     * @return
     * @throws Exception
     */
    // @ResponseBody
    // @RequestMapping("/openPersonalAccount")
    // public JsonResult openPersonalAccount(String name, String password,
    // String payPhone,
    // String payEmail, HttpSession session) throws Exception {
    // return
    // tradeAccountService.openAccount(sessionUtil.getSmartUser(session).getAccount(),
    // name,
    // password, payPhone, payEmail, TradeUserType.PERSION, 1, 1, 1);
    // }

    /**
     * 冻结个人账户
     */
    @ResponseBody
    @RequestMapping("/frozePersonalAccount")
    public JsonResult frozePersonalAccount(long userID) throws Exception {
        tradeAccountService.frozeAccount(userID, TradeUserType.PERSION);
        return new JsonResult(200);
    }

    /**
     * 激活冻结的账户
     */
    @ResponseBody
    @RequestMapping("/activatePersonalAccount")
    public JsonResult activatePersonAccount(long userID) throws Exception {
        tradeAccountService.activeAccount(userID, TradeUserType.PERSION);
        return new JsonResult(200);
    }

    /**
     * 开通部门支付账户
     */
    @ResponseBody
    @RequestMapping("/openDepartmentAccount")
    public JsonResult openCompanyAccount(long userID) throws
        Exception {
        return tradeAccountService.openAccount(userID, InnerAccount.get(userID).name(), "", "", "",
            TradeUserType.DEPARTMENT, 0, 0, 1);
    }

    /**
     * 冻结部门账户
     */
    @ResponseBody
    @RequestMapping("/frozeDepartmentAccount")
    public JsonResult frozeCompanyAccount(long userID) throws Exception {
        tradeAccountService.frozeAccount(userID, TradeUserType.DEPARTMENT);
        return new JsonResult(200);
    }

    /**
     * 激活冻结的账户
     */
    @ResponseBody
    @RequestMapping("/activateDepartmentAccount")
    public JsonResult activateDepartmentAccount(long userID) throws Exception {
        tradeAccountService.activeAccount(userID, TradeUserType.DEPARTMENT);
        return new JsonResult(200);
    }

    /**
     * 开通渠道账户
     */
    @ResponseBody
    @RequestMapping("/openChannelAccount")
    public JsonResult openChannelAccount(long userID) throws
        Exception {
        return tradeAccountService
            .openAccount(userID, ChannelType.get(userID).name(), "", "", "", TradeUserType.CHANNEL,
                0,
                0, 1);
    }

    /**
     * 冻结渠道账户
     */
    @ResponseBody
    @RequestMapping("/frozeChannelAccount")
    public JsonResult frozeChannelAccount(long userID) throws Exception {
        tradeAccountService.frozeAccount(userID, TradeUserType.CHANNEL);
        return new JsonResult();
    }

    /**
     * 激活冻结的账户
     */
    @ResponseBody
    @RequestMapping("/activateChannelAccount")
    public JsonResult activateChannelAccount(long userID) throws Exception {
        tradeAccountService.activeAccount(userID, TradeUserType.CHANNEL);
        return new JsonResult();
    }

}
