package com.simba.wallet.model.form;

public class TradeAccountSearchForm {

    private Long userID;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }
}
