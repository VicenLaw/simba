package com.simba.wallet.controller;

import com.google.common.base.Strings;
import com.simba.framework.util.date.DateUtil;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.wallet.model.TradeUser;
import com.simba.wallet.model.vo.TradeSmartUserVO;
import com.simba.wallet.service.TradeAccountService;
import com.simba.wallet.service.TradeUserService;
import com.simba.wallet.util.CommonUtil;
import com.simba.wallet.util.Constants.TradePayment;
import com.simba.wallet.util.Constants.TradeUserType;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 钱包用户信息控制器
 *
 * @author caozj
 */
@Controller
@RequestMapping("/tradeSmartUser")
public class TradeSmartUserController {

    private static final Log logger = LogFactory.getLog(TradeSmartUserController.class);

    @Autowired
    private TradeUserService tradeUserService;

    @Autowired
    private TradeAccountService tradeAccountService;

    @RequestMapping("/list")
    public String list() {
        return "tradeSmartUser/list";
    }

    @RequestMapping("/getList")
    public String getList(Pager pager, ModelMap model, String name) {
        List<TradeUser> list = null;
        if (!Strings.isNullOrEmpty(name)) {
            list = tradeUserService
                .pageByAnd("name", name, "type", TradeUserType.PERSION.getValue(), pager);
        } else {
            list = tradeUserService
                .pageBy("type", TradeUserType.PERSION.getValue(), pager);
        }

        List<TradeSmartUserVO> tradeSmartUserVOList = new ArrayList<>();
        for (TradeUser tradeUser : list) {
            String accountStatus = "";
            try {
                accountStatus = CommonUtil.getAccountStatus(
                    tradeAccountService.get(tradeUser.getUserID(), TradeUserType.PERSION));
            } catch (Exception e) {
                logger.error("获取钱包账号状态发生异常", e);
                accountStatus = "获取状态失败";
            }
            TradeSmartUserVO vo = new TradeSmartUserVO();
            vo.setId(tradeUser.getId());
            vo.setUserID(tradeUser.getUserID());
            vo.setAccountStatus(accountStatus);
            vo.setIsAllowPay(TradePayment.isAllowPay(tradeUser.getIsAllowPay()) ? "允许" : "不允许");
            vo.setUserStatus(CommonUtil.getUserStatus(tradeUser));
            vo.setName(tradeUser.getName());
            vo.setCreateTime(DateUtil.date2String(tradeUser.getCreateTime()));
            vo.setLastUpdateTime(DateUtil.date2String(tradeUser.getLastUpdateTime()));
            tradeSmartUserVOList.add(vo);
        }
        model.put("list", tradeSmartUserVOList);
        return "tradeSmartUser/table";
    }

    @ResponseBody
    @RequestMapping("/activateSmartUserPayment")
    public JsonResult activateSmartUserPayment(long tradeUserID) throws Exception {
        tradeUserService.activatePayment(tradeUserID);
        return new JsonResult();
    }

    @ResponseBody
    @RequestMapping("/frozeSmartUserPayment")
    public JsonResult frozeSmartUserPayment(long tradeUserID) throws Exception {
        tradeUserService.frozePayment(tradeUserID);
        return new JsonResult();
    }

    @ResponseBody
    @RequestMapping("/count")
    public JsonResult count() {
        Long count = tradeUserService.countBy("type", TradeUserType.PERSION.getValue());
        return new JsonResult(count, "", 200);
    }
}
