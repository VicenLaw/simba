package com.simba.wallet.pay.callbacktrade.impl;

import com.simba.framework.util.json.JsonResult;
import com.simba.wallet.model.TradeAccount;
import com.simba.wallet.pay.callbacktrade.BaseCallbackTrade;
import com.simba.wallet.util.Constants.ChannelType;
import com.simba.wallet.util.Constants.InnerAccount;
import com.simba.wallet.util.Constants.TradeStatus;
import com.simba.wallet.util.Constants.TradeType;
import java.util.Date;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 微信提现交易
 *
 * @author zhangfenghua
 */
@Service
@Transactional
public class AliRefundTrade extends BaseCallbackTrade {

    @Override
    public void updateBalance(TradeAccount smartUserTradeAccount,
        TradeAccount departmentTradeAccount, TradeAccount channelTradeAccount, long paymentAmount) {
        smartUserTradeAccount
            .setAccountBalance(smartUserTradeAccount.getAccountBalance() - paymentAmount);
        smartUserTradeAccount
            .setAvailableBalance(smartUserTradeAccount.getAvailableBalance() - paymentAmount);
        departmentTradeAccount
            .setAccountBalance(departmentTradeAccount.getAccountBalance() - paymentAmount);
        departmentTradeAccount
            .setAvailableBalance(departmentTradeAccount.getAvailableBalance() - paymentAmount);
        channelTradeAccount
            .setAccountBalance(channelTradeAccount.getAccountBalance() - paymentAmount);
        channelTradeAccount
            .setAvailableBalance(channelTradeAccount.getAvailableBalance() - paymentAmount);
    }

    @Override
    public JsonResult finishTrade(String orderNO, String channelOrderNO,
        String openID, Date channelStartTime, Date channelPaymentTime, String channelErrorMsg,
        String channelErrorCode,
        long paymentAmount, TradeStatus tradeStatus) {
        return finishTrade(ChannelType.ALIPAY, orderNO, channelOrderNO, openID,
            channelStartTime, channelPaymentTime, channelErrorMsg,
            channelErrorCode, paymentAmount, tradeStatus, TradeType.REFUND);
    }

    @Override
    public JsonResult startTrade(long userID, String ip, String location, String orderNO,
        String orderName, String orderDesc, String orderAddress, long originalAmount,
        long paymentAmount,
        Date channelStartTime) {
        return startTrade(userID, ip, location, orderNO, orderName, orderDesc, orderAddress,
            originalAmount, paymentAmount, new Date(), channelStartTime,
            InnerAccount.REFUND_ACCOUNT,
            ChannelType.ALIPAY, TradeType.REFUND);
    }

}
