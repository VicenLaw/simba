package com.simba.wallet.pay.trade;

import com.simba.framework.util.json.JsonResult;

public interface InnerTradeInterface {

    /**
     * 交易
     */
    JsonResult trade(long userID, String ip, String location, String orderNO, String orderName,
        String orderDesc, String orderAddress, long originalAmount, long paymentAmount);
}
