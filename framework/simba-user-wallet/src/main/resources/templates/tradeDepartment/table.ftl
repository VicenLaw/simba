<#if list??>
  <#list list as tradeDepartmentVO>
	<tr>
    <td>${tradeDepartmentVO.deptName}</td>
    <td>${tradeDepartmentVO.accountStatus}</td>
    <td>
      <button type="button" class="btn btn-default btn-sm"
              onclick="TradeDepartment.toUpdate(${tradeDepartmentVO.id});"><i
          class="fa fa-pencil-square-o"></i>修改
      </button>
      <button type="button" class="btn btn-default btn-sm"
              onclick="TradeDepartment.deleteTradeDepartment('${tradeDepartmentVO.id}');"><i
          class="fa fa-remove"></i>删除
      </button>
      <button type="button" class="btn btn-default btn-sm"
              onclick="TradeDepartment.openDepartmentAccount('${tradeDepartmentVO.id}');"><i class="fa
          fa-user"></i>开通账户
      </button>
      <button type="button" class="btn btn-default btn-sm"
              onclick="TradeDepartment.frozeDepartmentAccount('${tradeDepartmentVO.id}');"><i
          class="fa fa-user"></i>冻结账户
      </button>
      <button type="button" class="btn btn-default btn-sm"
              onclick="TradeDepartment.activateDepartmentAccount('${tradeDepartmentVO.id}');"><i
          class="fa fa-user"></i>激活账户
      </button>

    </td>
  </tr>
  </#list>
</#if>
