<#if list??>
  <#list list as tradeChannelVO>
	<tr>
    <td>${tradeChannelVO.type}</td>
    <td>${tradeChannelVO.accountStatus}</td>
    <td>
      <button type="button" class="btn btn-default btn-sm"
              onclick="TradeChannel.openChannelAccount('${tradeChannelVO
              .id}');"><i class="fa fa-user"></i>开通账户
      </button>
      <button type="button" class="btn btn-default btn-sm" onclick="TradeChannel.frozeChannelAccount
          ('${tradeChannelVO.id}');"><i class="fa fa-user"></i>冻结账户
      </button>
      <button type="button" class="btn btn-default btn-sm"
              onclick="TradeChannel.activateChannelAccount
                  ('${tradeChannelVO.id}');"><i class="fa fa-user"></i>激活账户
      </button>

    </td>
  </tr>
  </#list>
</#if>
