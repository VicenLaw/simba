var TradeChannel = {

  "toAdd": function () {
    window.self.location.href = contextPath + "/tradeChannel/toAdd";
  },
  "initTradeChannelList": function (start, pageSize, method) {
    var data = {}
    var data2 = {}
    method = method || "getList"
    $.extend(data2, data);
    data["pageStart"] = start
    data["pageSize"] = pageSize
    $.ajax({
      type: "get",
      url: contextPath + "/tradeChannel/" + method,
      data: data,
      async: true,
      dataType: "html",
      success: function (html) {
        $("#table").find("tbody").html(html);
        CheckBox.init();
        setTimeout("CheckBox.bindCheckAll();", 1000);
      }
    });
    $.ajax({
      type: "get",
      url: contextPath + "/tradeChannel/count",
      async: true,
      data: data2,
      dataType: "json",
      success: function (data) {
        var total = data.data;
        var pageHtml = Page.init(total, start, pageSize,
            "TradeChannel.clickPager");
        $("#page").html(pageHtml);
      }
    });
  },
  "clickPager": function (start, pageSize) {
    TradeChannel.initTradeChannelList(start, pageSize);
  },

  "toUpdate": function (id) {
    window.self.location.href = contextPath + "/tradeChannel/toUpdate?id=" + id;
  },

  "deleteTradeChannel": function (typeID) {
    $.ajax({
      type: "post",
      url: contextPath + "/tradeChannel/delete",
      data: {
        "userID": typeID,
      },
      async: true,
      dataType: "json",
      success: function (data) {
        if (data.code == 200) {
          TradeChannel.initTradeChannelList(0, Page.size);
        } else {
          parent.showInfo(data.msg);
        }
      }
    });
  },

  "checkForm": function () {
    return true;
  },

  "toList": function () {
    window.self.location.href = contextPath + "/tradeChannel/list";
  },
  "openChannelAccount": function (typeID) {
    var data = {}
    method = "openChannelAccount"
    data["userID"] = typeID

    $.ajax({
      type: "get",
      url: contextPath + "/tradeAccount/" + method,
      data: data,
      async: true,
      dataType: "json",
      success: function (data) {
        if (data.code == 200) {
          TradeChannel.initTradeChannelList(0, Page.size);
        } else {
          parent.showInfo(data.msg);
        }
      }
    });
  },
  "frozeChannelAccount": function (typeID) {
    var data = {}
    method = "frozeChannelAccount"
    data["userID"] = typeID

    $.ajax({
      type: "get",
      url: contextPath + "/tradeAccount/" + method,
      data: data,
      async: true,
      dataType: "json",
      success: function (data) {
        if (data.code == 200) {
          TradeChannel.initTradeChannelList(0, Page.size);
        } else {
          parent.showInfo(data.msg);
        }
      }
    });
  },
  "activateChannelAccount": function (typeID) {
    var data = {}
    method = "activateChannelAccount"
    data["userID"] = typeID

    $.ajax({
      type: "get",
      url: contextPath + "/tradeAccount/" + method,
      data: data,
      async: true,
      dataType: "json",
      success: function (data) {
        if (data.code == 200) {
          TradeChannel.initTradeChannelList(0, Page.size);
        } else {
          parent.showInfo(data.msg);
        }
      }
    });
  },
  "end": null
};
