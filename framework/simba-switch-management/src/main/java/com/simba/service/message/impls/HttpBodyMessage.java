package com.simba.service.message.impls;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.simba.framework.util.http.HttpClientUtil;
import com.simba.interfaces.ReceiveDealInterface;
import com.simba.model.ReceiveDealType;
import com.simba.model.ReceiveMsg;

/**
 * 将接收到的消息以http body的方式转发
 *
 * @author caozhejun
 */
@Service
public class HttpBodyMessage implements ReceiveDealInterface {

    private static final Log logger = LogFactory.getLog(HttpBodyMessage.class);

    @Override
    public Object deal(ReceiveMsg msg, ReceiveDealType type) {
        String message = msg.getMessage();
        String url = type.getExt();
        HttpClient httpClient = HttpClientUtil.getHttpClient();
        httpClient.getHttpConnectionManager().getParams().setSoTimeout(600000);
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(600000);
        logger.info("HttpClient params:" + httpClient.getHttpConnectionManager().getParams());
        String result = HttpClientUtil.postJson(url, message, httpClient);
        logger.info("HttpClient resp:" + result);
        return result;
    }

}
