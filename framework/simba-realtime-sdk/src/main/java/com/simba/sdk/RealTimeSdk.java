package com.simba.sdk;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.eureka.client.EurekaClientUtil;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.framework.util.json.JsonResult;

/**
 * 实时推送系统sdk
 * 
 * @author caozhejun
 *
 */
@Component
public class RealTimeSdk {

	private static final Log logger = LogFactory.getLog(RealTimeSdk.class);

	private static final String onLineSendUrl = "http://REALTIMEUSER/server/api/realTimeMessage/send";

	private static final String sendWithTimeoutUrl = "http://REALTIMEUSER/server/api/realTimeMessage/sendWithTimeout";

	private static final int timeoutMinutes = 60 * 24 * 7;

	@Autowired
	private EurekaClientUtil client;

	/**
	 * 通过websocket推送消息给用户(如果用户在线直接推送消息，如果用户不在线，消息不推送直接丢弃)
	 * 
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送的内容
	 * @param appid
	 *            应用的id
	 * @return
	 */
	public long send(String userId, String content, String appid) {
		return this.send(userId, content, appid, null);
	}

	/**
	 * 通过websocket推送消息给用户(如果用户在线直接推送消息，如果用户不在线，消息不推送直接丢弃)
	 * 
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送的内容
	 * @param appid
	 *            应用的id
	 * @param callbackUrl
	 *            回调url地址 回调时只传递id，代表推送成功的消息id，返回success代表接收通知成功
	 * @return 推送的消息id
	 */
	public long send(String userId, String content, String appid, String callbackUrl) {
		logger.info("通过websocket推送消息给用户(如果用户在线直接推送消息，如果用户不在线，消息不推送直接丢弃):userId=" + userId + ",appid=" + appid + ",content=" + content);
		Map<String, String> param = new HashMap<>();
		param.put("userId", userId);
		param.put("appid", appid);
		param.put("content", content);
		param.put("callbackUrl", callbackUrl);
		String result = client.post(onLineSendUrl, param);
		JsonResult jsonResult = FastJsonUtil.toObject(result, JsonResult.class);
		jsonResult.check("通过websocket推送消息给用户发生异常(如果用户在线直接推送消息，如果用户不在线，消息不推送直接丢弃):userId=" + userId + ",appid=" + appid + ",content=" + content);
		return (Long) jsonResult.getData();
	}

	/**
	 * 通过websocket推送消息给用户(如果用户在线直接推送消息，如果用户不在线，则保存消息，在过期时间内，如果用户上线则立刻推送，如果过期用户还未上线，把消息丢弃)
	 * 
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送的内容
	 * @param appid
	 *            应用的id
	 * @param timeoutMinutes
	 *            过期时间
	 * @param callbackUrl
	 *            回调url地址 回调时只传递id，代表推送成功的消息id，返回success代表接收通知成功
	 * 
	 */
	public long sendWithTimeout(String userId, String content, String appid, int timeoutMinutes, String callbackUrl) {
		logger.info("通过websocket推送消息给用户(如果用户在线直接推送消息，如果用户不在线，则保存消息，在过期时间内，如果用户上线则立刻推送，如果过期用户还未上线，把消息丢弃):userId=" + userId + ",appid=" + appid + ",timeoutMinutes=" + timeoutMinutes + ",content="
				+ content);
		Map<String, String> param = new HashMap<>();
		param.put("userId", userId);
		param.put("appid", appid);
		param.put("content", content);
		param.put("timeoutMinutes", timeoutMinutes + "");
		param.put("callbackUrl", callbackUrl);
		String result = client.post(sendWithTimeoutUrl, param);
		JsonResult jsonResult = FastJsonUtil.toObject(result, JsonResult.class);
		jsonResult.check("通过websocket推送消息给用户发生异常(如果用户在线直接推送消息，如果用户不在线，则保存消息，在过期时间内，如果用户上线则立刻推送，如果过期用户还未上线，把消息丢弃):userId=" + userId + ",appid=" + appid + ",content=" + content);
		return (Long) jsonResult.getData();
	}

	/**
	 * 通过websocket推送消息给用户(如果用户在线直接推送消息，如果用户不在线，则保存消息，在过期时间内，如果用户上线则立刻推送，如果过期用户还未上线，把消息丢弃)[过期时间默认7天]
	 * 
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送的内容
	 * @param appid
	 *            过期时间
	 */
	public long sendWithTimeout(String userId, String content, String appid) {
		return this.sendWithTimeout(userId, content, appid, timeoutMinutes, null);
	}

}
