define([
  'jquery',
  'common/mn/mn-kit',
  'text!./sidebar-form.hbs'
], function ($, mnKit, sidebarFormTemplate) {
  'use strict';

  var _ = mnKit._, Mn = mnKit.Mn, Radio = mnKit.Radio;
  var SidebarFormView = Mn.View.extend({
    tagName: 'form',
    className: 'sidebar-form',
    attributes: {
      'data-role': 'sidebar-form'
    },
    template: _.template(sidebarFormTemplate),
    initialize: function () {
      this.formChannel = Radio.channel('sidebar-form');
    },
    ui: {
      search: 'input[name="q"]',
      query: 'button[name="search"]'
    },
    events: {
      'click @ui.query': 'onQuery'
    },
    onQuery: function (event) {
      var $search = this.getUI('search');
      var param = $search.val();
      this.formChannel.trigger('sidebar:query', {
        param: param
      });
    }
  });
  return SidebarFormView;
});
