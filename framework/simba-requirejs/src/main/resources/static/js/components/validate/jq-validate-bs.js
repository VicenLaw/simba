/**
 * @file bs-jq-validate.js
 * @author betgar (betgar@163.com)
 * @date 09/27/2018
 * @time 16:56:34
 * @description jquery validate default config for bootstrap.
 */
define([
  'lodash',
  'jquery-validate',
  './jq-validate-bs-form'
], function (_, validator, bsValidateForm) {
  'use strict';
  var validatorDefaults = _.extend({}, validator.defaults);
  // 适配bootstrap结构的$.validator.defaults
  validator.setDefaults(bsValidateForm.validatorOptions());

  return {

    /**
     * 重置jQuery validator defaults到默认值.<br>
     * 并返回重置之前的$.validator.defaults.
     * @returns {Object}
     */
    resetValidatorDefaults: function () {
      var currentDefaults = _.extend({}, validator.defaults)
      validator.setDefaults(validatorDefaults);
      return currentDefaults;
    }
  };
});
