/**
 * @file sidebar.js
 * @author betgar (betgar@163.com)
 * @date 10/31/2018
 * @time 17:02:07
 * @description sidebar
 */
define([
  'jquery',
  'common/mn/mn-kit',
  'lodash',
  './user-state',
  './sidebar-form',
  '../menu/menu'
], function ($,
  mnKit,
  lodash,
  UserState,
  SidebarFormView,
  Menu,
  menuJson
) {
    'use strict';
    var _ = mnKit._, Backbone = mnKit.Backbone, Mn = mnKit.Mn, Radio = mnKit.Radio;

    var sidebarTemplate = '<div class="user-panel" data-role="user-panel"></div>' +
      '<form class="sidebar-form" data-role="sidebar-form"></form>' +
      '<ul class="sidebar-menu" data-role="sidebar-menu"></ul>';

    var SidebarView = Mn.View.extend({
      tagName: 'section',
      className: 'sidebar',
      options: {
        hideUserPanel: true,
        hideSidebarForm: true
      },
      getTemplate: function () {
        var hideUserPanel = this.getOption('hideUserPanel');
        var hideSidebarForm = this.getOption('hideSidebarForm');

        var $template = $(sidebarTemplate);
        if (hideUserPanel) {
          $template = $template.not('[data-role="user-panel"]');
        }
        if (hideSidebarForm) {
          $template = $template.not('[data-role="sidebar-form"]');
        }
        var filteredTmplStr = $('<div></div>').append($template).get(0).innerHTML;
        return _.template(filteredTmplStr);
      },
      regions: {
        userPanel: {
          el: '[data-role="user-panel"]',
          replaceElement: true
        },
        sidebarForm: {
          el: '[data-role="sidebar-form"]',
          replaceElement: true
        },
        sidebarMenu: {
          el: '[data-role="sidebar-menu"]',
          replaceElement: true
        }
      },
      onRender: function () {
        var hideUserPanel = this.getOption('hideUserPanel');
        var hideSidebarForm = this.getOption('hideSidebarForm');
        if (hideUserPanel) {
          this.getRegion('userPanel').empty();
        } else {
          // TODO: 传递参数 this.getOption('userState');
          this.showChildView('userPanel', new UserState.UserStateView({
            model: new UserState.StateModel()
          }));
        }

        if (hideSidebarForm) {
          this.getRegion('sidebarForm').empty();
        } else {
          this.showChildView('sidebarForm', new SidebarFormView());
        }

        this.showChildView('sidebarMenu', new Menu.View(this.getOption('menuOptions')));
      }
    });
    return SidebarView;
  });
