/**
 * @file menu.js
 * @author betgar (betgar@163.com)
 * @date 10/31/2018
 * @time 17:02:07
 * @description menu
 */
define([
  'jquery',
  'common/mn/mn-kit',
  'handlebars',
  'text!./menu.hbs',
  './menu-model'
], function ($, mnKit, hbs, menuTemplate, MenuModel) {
  'use strict';
  var _ = mnKit._, Backbone = mnKit.Backbone, Mn = mnKit.Mn,
    Radio = mnKit.Radio;
  var $menuTemplate = $(menuTemplate);
  // 注册handlebars模版片段.
  hbs.registerPartial('menuNestedItem', _.unescape($menuTemplate.filter('#nested-item').html()));
  hbs.registerPartial('menuSingleItem', _.unescape($menuTemplate.filter('#single-item').html()));
  hbs.registerPartial('menuItem', _.unescape($menuTemplate.filter('#item').html()));

  var MenuItemView = Mn.View.extend({
    template: hbs.compile('{{>menuItem}}'),
    tagName: 'ul',
    className: 'sidebar-menu',
    attributes: {
      'data-role': 'sidebar-menu',
      'data-widget': 'tree'
    },
    onRender: function () {
    },
    onBeforeAttach: function () {
      var model = this.model;
    }

  });

  // MenuItemView.setRenderer(function (template, data) {
  //   return template({
  //     item: data,
  //     templateFn: template
  //   });
  // });

  // var MenuView = Mn.CollectionView.extend({
  //   childView: MenuItemView,
  //   tagName: 'ul',
  //   className: 'sidebar-menu',
  //   attributes: {
  //     'data-role': 'sidebar-menu',
  //     'data-widget': 'tree'
  //   }
  // });

  return {
    View: MenuItemView,
    Collection: MenuModel.Collection
  };
});
