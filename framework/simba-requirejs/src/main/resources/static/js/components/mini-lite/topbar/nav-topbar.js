/**
 * @file topbar.js
 * @author betgar (betgar@163.com)
 * @date 10/22/2018
 * @time 11:26:35
 * @description
 */
define([
    'jquery',
    'pubsub',
    'underscore',
    'backbone',
    'lodash',
    'config',
    '../mini-events'
], function ($, pubsub, _, Backbone, lodash, config, miniLiteEvents) {
    'use strict';
    // <div class="row border-bottom">
    var tmpl = ['<nav class="navbar navbar-static-top" role="navigation"',
        '                 style="margin-bottom: 0">',
        '                <div class="navbar-header">',
        '                    <a class="navbar-minimalize minimalize-styl-2 btn btn-default " href="#" title="收起菜单">',
        '                    	<i class="fa fa-bars"></i>',
        '                    </a>',
        '                    <form role="search" class="navbar-form-custom" method="" action="">',
        '                        <div class="form-group">',
        '                            <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">',
        '                        </div>',
        '                    </form>',
        '                </div>',
        '                <ul class="nav navbar-top-links navbar-right welcome-message">',
        '	                <li>',
        '	                    <span class="m-r-sm text-muted">欢迎来到若依管理后台.</span>',
        '	                </li>',
        '	                <li><a data-role="fullscreen"><i class="fa fa-arrows-alt"></i>全屏</a></li>',
        '                    <li><a href="/logout" data-role="logout"><i class="fa fa-sign-out"></i>退出</a></li>',
        '                </ul>',
        '            </nav>'].join("");

    _.templateSettings = {
        interpolate: /\{\{(.+?)\}\}/g
    };
    var configStore = config.store();
    var menuMiniEvent = lodash.get(configStore, miniLiteEvents.viewEvents.MENU_MINI);
    var fullScreenEvent = lodash.get(configStore, miniLiteEvents.viewEvents.VIEW_FULLSCREEN);
    var loginEvent = lodash.get(configStore, miniLiteEvents.sysEvents.LOGOUT);

    var TopBar = Backbone.View.extend({
        tagName: 'div',
        className: 'row border-bottom',
        events: {
            'click [data-role="fullscreen"]': 'fullScreenHandler',
            'click [data-role="logout"]': 'logoutHandler',
            'click .navbar-minimalize': 'menuMinimalizeHandler'
        },
        initialize: function (options) {
            this.render(options);
        },
        render: function (options) {
            if (options && typeof options.template === 'function') {
                this.$el.append(options.template);
            } else {
                this.$el.append(tmpl);
            }
            return this;
        },
        fullScreenHandler: function () {
            fullScreenEvent.enable && pubsub.publish(fullScreenEvent.name);
        },
        logoutHandler: function () {
            // TODO: 调用注销功能
            // 发布注销事件
            loginEvent.enable && pubsub.publish(loginEvent.name);
        },
        menuMinimalizeHandler: function () {
            menuMiniEvent.enable && pubsub.publish(menuMiniEvent.name, {
                type: 'minilizeButton'
            });
        }
    });


    return TopBar;
});
