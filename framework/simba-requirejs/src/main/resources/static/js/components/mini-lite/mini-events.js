/**
 * @file mini-events.js
 * @author betgar (betgar@163.com)
 * @date 10/20/2018
 * @time 09:43:44
 * @description 记录全局事件的类.
 */
define([
    'config',
], function(config) {
    'use strict';
    var miniLiteEvents = {
        views: {
            events: {
                menuMini: {
                    name: 'miniLite.view.menu.minimalize',
                    enable: true
                },
                viewFullscreen: {
                    name: 'miniLite.view.fullscreen',
                    enable: true
                }
            }
        },
        system: {
            events: {
                login: {
                    name: 'miniLite.sys.login',
                    enable: true
                },
                logout: {
                    name: 'miniLite.sys.logout',
                    enable: true
                }
            }
        }
    };
    config.store({
        miniLite: miniLiteEvents
    });


    return {
        viewEvents: {
            MENU_MINI: 'miniLite.views.events.menuMini',
            VIEW_FULLSCREEN: 'miniLite.views.events.viewFullscreen'
        },
        sysEvents: {
            LOGIN: 'miniLite.system.events.login',
            LOGOUT: 'miniLite.system.events.logout'
        }
    };
});
