/**
 * @file menu-model.js
 * @author betgar (betgar@163.com)
 * @date 11/05/2018
 * @time 14:37:38
 * @description menu-model.
 */
define([
  'backbone'
], function(Backbone) {
  'use strict';
  var Item = Backbone.Model.extend({
    defaults: {
      id: '',
      text: '',
      url: '',
      icon: 'fa fa-link'
    }
  });

  var Items = Backbone.Collection.extend({
    model: Item
  });
  return {
    Model: Item,
    Collection: Items
  }
});
