define([
  'jquery',
  'common/mn/mn-kit'
], function ($, mnKit) {
  'use strict';
  var _ = mnKit._, Backbone = mnKit.Backbone, Mn = mnKit.Mn;
  var templateStr = ['<div class="pull-left image">',
    '  <img src="{{icon}}" class="img-circle" alt="User Image">',
    '</div>',
    '<div class="pull-left info">',
    '  <p>{{name}}</p>',
    '  <a href="#">',
    '    <i class="fa fa-circle text-success"></i>{{state}}</a>',
    '</div>'].join("");;

  var UserState = Mn.View.extend({
    template: _.template(templateStr),
    className: 'user-panel',
    attributes: {
      'data-role': 'user-panel'
    }
  });
  var StateModel = Backbone.Model.extend({
    defaults: {
      state: '在线',
      name: '用户',
      icon: '/js/plugins/adminlte/2.4.2/img/avatar.png'
    }
  });
  return {
    UserStateView: UserState,
    StateModel: StateModel
  };
});
