/**
 * @file mn-kit.js
 * @author betgar (betgar@163.com)
 * @date 11/01/2018
 * @time 11:28:10
 * @description backbone.marionette工具链的预制设置.
 */
define([
  'underscore',
  'backbone',
  'backbone-mn',
  'backbone.radio',
  'mn-templatecache',
], function(_, Backbone, Mn, Radio, TemplateCache) {
  'use strict';
  _.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
  };

  // 设置renderer函数.
  Mn.setRenderer(TemplateCache.render);
  return {
    _: _,
    Backbone: Backbone,
    Mn: Mn,
    Radio: Radio,
    TemplateCache: TemplateCache
  }
});
