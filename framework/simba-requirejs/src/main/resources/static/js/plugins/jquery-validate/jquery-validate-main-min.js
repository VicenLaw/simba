/**
 * @file jquery-validate-main-min.js
 * @author betgar (betgar@163.com)
 * @date 10/05/2018
 * @time 20:29:49
 * @description prod模式下jquery-validate的入口
 */
define(function (require) {
  require('./1.17.0/localization/messages_zh.min');
  require('./1.17.0/js/additional-methods.min');
  return $.validator;
});
