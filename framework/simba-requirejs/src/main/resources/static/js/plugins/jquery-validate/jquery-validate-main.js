/**
 * @file jquery-validate-main.js
 * @author betgar (betgar@163.com)
 * @date 10/05/2018
 * @time 20:29:17
 * @description dev模式jquery-validate的入口文件.
 */
define(function (require) {
  require('./1.17.0/localization/messages_zh');
  require('./1.17.0/js/additional-methods');
  return $.validator;
});
