package com.simba;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigClientApplication {

//	public static void main(String[] args) {
//		SpringApplication.run(ConfigClientApplication.class, args);
//	}
	
	@Autowired
	private Environment env;



	@RequestMapping("/testGetProperties")
	public String getPerson() {
		return env.getProperty("test.user.name")+"-- 版本："+env.getProperty("info.version");
	}
}
