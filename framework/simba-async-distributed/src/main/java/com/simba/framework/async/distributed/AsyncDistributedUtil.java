package com.simba.framework.async.distributed;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.framework.util.common.Hessian2Util;
import com.simba.framework.util.distributed.ClusterMessage;
import com.simba.framework.util.id.SnowFlakeId;

/**
 * 异步消息发布
 */
@Component
public class AsyncDistributedUtil {

	private static final Log logger = LogFactory.getLog(AsyncDistributedUtil.class);

	@Value("${spring.application.name}")
	private String exchangeName;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@PostConstruct
	private void init() {
		logger.info("异步订阅集群频道成功");
	}

	private SnowFlakeId snowFlakeId = new SnowFlakeId();

	private RabbitTemplate.ConfirmCallback confirmCallback = (correlationData, ack, cause) -> {
		if (!ack) {
			logger.error("Producer未收到broker的ack,消息发送失败!丢失的消息Id:" + correlationData.getId());
		}
	};

	/**
	 * 发送一个消息对象给集群中的所有服务器,通知所有服务器执行方法
	 *
	 * @param message
	 *            消息对象
	 */
	public void executeInCluster(ClusterMessage message) {
		MessageProperties messageProperties = new MessageProperties();
		messageProperties.setContentType("byte");
		CorrelationData correlationData = new CorrelationData(Long.toString(snowFlakeId.nextId()));
		Message msg = new Message(Hessian2Util.getInstance().serialize(message), messageProperties);
		rabbitTemplate.setConfirmCallback(confirmCallback);
		rabbitTemplate.send(exchangeName, "", msg, correlationData);
	}

}
