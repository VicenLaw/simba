package com.simba.framework.async.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 处理rabbitmq订阅的线程池
 * 
 * @author caozhejun
 *
 */
@Configuration
public class RabbitMQScheduleConfig {

	@Bean
	public TaskExecutor rabbitMQTaskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(20);
		taskExecutor.setMaxPoolSize(500);
		taskExecutor.setQueueCapacity(1000);
		return taskExecutor;
	}

}
