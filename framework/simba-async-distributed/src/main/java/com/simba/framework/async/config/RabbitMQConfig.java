package com.simba.framework.async.config;

import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.simba.framework.async.util.QueueNameGenerator;

/**
 * 异步集群间通知的rabbitmq配置类
 * 
 * @author linshuo
 *
 */
@Configuration
public class RabbitMQConfig {

	private static final Log logger = LogFactory.getLog(RabbitMQConfig.class);

	@Value("${spring.application.name}")
	private String exchangeName;

	@Value("${spring.rabbitmq.host}")
	private String rabbitHost;

	@Value("${spring.rabbitmq.port}")
	private int rabbitPort;

	@Value("${spring.rabbitmq.username}")
	private String rabbitUser;

	@Value("${spring.rabbitmq.password}")
	private String rabbitPwd;

	@Autowired
	private QueueNameGenerator qNameGen;

	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
		connectionFactory.setAddresses(rabbitHost + ":" + rabbitPort);
		connectionFactory.setPublisherConfirms(true);
		connectionFactory.setPublisherReturns(false);
		connectionFactory.setUsername(rabbitUser);
		connectionFactory.setPassword(rabbitPwd);
		connectionFactory.setVirtualHost("/");
		return connectionFactory;
	}

	@Bean
	public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
		RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);
		rabbitAdmin.setAutoStartup(true);
		return rabbitAdmin;
	}

	@Bean
	public FanoutExchange exchange() {
		FanoutExchange exchange = new FanoutExchange(exchangeName, true, false);
		logger.info("初始化rabbitmq的exchange:" + exchangeName);
		return exchange;
	}

	@Bean
	public Queue queue() {
		String queueName = qNameGen.createQueueName();
		Queue queue = new Queue(queueName, true);
		logger.info("初始化rabbitmq的queue:" + queueName);
		return queue;
	}

	@Bean
	public Binding binding() {
		return BindingBuilder.bind(queue()).to(exchange());
	}

	@Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
		return new RabbitTemplate(connectionFactory);
	}

	@Bean
	public SimpleMessageListenerContainer messageContainer(ConnectionFactory connectionFactory) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
		container.setQueues(queue());
		container.setConcurrentConsumers(1);
		container.setMaxConcurrentConsumers(5);
		container.setDefaultRequeueRejected(false);
		container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
		container.setExposeListenerChannel(true);
		container.setConsumerTagStrategy(queue -> queue + "_" + UUID.randomUUID().toString());
		container.setMessageListener(new DisMessageListener());
		return container;
	}

}
