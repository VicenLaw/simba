package com.simba.framework.async.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.framework.util.common.SystemUtil;

/**
 * 创建与本服务绑定的队列名的工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class QueueNameGenerator {

	@Value("${server.port}")
	private int port;

	/**
	 * 创建队列名
	 * 
	 * @return
	 */
	public String createQueueName() {
		return "disb:" + SystemUtil.getMachineName() + ":" + SystemUtil.getIpAddress() + ":" + port;
	}

}
