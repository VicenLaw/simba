package com.simba.framework.async.config;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.core.task.TaskExecutor;

import com.rabbitmq.client.Channel;
import com.simba.exception.SimbaException;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.framework.util.common.Hessian2Util;
import com.simba.framework.util.distributed.ClusterExecute;
import com.simba.framework.util.distributed.ClusterMessage;
import com.simba.framework.util.distributed.DistributedData;
import com.simba.framework.util.json.FastJsonUtil;

/**
 * rabbitmq的监听器
 * 
 * @author linshuo
 *
 */
public class DisMessageListener implements ChannelAwareMessageListener {

	private static final Log logger = LogFactory.getLog(DisMessageListener.class);

	/**
	 * 消费逻辑
	 *
	 * @param message
	 * @param channel
	 */
	@Override
	public void onMessage(Message message, Channel channel) throws IOException {
		String contentType = message.getMessageProperties().getContentType();
		Long deliveryTag = message.getMessageProperties().getDeliveryTag();
		if (contentType != null) {
			ClusterMessage msg;
			if (contentType.contains("text")) {
				msg = FastJsonUtil.toObject(new String(message.getBody()), ClusterMessage.class);
			} else if (contentType.contains("byte")) {
				msg = (ClusterMessage) Hessian2Util.getInstance().deserialize(message.getBody());
			} else {
				channel.basicReject(deliveryTag, false);
				throw new SimbaException("无效的ContentType" + contentType);
			}
			clusterMessageHandler(msg);
		}
		channel.basicAck(deliveryTag, false);
	}

	private void clusterMessageHandler(ClusterMessage message) {
		TaskExecutor taskExecutor = (TaskExecutor) ApplicationContextUtil.getBean("rabbitMQTaskExecutor");
		taskExecutor.execute(() -> {
			logger.info("接收到集群异步通知:" + message.toString());
			ClusterExecute execute = DistributedData.getInstance().get(message.getClassFullPath());
			if (execute != null) {
				execute.execute(message.getData());
				logger.info(message.getClassFullPath() + "执行完成");
			} else {
				logger.warn("没有找到" + message.getClassFullPath());
			}
		});
	}
}
