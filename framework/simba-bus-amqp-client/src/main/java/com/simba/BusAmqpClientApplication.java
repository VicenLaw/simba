package com.simba;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class BusAmqpClientApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(BusAmqpClientApplication.class).web(true).run(args);
	}
}
