package com.simba;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

public class BusRefreshConfig {

	public static void main(String[] args) throws Exception {
		System.out.println("start ===============");
		LoggerContext loggerContext = (LoggerContext) LoggerFactory
				.getILoggerFactory();
		loggerContext.getLogger("root").setLevel(Level.toLevel("INFO"));
		// 创建默认的HttpClient
		CloseableHttpClient httpclient = HttpClients.createDefault();
		// 调用 POST 方法请求服务
//		指定实例，只会触发端口号为60000的/bus/refresh?destination=config-client:60000
//		指定服务，这里会触发config-client服务的所有实例进行刷新/bus/refresh?destination=config-client:**
		HttpPost httpMethod = new HttpPost("http://127.0.0.1:9700/bus/refresh");//该方法由actuator封装
		// 获取响应
		HttpResponse response = httpclient.execute(httpMethod);
		// 根据 响应解析出字符串
		System.out.println(EntityUtils.toString(response.getEntity()));
		System.out.println("end ===============");
	}
}
