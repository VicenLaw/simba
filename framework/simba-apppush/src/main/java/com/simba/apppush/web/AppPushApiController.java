package com.simba.apppush.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.util.StringUtils;
import com.simba.apppush.util.UserAppPushUtil;
import com.simba.exception.BussException;
import com.simba.framework.util.json.JsonResult;

/**
 * App推送的Controller
 * 
 * @author caozhejun
 *
 */
@RestController
@RequestMapping("/api/app/push")
public class AppPushApiController {

	@Autowired
	private UserAppPushUtil userAppPushUtil;

	/**
	 * 标记用户手机类型和对应推送的token
	 * 
	 * @param session
	 * @param token
	 *            推送token
	 * @param mobileType
	 *            手机类型
	 * @return
	 */
	@PostMapping("/flag")
	public JsonResult flag(HttpSession session, String token, String mobileType) {
		String userId = (String) session.getAttribute("userId");
		if (StringUtils.isEmpty(userId)) {
			throw new BussException("用户没有登录");
		}
		userAppPushUtil.saveUserToken(userId, token);
		userAppPushUtil.saveUserMobileType(userId, mobileType);
		return new JsonResult();
	}

}
