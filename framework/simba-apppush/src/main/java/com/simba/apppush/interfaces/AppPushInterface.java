package com.simba.apppush.interfaces;

/**
 * App推送接口
 * 
 * @author caozhejun
 *
 */
public interface AppPushInterface {

	/**
	 * 获取处理的手机类型
	 * 
	 * @return
	 */
	String getMobileType();

	/**
	 * 发送手机App推送
	 * 
	 * @param userId
	 * @param content
	 */
	void send(String userId, String content);

	/**
	 * 是否启用
	 * 
	 * @return
	 */
	boolean enable();

}
