package com.simba.apppush.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.interfaces.AppPushInterface;

/**
 * 手机推送工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class AppPushUtil {

	@Autowired
	private UserAppPushUtil userAppPushUtil;

	/**
	 * 给用户App推送消息
	 * 
	 * @param userId
	 * @param content
	 */
	public void send(String userId, String content) {
		String mobileType = userAppPushUtil.getUserMobileType(userId);
		AppPushInterface push = null;
		if (StringUtils.isEmpty(mobileType)) {
			push = MobileTypePushUtil.getDefault();
		} else {
			push = MobileTypePushUtil.get(mobileType);
		}
		push.send(userId, content);
	}

}
