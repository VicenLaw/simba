package com.simba.apppush.impls;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.interfaces.AppPushInterface;
import com.simba.apppush.vivo.util.VivoAccessTokenUtil;
import com.simba.apppush.vivo.util.VivoAppPushUtil;
import com.simba.exception.BussException;

/**
 * vivo推送
 * 
 * @author caozhejun
 *
 */
@Component
public class VivoAppPush implements AppPushInterface {

	@Autowired
	private VivoAppPushUtil vivoAppPushUtil;

	@Autowired
	private VivoAccessTokenUtil vivoAccessTokenUtil;

	@Override
	public String getMobileType() {
		return "vivo";
	}

	@Override
	public void send(String userId, String content) {
		try {
			vivoAppPushUtil.send(userId, content);
		} catch (IOException e) {
			throw new BussException("发送Vivo推送发生异常", e);
		}
	}

	@Override
	public boolean enable() {
		return vivoAccessTokenUtil.isEnable();
	}

}
