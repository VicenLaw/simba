package com.simba.apppush.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.interfaces.AppPushInterface;
import com.simba.apppush.oppo.util.OppoAppPushUtil;
import com.simba.apppush.util.UserAppPushUtil;
import com.simba.exception.BussException;

/**
 * Oppo推送
 * 
 * @author caozhejun
 *
 */
@Component
public class OppoAppPush implements AppPushInterface {

	@Autowired
	private OppoAppPushUtil oppoAppPushUtil;

	@Autowired
	private UserAppPushUtil userAppPushUtil;

	@Override
	public String getMobileType() {
		return "oppo";
	}

	@Override
	public void send(String userId, String content) {
		String token = userAppPushUtil.getUserToken(userId);
		try {
			oppoAppPushUtil.send(token, content);
		} catch (Exception e) {
			throw new BussException("发送OPPO推送发生异常", e);
		}
	}

	@Override
	public boolean enable() {
		return oppoAppPushUtil.isEnable();
	}

}
