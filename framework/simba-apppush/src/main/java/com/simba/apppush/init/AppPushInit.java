package com.simba.apppush.init;

import java.util.List;

import org.springframework.stereotype.Component;

import com.simba.apppush.interfaces.AppPushInterface;
import com.simba.apppush.util.MobileTypePushUtil;
import com.simba.framework.util.applicationcontext.ApplicationContextInit;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;

/**
 * Spring容器初始化完成之后，初始化App推送
 * 
 * @author caozhejun
 *
 */
@Component
public class AppPushInit implements ApplicationContextInit {

	@Override
	public void init() {
		List<AppPushInterface> impls = ApplicationContextUtil.getBeansOfType(AppPushInterface.class);
		MobileTypePushUtil.init(impls);
	}

	@Override
	public int sort() {
		return 1000;
	}

}
