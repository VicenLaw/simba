package com.simba.apppush.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simba.apppush.interfaces.AppPushInterface;
import com.simba.exception.SimbaException;

/**
 * 手机App推送类型工具类
 * 
 * @author caozhejun
 *
 */
public class MobileTypePushUtil {

	private static Map<String, AppPushInterface> appPushMap;

	/**
	 * 初始化
	 * 
	 * @param impls
	 */
	public static void init(List<AppPushInterface> impls) {
		appPushMap = new HashMap<>(impls.size());
		impls.forEach((AppPushInterface impl) -> {
			if (impl.enable()) {
				appPushMap.put(impl.getMobileType(), impl);
			}
		});
	}

	/**
	 * 获取对应手机类型的App推送实现类
	 * 
	 * @param mobileType
	 * @return
	 */
	public static AppPushInterface get(String mobileType) {
		AppPushInterface impl = appPushMap.get(mobileType);
		if (impl == null) {
			impl = appPushMap.get(AppPushConstants.defaultMobileType);
		}
		if (impl == null) {
			throw new SimbaException(mobileType + "没有找到对应的App推送实现类");
		}
		return impl;
	}

	/**
	 * 获取默认的App推送实现类
	 * 
	 * @return
	 */
	public static AppPushInterface getDefault() {
		AppPushInterface impl = appPushMap.get(AppPushConstants.defaultMobileType);
		if (impl == null) {
			throw new SimbaException("没有找到默认的App推送实现类");
		}
		return impl;
	}

}
