package com.simba.apppush.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.interfaces.AppPushInterface;
import com.simba.exception.BussException;
import com.simba.xiaomi.util.XiaoMiAppPushUtil;

/**
 * 小米推送
 * 
 * @author caozhejun
 *
 */
@Component
public class XiaoMiAppPush implements AppPushInterface {

	@Autowired
	private XiaoMiAppPushUtil xiaoMiAppPushUtil;

	@Override
	public String getMobileType() {
		return "xiaomi";
	}

	@Override
	public void send(String userId, String content) {
		try {
			xiaoMiAppPushUtil.send(userId, content);
		} catch (Exception e) {
			throw new BussException("发送小米推送发生异常", e);
		}
	}

	@Override
	public boolean enable() {
		return xiaoMiAppPushUtil.isEnable();
	}

}
