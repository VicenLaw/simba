package com.simba.apppush.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.interfaces.AppPushInterface;
import com.simba.apppush.util.AppPushConstants;
import com.simba.exception.BussException;
import com.simba.jpush.app.util.JpushAppUtil;

/**
 * 极光推送
 * 
 * @author caozhejun
 *
 */
@Component
public class JPushAppPush implements AppPushInterface {

	@Autowired
	private JpushAppUtil jpushAppUtil;

	@Override
	public String getMobileType() {
		return AppPushConstants.defaultMobileType;
	}

	@Override
	public void send(String userId, String content) {
		try {
			jpushAppUtil.sendNotification(userId, content);
		} catch (Exception e) {
			throw new BussException("发送极光推送发生异常", e);
		}
	}

	@Override
	public boolean enable() {
		return jpushAppUtil.isEnabled();
	}

}
