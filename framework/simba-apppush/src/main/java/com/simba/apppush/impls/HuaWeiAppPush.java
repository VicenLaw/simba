package com.simba.apppush.impls;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.huawei.util.HuaWeiAppPushUtil;
import com.simba.apppush.interfaces.AppPushInterface;
import com.simba.apppush.util.UserAppPushUtil;
import com.simba.exception.BussException;

/**
 * 华为推送
 * 
 * @author caozhejun
 *
 */
@Component
public class HuaWeiAppPush implements AppPushInterface {

	@Autowired
	private HuaWeiAppPushUtil huaWeiAppPushUtil;

	@Autowired
	private UserAppPushUtil userAppPushUtil;

	@Override
	public String getMobileType() {
		return "huawei";
	}

	@Override
	public void send(String userId, String content) {
		String token = userAppPushUtil.getUserToken(userId);
		try {
			huaWeiAppPushUtil.send(token, content);
		} catch (IOException e) {
			throw new BussException("发送华为推送发生异常", e);
		}
	}

	@Override
	public boolean enable() {
		return huaWeiAppPushUtil.isEnabled();
	}

}
