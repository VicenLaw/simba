package com.simba.apppush.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.druid.util.StringUtils;
import com.simba.cache.Redis;

/**
 * 用户App推送工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class UserAppPushUtil {

	@Autowired
	private Redis redisUtil;

	private static final String tokenKey = "Simba:String:AppPush:Token:";

	private static final String mobileTypeKey = "Simba:String:AppPush:MobileType:";

	/**
	 * 保存用户发送App推送的Token
	 * 
	 * @param userId
	 * @param token
	 */
	public void saveUserToken(String userId, String token) {
		if (StringUtils.isEmpty(token)) {
			return;
		}
		String key = tokenKey + userId;
		redisUtil.setString(key, token);
	}

	/**
	 * 获取用户发送App推送的Token
	 * 
	 * @param userId
	 * @return
	 */
	public String getUserToken(String userId) {
		String key = tokenKey + userId;
		return redisUtil.getString(key);
	}

	/**
	 * 保存用户手机类型
	 * 
	 * @param userId
	 * @param mobileType
	 */
	public void saveUserMobileType(String userId, String mobileType) {
		String key = mobileTypeKey + userId;
		redisUtil.setString(key, mobileType);
	}

	/**
	 * 获取用户手机类型
	 * 
	 * @param userId
	 * @return
	 */
	public String getUserMobileType(String userId) {
		String key = mobileTypeKey + userId;
		return redisUtil.getString(key);
	}
}
