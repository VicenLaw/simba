package com.simba.framework.session.jobs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.simba.framework.session.zookeeper.ZooKeeperSessionHelper;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;

/**
 * 当分布式session使用zookeeper实现时，定时清过期session中的数据
 * 
 * @author caozj
 *
 */
@Component
@ConditionalOnProperty(value = "distribute.type", havingValue = "zookeeper")
public class ClearZookeeperSession {

	private static final Log logger = LogFactory.getLog(ClearZookeeperSession.class);

	@Scheduled(cron = "0 0/8 * * * *")
	private void initJobs() {
		logger.info("开始清过期zookeeper的session数据");
		((ZooKeeperSessionHelper) ApplicationContextUtil.getBean("zooKeeperSessionHelper")).clearTimeoutData();
		logger.info("完成清过期zookeeper的session数据");
	}

}
