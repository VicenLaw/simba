package com.simba.framework.distributed;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.task.TaskExecutor;

import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.framework.util.common.SerializeUtil;
import com.simba.framework.util.distributed.ClusterExecute;
import com.simba.framework.util.distributed.ClusterMessage;
import com.simba.framework.util.distributed.DistributedData;

import redis.clients.jedis.BinaryJedisPubSub;

/**
 * 订阅Redis的消息
 * 
 * @author caozj
 *
 */
public class RedisReceiveMessage extends BinaryJedisPubSub implements ReceiveMessageInterface {

	private static final Log logger = LogFactory.getLog(RedisReceiveMessage.class);

	@Override
	public void onMessage(byte[] channel, byte[] message) {
		TaskExecutor taskExecutor = (TaskExecutor) ApplicationContextUtil.getBean("redisSubTaskExecutor");
		taskExecutor.execute(() -> {
			ClusterMessage clusterMessage = (ClusterMessage) SerializeUtil.unserialize(message);
			logger.info("接收到集群" + SerializeUtil.unserialize(channel) + "的消息:" + clusterMessage.toString());
			ClusterExecute execute = DistributedData.getInstance().get(clusterMessage.getClassFullPath());
			if (execute != null) {
				execute.execute(clusterMessage.getData());
				logger.info(clusterMessage.getClassFullPath() + "执行完成");
			} else {
				logger.warn("没有找到" + clusterMessage.getClassFullPath());
			}
		});
	}

}
