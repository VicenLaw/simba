package com.simba.framework.distributed;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 处理redis订阅的线程池
 * 
 * @author caozhejun
 *
 */
@Configuration
public class RedisSubScheduleConfig {

	@Bean
	public TaskExecutor redisSubTaskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(20);
		taskExecutor.setMaxPoolSize(500);
		taskExecutor.setQueueCapacity(1000);
		return taskExecutor;
	}

}
