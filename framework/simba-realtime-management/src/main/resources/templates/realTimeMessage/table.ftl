<#list list as realTimeMessage>
	<tr>
		<td>${realTimeMessage.appid}</td>
		<td>${realTimeMessage.userId}</td>
		<td>${realTimeMessage.message}</td>
		<td>${realTimeMessage.statusDescription!}</td>
		<td>${realTimeMessage.createTime!}</td>
		<td>${realTimeMessage.availTime!}</td>
		<td>${realTimeMessage.reachTime!}</td>
	</tr>
</#list>