package com.simba.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simba.dao.RealTimeMessageDao;
import com.simba.framework.util.jdbc.Jdbc;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.jdbc.StatementParameter;
import com.simba.model.RealTimeMessage;
import com.simba.model.RealTimeMessageStatus;
import com.simba.model.form.RealTimeMessageSearchForm;
import com.simba.util.CallbackUtil;

/**
 * 实时推送消息表 Dao实现类
 * 
 * @author caozj
 * 
 */
@Repository
public class RealTimeMessageDaoImpl implements RealTimeMessageDao {

	@Autowired
	private Jdbc jdbc;

	@Autowired
	private CallbackUtil callbackUtil;

	private static final String table = "realTimeMessage";

	@Override
	public void add(RealTimeMessage realTimeMessage) {
		String sql = "insert into " + table + "(id, userId, message, createTime,appid,status,availTime,reachTime,callbackUrl,callbackResult) values(?,?,?,?,?,?,?,?,?,?)";
		jdbc.updateForGeneratedKey(sql, realTimeMessage.getId(), realTimeMessage.getUserId(), realTimeMessage.getMessage(), realTimeMessage.getCreateTime(), realTimeMessage.getAppid(),
				realTimeMessage.getStatus(), realTimeMessage.getAvailTime(), realTimeMessage.getReachTime(), realTimeMessage.getCallbackUrl(), realTimeMessage.getCallbackResult());
	}

	@Override
	public void update(RealTimeMessage realTimeMessage) {
		String sql = "update " + table
				+ " set  userId = ? , message = ? , createTime = ? ,appid = ? , status = ? , availTime = ? , reachTime = ? , callbackUrl = ? , callbackResult =  ? where id = ?  ";
		jdbc.updateForBoolean(sql, realTimeMessage.getUserId(), realTimeMessage.getMessage(), realTimeMessage.getCreateTime(), realTimeMessage.getAppid(), realTimeMessage.getStatus(),
				realTimeMessage.getAvailTime(), realTimeMessage.getReachTime(), realTimeMessage.getCallbackUrl(), realTimeMessage.getCallbackResult(), realTimeMessage.getId());
	}

	@Override
	public void delete(Long id) {
		String sql = "delete from " + table + " where id = ? ";
		jdbc.updateForBoolean(sql, id);
	}

	@Override
	public List<RealTimeMessage> page(Pager page) {
		String sql = "select * from " + table;
		return jdbc.queryForPage(sql, RealTimeMessage.class, page);
	}

	@Override
	public List<RealTimeMessage> page(Pager page, RealTimeMessageSearchForm searchForm) {
		String sql = "select * from " + table;
		StatementParameter param = new StatementParameter();
		sql = buildCondition(sql, searchForm, param);
		sql += " order by createTime desc";
		return jdbc.queryForPage(sql, RealTimeMessage.class, page, param);
	}

	@Override
	public List<RealTimeMessage> listAll() {
		String sql = "select * from " + table;
		return jdbc.queryForList(sql, RealTimeMessage.class);
	}

	@Override
	public Long count() {
		String sql = "select count(*) from " + table;
		return jdbc.queryForLong(sql);
	}

	@Override
	public RealTimeMessage get(Long id) {
		String sql = "select * from " + table + " where id = ? ";
		return jdbc.query(sql, RealTimeMessage.class, id);
	}

	@Override
	public RealTimeMessage getBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.query(sql, RealTimeMessage.class, value);
	}

	@Override
	public RealTimeMessage getByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.query(sql, RealTimeMessage.class, value1, value2);
	}

	@Override
	public RealTimeMessage getByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.query(sql, RealTimeMessage.class, value1, value2);
	}

	@Override
	public List<RealTimeMessage> listBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.queryForList(sql, RealTimeMessage.class, value);
	}

	@Override
	public List<RealTimeMessage> listByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.queryForList(sql, RealTimeMessage.class, value1, value2);
	}

	@Override
	public List<RealTimeMessage> listByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.queryForList(sql, RealTimeMessage.class, value1, value2);
	}

	@Override
	public List<RealTimeMessage> pageBy(String field, Object value, Pager page) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value);
		return jdbc.queryForPage(sql, RealTimeMessage.class, page, param);
	}

	@Override
	public List<RealTimeMessage> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, RealTimeMessage.class, page, param);
	}

	@Override
	public List<RealTimeMessage> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, RealTimeMessage.class, page, param);
	}

	@Override
	public Long countBy(String field, Object value) {
		String sql = "select count(*) from " + table + " where " + field + " = ? ";
		return jdbc.queryForLong(sql, value);
	}

	@Override
	public void deleteBy(String field, Object value) {
		String sql = "delete from " + table + " where " + field + " = ? ";
		jdbc.updateForBoolean(sql, value);
	}

	private String buildCondition(String sql, RealTimeMessageSearchForm searchForm, StatementParameter param) {
		sql += " where 1=1 ";
		if (StringUtils.isNotEmpty(searchForm.getUserId())) {
			sql += " and userId = ? ";
			param.setString(searchForm.getUserId());
		}
		if (StringUtils.isNotEmpty(searchForm.getAppid())) {
			sql += " and appid = ? ";
			param.setString(searchForm.getAppid());
		}
		if (StringUtils.isNotEmpty(searchForm.getMessage())) {
			sql += " and message like '%" + searchForm.getMessage() + "%'";
		}
		if (StringUtils.isNotEmpty(searchForm.getStartTime())) {
			sql += " and createTime > ? ";
			param.setString(searchForm.getStartTime());
		}
		if (StringUtils.isNotEmpty(searchForm.getEndTime())) {
			sql += " and createTime < ? ";
			param.setString(searchForm.getEndTime());
		}
		if (searchForm.getStatus() != null) {
			sql += " and status = ? ";
			param.setInt(searchForm.getStatus());
		}
		return sql;
	}

	@Override
	public Long count(RealTimeMessageSearchForm searchForm) {
		String sql = "select count(*) from " + table;
		StatementParameter param = new StatementParameter();
		sql = buildCondition(sql, searchForm, param);
		return jdbc.queryForLong(sql, param);
	}

	@Override
	public void updateSendingMessage() {
		Date time = DateUtils.addMinutes(new Date(), -5);
		String sql = "update " + table + " set status = " + RealTimeMessageStatus.FAIL.getId() + " where status = " + RealTimeMessageStatus.SENDING.getId() + " and createTime < ? ";
		jdbc.updateForBoolean(sql, time);
	}

	@Override
	public void updateTimeoutMessage() {
		String sql = "update " + table + " set status = " + RealTimeMessageStatus.TIMEOUT.getId() + " where status = " + RealTimeMessageStatus.UNSEND.getId() + " and availTime < ? ";
		jdbc.updateForBoolean(sql, new Date());
	}

	@Override
	public List<Long> listAllNeedSend(String userId, String appid) {
		String sql = "select id from " + table + " where userId = ? and appid = ? and status = ? and availTime > ? ";
		return jdbc.queryForLongs(sql, userId, appid, RealTimeMessageStatus.UNSEND.getId(), new Date());
	}

	@Override
	public void updateSuccess(RealTimeMessage realTimeMessage) {
		String sql = "update " + table + " set  status = ? ,  reachTime = ? where id = ? ";
		jdbc.updateForBoolean(sql, RealTimeMessageStatus.SUCCESS.getId(), new Date(), realTimeMessage.getId());
		callbackUtil.callback(realTimeMessage);
	}

	@Override
	public void updateCallbackResult(long id) {
		String sql = "update " + table + " set callbackResult = 1 where id = ? ";
		jdbc.updateForBoolean(sql, id);
	}

	@Override
	public List<Long> listAllNeedCallback() {
		String sql = "select id from " + table + " where callbackUrl is not null and callbackUrl != '' and callbackResult = 0  and reachTime > ? ";
		return jdbc.queryForLongs(sql, DateUtils.addDays(new Date(), -1));
	}

}
