package com.simba.dao;

import java.util.List;

import com.simba.framework.util.jdbc.Pager;
import com.simba.model.RealTimeMessage;
import com.simba.model.form.RealTimeMessageSearchForm;

/**
 * 实时推送消息表 Dao表 Dao
 * 
 * @author caozj
 * 
 */
public interface RealTimeMessageDao {

	void add(RealTimeMessage realTimeMessage);

	void update(RealTimeMessage realTimeMessage);

	void delete(Long id);

	List<RealTimeMessage> listAll();

	Long count();

	Long countBy(String field, Object value);

	void deleteBy(String field, Object value);

	List<RealTimeMessage> page(Pager page);

	List<RealTimeMessage> page(Pager page, RealTimeMessageSearchForm searchForm);

	RealTimeMessage get(Long id);

	RealTimeMessage getBy(String field, Object value);

	RealTimeMessage getByAnd(String field1, Object value1, String field2, Object value2);

	RealTimeMessage getByOr(String field1, Object value1, String field2, Object value2);

	List<RealTimeMessage> listBy(String field, Object value);

	List<RealTimeMessage> listByAnd(String field1, Object value1, String field2, Object value2);

	List<RealTimeMessage> listByOr(String field1, Object value1, String field2, Object value2);

	List<RealTimeMessage> pageBy(String field, Object value, Pager page);

	List<RealTimeMessage> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<RealTimeMessage> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);

	Long count(RealTimeMessageSearchForm searchForm);

	/**
	 * 处理发送中的消息，消息是不缓存的，如果用户在线则直接发送，用户不在线则丢弃消息变成用户不在线，发送失败状态
	 */
	void updateSendingMessage();

	/**
	 * 处理过期消息
	 */
	void updateTimeoutMessage();

	/**
	 * 列出所有需要发送的消息
	 * 
	 * @param userId
	 * @param appid
	 * @return
	 */
	List<Long> listAllNeedSend(String userId, String appid);

	/**
	 * 更新消息为发送成功
	 * 
	 * @param realTimeMessage
	 */
	void updateSuccess(RealTimeMessage realTimeMessage);

	/**
	 * 更新回调结果为成功
	 * 
	 * @param id
	 */
	void updateCallbackResult(long id);

	/**
	 * 列出24小时内所有需要回调的消息
	 * 
	 * @return
	 */
	List<Long> listAllNeedCallback();

}
