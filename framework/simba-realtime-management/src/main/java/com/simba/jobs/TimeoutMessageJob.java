package com.simba.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.simba.service.RealTimeMessageService;

/**
 * 处理过期消息
 * 
 * @author caozhejun
 *
 */
@Component
public class TimeoutMessageJob {

	@Autowired
	private RealTimeMessageService realTimeMessageService;

	@Scheduled(fixedRate = 60000, initialDelay = 5000)
	private void updateSendingMessage() {
		realTimeMessageService.updateTimeoutMessage();
	}

}
