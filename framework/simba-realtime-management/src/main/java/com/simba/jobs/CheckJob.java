package com.simba.jobs;

import java.io.IOException;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.simba.cache.Redis;
import com.simba.registry.util.RegistryUtil;
import com.simba.websocket.UserIdConnectionPool;

/**
 * 定时检查websocket连接心跳
 * 
 * @author caozhejun
 *
 */
@Component
public class CheckJob {

	private static final Log logger = LogFactory.getLog(CheckJob.class);

	private int timeout = 60 * 1000;

	private String appids;

	@Autowired
	private Redis redis;

	@Scheduled(fixedRate = 30000, initialDelay = 5000)
	private void check() {
		appids = StringUtils.defaultString(RegistryUtil.get("websocket.check.appids"));
		if (StringUtils.isEmpty(appids)) {
			return;
		}
		String[] appidList = appids.split(",");
		for (String appid : appidList) {
			check(appid.trim());
		}
	}

	private void check(String appid) {
		logger.debug("开始检查心跳:" + appid);
		Set<String> keys = redis.hkeys(appid);
		logger.debug(appid + "需要检查的在线用户为" + keys.toString());
		keys.forEach((String key) -> {
			String[] ks = key.split("_");
			if (ks.length == 2) {
				String userId = ks[0];
				String sid = ks[1];
				long lastTime = NumberUtils.toLong(redis.hgetString(appid, key));
				if (System.currentTimeMillis() - lastTime > timeout) {
					WebSocketSession session = UserIdConnectionPool.getInstance().get(userId, appid, sid);
					if (session != null) {
						try {
							session.close();
							logger.info("[appid:" + appid + "][userId:" + userId + "][sid:" + sid + "]已经超时没有发送心跳消息,将其踢出");
						} catch (IOException e) {
							logger.error("关闭websocket session发生异常[appid:" + appid + "][userId:" + userId + "][sid:" + sid + "]", e);
						}
					}
				}
			}
		});
	}

}
