package com.simba.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.simba.service.RealTimeMessageService;

/**
 * 回调推送结果定时器
 * 
 * @author caozhejun
 *
 */
@Component
public class CallbackJob {

	@Autowired
	private RealTimeMessageService realTimeMessageService;

	@Scheduled(fixedRate = 600000, initialDelay = 5000)
	private void updateCallbackMessage() {
		realTimeMessageService.updateCallbackMessage();
	}
}
