package com.simba.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.simba.service.RealTimeMessageService;

/**
 * 处理发送中的消息，消息是不缓存的，如果用户在线则直接发送，用户不在线则丢弃消息变成用户不在线，发送失败状态
 * 
 * @author caozhejun
 *
 */
@Component
public class SendingMessageJob {

	@Autowired
	private RealTimeMessageService realTimeMessageService;

	@Scheduled(fixedRate = 300000, initialDelay = 5000)
	private void updateSendingMessage() {
		realTimeMessageService.updateSendingMessage();
	}

}
