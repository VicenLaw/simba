package com.simba.websocket.distributed;

import java.io.Serializable;

/**
 * 推送实时消息给用户在集群中传递的对象
 * 
 * @author caozhejun
 *
 */
public class UserIdMessageData implements Serializable {

	private static final long serialVersionUID = -7521849117967501233L;

	private long msgId;

	private String userId;

	private String message;

	private String appid;

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserIdMessageData [msgId=");
		builder.append(msgId);
		builder.append(", userId=");
		builder.append(userId);
		builder.append(", message=");
		builder.append(message);
		builder.append(", appid=");
		builder.append(appid);
		builder.append("]");
		return builder.toString();
	}

}
