package com.simba.websocket;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import com.simba.dao.RealTimeMessageDao;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.framework.util.common.PropertyDecodeUtil;
import com.simba.model.RealTimeMessage;
import com.simba.model.RealTimeMessageStatus;
import com.simba.model.constant.ConstantData;
import com.simba.registry.util.RegistryUtil;
import com.simba.util.OnlineUserUtil;

/**
 * 用户websocket连接处理类
 * 
 * @author caozhejun
 *
 */
public class UserIdWebSocketHandler implements WebSocketHandler {

	private static final Log logger = LogFactory.getLog(UserIdWebSocketHandler.class);

	private static final Set<String> onlineSet = Collections.synchronizedSet(new HashSet<>());

	private void addOnlineUser(String appid, String userId, String sid) throws UnknownHostException {
		String key = appid + ":" + userId;
		if (onlineSet.add(key)) {
			OnlineUserUtil onlineUserUtil = ApplicationContextUtil.getBean(OnlineUserUtil.class);
			onlineUserUtil.incrOnlineUser(appid);
			logger.info("用户上线:[appid:" + appid + "][userId:" + userId + "][sid:" + sid + "]");
		}
	}

	private void addOfflineUser(String appid, String userId, String sid) throws UnknownHostException {
		String key = appid + ":" + userId;
		if (onlineSet.remove(key)) {
			OnlineUserUtil onlineUserUtil = ApplicationContextUtil.getBean(OnlineUserUtil.class);
			onlineUserUtil.incrOfflineUser(appid);
			logger.info("用户下线:[appid:" + appid + "][userId:" + userId + "][sid:" + sid + "]");
		}
	}

	private void set(String userId, String appid, WebSocketSession session) {
		session.getAttributes().put("userId", userId);
		session.getAttributes().put("appid", appid);
		logger.debug("设置连接用户[sid:" + session.getId() + "][userId:" + userId + "][appid:" + appid + "]");
	}

	private String getUserId(WebSocketSession session) {
		Map<String, Object> attributes = session.getAttributes();
		String userId = null;
		if (attributes == null) {
			logger.error("getUserId:没有session的attribute");
		} else {
			userId = (String) attributes.get("userId");
			logger.debug("从session中获取userId[" + userId + "]");
		}
		logger.debug("获取连接用户[" + session.getId() + "][" + userId + "]");
		return userId;
	}

	private String getAppid(WebSocketSession session) {
		Map<String, Object> attributes = session.getAttributes();
		String appid = null;
		if (attributes == null) {
			logger.error("getAppid:没有session的attribute");
		} else {
			appid = (String) attributes.get("appid");
			logger.debug("从session中获取appid[" + appid + "]");
		}
		logger.debug("获取连接应用[" + session.getId() + "][" + appid + "]");
		return appid;
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		logger.debug("用户websocket连接成功sid:" + session.getId());
		String userId = getUserId(session);
		String appid = getAppid(session);
		if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(appid)) {
			UserIdConnectionPool.getInstance().add(userId, appid, session);
			logger.debug("用户连接Session保存成功[userId:" + userId + "][appid:" + appid + "][sid:" + session.getId() + "]");
			addOnlineUser(appid, userId, session.getId());
			send(userId, appid, session);
		}
		logger.info("此服务器上连接的用户有******" + UserIdConnectionPool.getInstance().getAllOnlineUsers().toString() + "******");
	}

	/**
	 * userId:1,appid:smarthome or userId:1
	 */
	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		Object payload = message.getPayload();
		if (payload == null) {
			logger.error("收到用户websocket消息，但是内容为空");
			return;
		}
		String content = payload.toString();
		if (StringUtils.isEmpty(content)) {
			logger.error("收到用户websocket消息，但是内容为空");
			return;
		}
		if (content.startsWith(ConstantData.MISPLIT)) {
			logger.debug("收到用户websocket【密文】消息:[" + content + "]");
			// 解密
			content = PropertyDecodeUtil.resolveValue(content);
		}
		logger.debug("收到用户websocket【明文】消息:[" + content + "]");
		if (content.startsWith("userId:")) {
			String[] data = content.split(",");
			String userId = data[0].split(":")[1];
			String appid = null;
			if (data.length > 1) {
				appid = data[1].split(":")[1];
			} else {
				appid = "default";
			}
			UserIdConnectionPool.getInstance().add(userId, appid, session);
			logger.debug("用户连接Session保存成功[userId:" + userId + "][appid:" + appid + "]");
			set(userId, appid, session);
			addOnlineUser(appid, userId, session.getId());
			send(userId, appid, session);
			String appids = StringUtils.defaultString(RegistryUtil.get("websocket.check.appids"));
			if (StringUtils.isNotEmpty(appids)) {
				String[] appidList = appid.split(",");
				for (String sendAppId : appidList) {
					if (sendAppId.trim().equals(appid)) {
						// 收到消息之后立刻回复ok
						TextMessage text = new TextMessage("ok");
						session.sendMessage(text);
					}
				}
			}
		} else {
			logger.error("收到用户websocket消息，但是内容格式错误:[" + content + "]");
		}
	}

	/**
	 * 推送缓存的还未过期的消息给用户
	 * 
	 * @param userId
	 * @param appid
	 * @param session
	 * @throws IOException
	 */
	private void send(String userId, String appid, WebSocketSession session) throws IOException {
		RealTimeMessageDao dao = ApplicationContextUtil.getBean(RealTimeMessageDao.class);
		List<Long> needSendIds = dao.listAllNeedSend(userId, appid);
		for (Long id : needSendIds) {
			RealTimeMessage message = dao.get(id);
			if (message.getStatus() != RealTimeMessageStatus.UNSEND.getId()) {
				continue;
			}
			String content = message.getMessage();
			TextMessage text = new TextMessage(content);
			session.sendMessage(text);
			logger.info("用户上线,推送给用户[appid:" + appid + "][userId:" + userId + "]的内容:" + content + "[成功]");
			dao.updateSuccess(message);
		}
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		logger.error("用户websocket连接发送异常:" + session.getId());
		String userId = getUserId(session);
		String appid = getAppid(session);
		logger.error("用户websocket连接发生异常:[userId:" + userId + "][appid:" + appid + "][sid:" + session.getId() + "]", exception);
		if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(appid)) {
			UserIdConnectionPool.getInstance().remove(userId, appid, session);
			logger.info("清空websocket连接[userId:" + userId + "][appid:" + appid + "]");
			addOfflineUser(appid, userId, session.getId());
		}
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
		logger.error("用户websocket连接关闭:" + session.getId());
		String userId = getUserId(session);
		String appid = getAppid(session);
		logger.info("用户websocket连接关闭:[userId:" + userId + "][appid:" + appid + "][sid:" + session.getId() + "]," + closeStatus);
		if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(appid)) {
			UserIdConnectionPool.getInstance().remove(userId, appid, session);
			logger.info("清空websocket连接[userId:" + userId + "][appid:" + appid + "]");
			addOfflineUser(appid, userId, session.getId());
		}
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}

}
