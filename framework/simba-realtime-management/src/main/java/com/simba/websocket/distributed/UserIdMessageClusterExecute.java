package com.simba.websocket.distributed;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.simba.framework.util.distributed.ClusterExecute;
import com.simba.service.RealTimeMessageService;
import com.simba.websocket.UserIdConnectionPool;

/**
 * 用户实时推送消息的集群处理类
 * 
 * @author caozhejun
 *
 */
@Component
public class UserIdMessageClusterExecute implements ClusterExecute {

	private static final Log logger = LogFactory.getLog(UserIdMessageClusterExecute.class);

	@Autowired
	private RealTimeMessageService realTimeMessageService;

	@Override
	public void execute(Object data) {
		if (!(data instanceof UserIdMessageData)) {
			logger.error("类型错误：" + data.getClass().getCanonicalName() + ",应该为" + UserIdMessageData.class.getName());
			return;
		}
		logger.info("此服务器上连接的用户有******" + UserIdConnectionPool.getInstance().getAllOnlineUsers().toString() + "******");
		UserIdMessageData messageData = (UserIdMessageData) data;
		logger.info("接收到集群处理消息:" + messageData.toString());
		String userId = messageData.getUserId();
		String appid = messageData.getAppid();
		List<WebSocketSession> sessions = UserIdConnectionPool.getInstance().get(userId, appid);
		if (sessions.size() == 0) {
			return;
		}
		String content = messageData.getMessage();
		boolean success = false;
		for (WebSocketSession session : sessions) {
			if (session != null && session.isOpen()) {
				try {
					TextMessage text = new TextMessage(content);
					session.sendMessage(text);
					logger.info("接收到需要推送给用户[appid:" + appid + "][userId:" + userId + "]的内容:" + content + "[成功]");
					success = true;
				} catch (Exception e) {
					logger.error("接收到需要推送给用户[appid:" + appid + "][userId:" + userId + "]的内容:" + content + "[异常]", e);
				}
			}
		}
		if (success) {
			realTimeMessageService.updateSuccess(realTimeMessageService.get(messageData.getMsgId()));
		}
	}

}
