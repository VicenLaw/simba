package com.simba.websocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.socket.WebSocketSession;

import com.simba.cache.Redis;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.registry.util.RegistryUtil;

/**
 * 用户websocket连接Session对象池
 * 
 * @author caozhejun
 *
 */
public class UserIdConnectionPool {

	private Map<String, Map<String, WebSocketSession>> sessionMap;

	private UserIdConnectionPool() {
		init();
	}

	private void init() {
		sessionMap = new ConcurrentHashMap<>();
	}

	private static final class UserIdConnectionPoolHolder {
		private static final UserIdConnectionPool instance = new UserIdConnectionPool();
	}

	public static UserIdConnectionPool getInstance() {
		return UserIdConnectionPoolHolder.instance;
	}

	public void add(String userId, String appid, WebSocketSession session) {
		String key = buildKey(userId, appid);
		Map<String, WebSocketSession> userSessions = sessionMap.get(key);
		if (userSessions == null) {
			userSessions = new HashMap<>();
		}
		String appids = StringUtils.defaultString(RegistryUtil.get("websocket.check.appids"));
		if (StringUtils.isNotEmpty(appids)) {
			String[] appidList = appid.split(",");
			for (String sendAppId : appidList) {
				if (sendAppId.trim().equals(appid)) {
					userSessions.clear();
				}
			}
		}
		userSessions.put(session.getId(), session);
		sessionMap.put(key, userSessions);
		Redis redis = ApplicationContextUtil.getBean(Redis.class);
		redis.hsetString(appid, userId + "_" + session.getId(), System.currentTimeMillis() + "");
	}

	private String buildKey(String userId, String appid) {
		return appid + "_" + userId;
	}

	public void remove(String userId, String appid, WebSocketSession session) {
		this.remove(userId, appid, session.getId());
	}

	public void remove(String userId, String appid, String sid) {
		String key = buildKey(userId, appid);
		Map<String, WebSocketSession> userSessions = sessionMap.get(key);
		if (userSessions != null) {
			userSessions.remove(sid);
			sessionMap.put(key, userSessions);
		}
	}

	public List<WebSocketSession> get(String userId, String appid) {
		String key = buildKey(userId, appid);
		Map<String, WebSocketSession> userSessions = sessionMap.get(key);
		List<WebSocketSession> sessions = new ArrayList<>();
		if (userSessions != null) {
			sessions.addAll(userSessions.values());
		}
		return sessions;
	}

	public WebSocketSession get(String userId, String appid, String sid) {
		List<WebSocketSession> sessions = this.get(userId, appid);
		WebSocketSession currentSession = null;
		for (WebSocketSession session : sessions) {
			if (session.getId().equals(sid)) {
				currentSession = session;
				break;
			}
		}
		return currentSession;
	}

	public Set<String> all() {
		return sessionMap.keySet();
	}

	public Map<String, Map<String, WebSocketSession>> getAllSessions() {
		return sessionMap;
	}

	public Set<String> getAllOnlineUsers() {
		Set<String> onlineUsers = new HashSet<>();
		Set<String> keys = this.all();
		keys.forEach((String key) -> {
			Map<String, WebSocketSession> userSessions = sessionMap.get(key);
			userSessions.keySet().forEach((String sid) -> {
				onlineUsers.add("user:" + key + ",sid:" + sid);
			});
		});
		return onlineUsers;
	}

	public List<WebSocketSession> allSession() {
		Set<String> keys = this.all();
		List<WebSocketSession> sessions = new ArrayList<>();
		keys.forEach((String key) -> {
			Map<String, WebSocketSession> userSessions = sessionMap.get(key);
			if (userSessions != null && userSessions.size() > 0) {
				sessions.addAll(userSessions.values());
			}
		});
		return sessions;
	}
}
