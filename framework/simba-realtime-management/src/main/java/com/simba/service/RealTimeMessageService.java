package com.simba.service;

import java.util.List;

import com.simba.framework.util.jdbc.Pager;
import com.simba.model.RealTimeMessage;
import com.simba.model.form.RealTimeMessageSearchForm;

/**
 * 实时信息推送Service
 * 
 * @author caozj
 * 
 */
public interface RealTimeMessageService {

	void add(RealTimeMessage realTimeMessage);

	void update(RealTimeMessage realTimeMessage);

	void delete(Long id);

	List<RealTimeMessage> listAll();

	Long count();

	Long countBy(String field, Object value);

	void deleteBy(String field, Object value);

	List<RealTimeMessage> page(Pager page);

	List<RealTimeMessage> page(Pager page, RealTimeMessageSearchForm searchForm);

	RealTimeMessage get(Long id);

	void batchDelete(List<Long> idList);

	RealTimeMessage getBy(String field, Object value);

	RealTimeMessage getByAnd(String field1, Object value1, String field2, Object value2);

	RealTimeMessage getByOr(String field1, Object value1, String field2, Object value2);

	List<RealTimeMessage> listBy(String field, Object value);

	List<RealTimeMessage> listByAnd(String field1, Object value1, String field2, Object value2);

	List<RealTimeMessage> listByOr(String field1, Object value1, String field2, Object value2);

	List<RealTimeMessage> pageBy(String field, Object value, Pager page);

	List<RealTimeMessage> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<RealTimeMessage> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);

	Long count(RealTimeMessageSearchForm searchForm);

	/**
	 * 处理发送中的消息，消息是不缓存的，如果用户在线则直接发送，用户不在线则丢弃消息变成用户不在线，发送失败状态
	 */
	void updateSendingMessage();

	/**
	 * 处理过期消息
	 */
	void updateTimeoutMessage();

	/**
	 * 更新消息为发送成功
	 * 
	 * @param realTimeMessage
	 */
	void updateSuccess(RealTimeMessage realTimeMessage);

	/**
	 * 更新回调结果为成功
	 * 
	 * @param id
	 */
	void updateCallbackResult(long id);

	/**
	 * 处理回调消息
	 */
	void updateCallbackMessage();

}
