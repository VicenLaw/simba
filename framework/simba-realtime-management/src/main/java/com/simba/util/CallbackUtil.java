package com.simba.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.simba.dao.RealTimeMessageDao;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.model.RealTimeMessage;
import com.simba.okhttp.util.OkHttpClientUtil;

/**
 * 推送成功回调url工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class CallbackUtil {

	private static final Log logger = LogFactory.getLog(CallbackUtil.class);

	@Bean
	public TaskExecutor realtimeCallbackTaskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(20);
		taskExecutor.setMaxPoolSize(500);
		taskExecutor.setQueueCapacity(2000);
		return taskExecutor;
	}

	/**
	 * 通知调用方推送成功
	 * 
	 * @param realTimeMessage
	 * @throws IOException
	 */
	@Async("realtimeCallbackTaskExecutor")
	public void callback(RealTimeMessage realTimeMessage) {
		String url = realTimeMessage.getCallbackUrl();
		if (StringUtils.isEmpty(url)) {
			return;
		}
		Map<String, String> param = new HashMap<>(1);
		param.put("id", realTimeMessage.getId() + "");
		try {
			String response = OkHttpClientUtil.getInstance().post(url, param);
			if ("success".equals(response)) {
				ApplicationContextUtil.getBean(RealTimeMessageDao.class).updateCallbackResult(realTimeMessage.getId());
			}
		} catch (IOException e) {
			logger.error("回调" + url + "发生异常", e);
		}
	}
}
