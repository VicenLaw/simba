package com.simba.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 实时推送消息状态
 * 
 * @author caozhejun
 *
 */
public enum RealTimeMessageStatus {

	UNSEND(0, "未发送"),

	SUCCESS(1, "发送成功"),

	TIMEOUT(2, "已过期"),

	SENDING(3, "发送中"),

	FAIL(4, "用户不在线，发送失败");

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	private RealTimeMessageStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}

	private static final Map<Integer, RealTimeMessageStatus> maps = new HashMap<>();

	static {
		for (RealTimeMessageStatus status : RealTimeMessageStatus.values()) {
			maps.put(status.getId(), status);
		}
	}

	public static RealTimeMessageStatus get(int id) {
		return maps.get(id);
	}

}
