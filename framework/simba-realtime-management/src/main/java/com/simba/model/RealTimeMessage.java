package com.simba.model;

import java.util.Date;

import com.simba.annotation.DescAnnotation;

/**
 * 实时信息推送表
 * 
 * @author lilei
 *
 */
@DescAnnotation(desc = "实时信息推送表")
public class RealTimeMessage {

	/**
	 * 自增主键ID
	 */
	private long id;

	/**
	 * 用户ID
	 */
	@DescAnnotation(desc = "接收方ID")
	private String userId;
	/**
	 * 消息
	 */
	@DescAnnotation(desc = "消息")
	private String message;
	/**
	 * 时间
	 */
	@DescAnnotation(desc = "时间")
	private Date createTime;

	/**
	 * 应用id
	 */
	private String appid;

	private int status;

	private Date availTime;

	private Date reachTime;

	private String callbackUrl;

	private int callbackResult;

	private String statusDescription;

	public int getCallbackResult() {
		return callbackResult;
	}

	public void setCallbackResult(int callbackResult) {
		this.callbackResult = callbackResult;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getAvailTime() {
		return availTime;
	}

	public void setAvailTime(Date availTime) {
		this.availTime = availTime;
	}

	public Date getReachTime() {
		return reachTime;
	}

	public void setReachTime(Date reachTime) {
		this.reachTime = reachTime;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RealTimeMessage [id=");
		builder.append(id);
		builder.append(", userId=");
		builder.append(userId);
		builder.append(", message=");
		builder.append(message);
		builder.append(", createTime=");
		builder.append(createTime);
		builder.append(", appid=");
		builder.append(appid);
		builder.append(", status=");
		builder.append(status);
		builder.append(", availTime=");
		builder.append(availTime);
		builder.append(", reachTime=");
		builder.append(reachTime);
		builder.append(", callbackUrl=");
		builder.append(callbackUrl);
		builder.append(", callbackResult=");
		builder.append(callbackResult);
		builder.append(", statusDescription=");
		builder.append(statusDescription);
		builder.append("]");
		return builder.toString();
	}

}
