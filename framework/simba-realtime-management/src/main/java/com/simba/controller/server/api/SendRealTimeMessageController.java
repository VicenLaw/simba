package com.simba.controller.server.api;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simba.framework.async.distributed.AsyncDistributedUtil;
import com.simba.framework.util.distributed.ClusterMessage;
import com.simba.framework.util.id.SnowFlakeId;
import com.simba.framework.util.json.JsonResult;
import com.simba.model.RealTimeMessage;
import com.simba.model.RealTimeMessageStatus;
import com.simba.service.RealTimeMessageService;
import com.simba.websocket.distributed.UserIdMessageClusterExecute;
import com.simba.websocket.distributed.UserIdMessageData;

/**
 * 发送实时信息推送Controller
 * 
 * @author caozhejun
 *
 */
@RestController
@RequestMapping("/server/api/realTimeMessage")
public class SendRealTimeMessageController {

	private static final Log logger = LogFactory.getLog(SendRealTimeMessageController.class);

	@Autowired
	private AsyncDistributedUtil distributedUtil;

	@Resource
	private TaskExecutor taskExecutor;

	@Autowired
	private RealTimeMessageService realTimeMessageService;

	private SnowFlakeId idGenerator;

	@PostConstruct
	private void init() {
		idGenerator = new SnowFlakeId();
	}

	/**
	 * 根据用户ID推送实时消息(如果用户在线直接推送消息，如果用户不在线，消息不推送直接丢弃)
	 * 
	 * @param userId
	 * @param content
	 * @param appid
	 * @param callbackUrl
	 * 
	 * @return
	 */
	@RequestMapping("/send")
	public JsonResult send(String userId, String content, String appid, String callbackUrl) {
		logger.info("接收到需要推送给用户[appid:" + appid + "][userId:" + userId + "]的内容:" + content);
		long id = idGenerator.nextId();
		taskExecutor.execute(() -> {
			sendInCluster(id, userId, content, appid, RealTimeMessageStatus.SENDING.getId(), -1, callbackUrl);
		});
		return new JsonResult(id);
	}

	/**
	 * 根据用户ID推送实时消息(如果用户在线直接推送消息，如果用户不在线，则保存消息，在过期时间内，如果用户上线则立刻推送，如果过期用户还未上线，把消息丢弃)
	 * 
	 * @param userId
	 * @param content
	 * @param appid
	 * @param timeoutMinutes
	 * @param callbackUrl
	 * @return
	 */
	@RequestMapping("/sendWithTimeout")
	public JsonResult send(String userId, String content, String appid, int timeoutMinutes, String callbackUrl) {
		logger.info("接收到需要推送给用户[appid:" + appid + "][userId:" + userId + "][过期时间:" + timeoutMinutes + "分]的内容:" + content);
		long id = idGenerator.nextId();
		taskExecutor.execute(() -> {
			sendInCluster(id, userId, content, appid, RealTimeMessageStatus.UNSEND.getId(), timeoutMinutes, callbackUrl);
		});
		return new JsonResult(id);
	}

	private void sendInCluster(long id, String userId, String content, String appid, int status, int timeoutMinutes, String callbackUrl) {
		Date now = new Date();
		RealTimeMessage realTimeMessage = new RealTimeMessage();
		realTimeMessage.setId(id);
		realTimeMessage.setUserId(userId);
		realTimeMessage.setAppid(appid);
		realTimeMessage.setMessage(content);
		realTimeMessage.setStatus(status);
		realTimeMessage.setCreateTime(now);
		realTimeMessage.setCallbackUrl(callbackUrl);
		if (timeoutMinutes > 0) {
			realTimeMessage.setAvailTime(DateUtils.addMinutes(now, timeoutMinutes));
		}
		realTimeMessage.setCallbackResult(0);
		realTimeMessageService.add(realTimeMessage);
		UserIdMessageData data = new UserIdMessageData();
		data.setMsgId(id);
		data.setUserId(userId);
		data.setMessage(content);
		data.setAppid(appid);
		distributedUtil.executeInCluster(new ClusterMessage(UserIdMessageClusterExecute.class.getCanonicalName(), data));
	}
}
