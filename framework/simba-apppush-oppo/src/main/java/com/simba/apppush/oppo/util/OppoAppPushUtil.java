package com.simba.apppush.oppo.util;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.oppo.push.server.Notification;
import com.oppo.push.server.Result;
import com.oppo.push.server.Sender;
import com.oppo.push.server.Target;
import com.oppo.push.server.TargetType;

/**
 * OPPO App推送工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class OppoAppPushUtil {

	private static final Log logger = LogFactory.getLog(OppoAppPushUtil.class);

	@Value("${oppo.app.key:}")
	private String appKey;

	@Value("${oppo.app.secret:}")
	private String appSecret;

	@Value("${oppo.app.title:}")
	private String title;

	@Value("${oppo.app.subTitle:}")
	private String subTitle;

	private Sender sender;

	private boolean enable = false;

	public OppoAppPushUtil() {

	}

	public OppoAppPushUtil(String appKey, String appSecret, String title, String subTitle) {
		this.appKey = appKey;
		this.appSecret = appSecret;
		this.title = title;
		this.subTitle = subTitle;
	}

	public boolean isEnable() {
		return enable;
	}

	public Sender getSender() {
		return sender;
	}

	@PostConstruct
	public void init() throws Exception {
		try {
			if (StringUtils.isNotEmpty(appKey) && StringUtils.isNotEmpty(appSecret)) {
				sender = new Sender(appKey, appSecret);
				enable = true;
			}
		} catch (Exception e) {
			logger.error("初始化oppo推送发生异常[appKey:" + appKey + "][appSecret:" + appSecret + "]", e);
		}
	}

	/**
	 * 发送通知
	 * 
	 * @param token
	 *            用户手机注册id
	 * @param content
	 *            推送内容
	 * @return
	 * @throws Exception
	 */
	public Result send(String token, String content) throws Exception {
		Notification notification = new Notification();
		notification.setTitle(title);
		notification.setSubTitle(subTitle);
		notification.setContent(content);
		Target target = new Target();
		target.setTargetValue(token);
		target.setTargetType(TargetType.ALL);
		Result result = sender.unicastNotification(notification, target);
		logger.info("发送oppo App推送返回结果:" + result.getStatusCode() + "," + result.getReturnCode());
		return result;
	}

}
