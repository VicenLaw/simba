package com.simba.xiaomi.util;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.xiaomi.model.AndroidMessage;
import com.simba.xiaomi.model.IOSMessage;
import com.xiaomi.xmpush.server.Constants;
import com.xiaomi.xmpush.server.Result;
import com.xiaomi.xmpush.server.Sender;

/**
 * 小米App推送工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class XiaoMiAppPushUtil {

	private static final Log logger = LogFactory.getLog(XiaoMiAppPushUtil.class);

	@Value("${xiaomi.app.push.secret:}")
	private String appSecret;

	@Value("${xiaomi.app.title:}")
	private String title;

	@Value("${xiaomi.app.apk.package:}")
	private String apkPackage;

	private boolean enable = false;

	/**
	 * 重试次数
	 */
	private int retries = 3;

	private Sender sender;

	public Sender getSender() {
		return sender;
	}

	public XiaoMiAppPushUtil() {

	}

	public XiaoMiAppPushUtil(String appSecret, String title, String apkPackage) {
		this.appSecret = appSecret;
		this.title = title;
		this.apkPackage = apkPackage;
	}

	@PostConstruct
	public void init() {
		try {
			if (StringUtils.isNotEmpty(appSecret)) {
				Constants.useOfficial();
				sender = new Sender(appSecret);
				enable = true;
			}
		} catch (Exception e) {
			logger.error("初始化消息推送发生异常[appSecret:" + appSecret + "][title:" + title + "][apkPackage:" + apkPackage + "]", e);
		}
	}

	public boolean isEnable() {
		return enable;
	}

	/**
	 * 发送Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @param regid
	 *            设备注册id
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	public Result sendForAndriod(AndroidMessage message, String regid) throws IOException, ParseException {
		return sender.send(message.toMessage(), regid, retries);
	}

	/**
	 * 发送Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @param regids
	 *            设备注册id列表
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForAndriod(AndroidMessage message, List<String> regids) throws IOException, ParseException {
		return sender.send(message.toMessage(), regids, retries);
	}

	/**
	 * 发送Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @param alias
	 *            别名
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForAndriodByAlias(AndroidMessage message, String alias) throws IOException, ParseException {
		return sender.sendToAlias(message.toMessage(), alias, retries);
	}

	/**
	 * 发送Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @param alias
	 *            别名列表
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForAndriodByAlias(AndroidMessage message, List<String> alias) throws IOException, ParseException {
		return sender.sendToAlias(message.toMessage(), alias, retries);
	}

	/**
	 * 发送Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @param userAccount
	 *            账号
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForAndriodByUserAccount(AndroidMessage message, String userAccount) throws IOException, ParseException {
		return sender.sendToUserAccount(message.toMessage(), userAccount, retries);
	}

	/**
	 * 发送Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @param userAccounts
	 *            账号列表
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForAndriodByUserAccount(AndroidMessage message, List<String> userAccounts) throws IOException, ParseException {
		return sender.sendToUserAccount(message.toMessage(), userAccounts, retries);
	}

	/**
	 * 发送Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @param topic
	 *            主题
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForAndriodByTopic(AndroidMessage message, String topic) throws IOException, ParseException {
		return sender.broadcast(message.toMessage(), topic, retries);
	}

	/**
	 * 发送所有用户Android手机App推送
	 * 
	 * @param message
	 *            Android消息对象
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForAndriodAll(AndroidMessage message) throws IOException, ParseException {
		return sender.broadcastAll(message.toMessage(), retries);
	}

	/**
	 * 发送IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @param regid
	 *            设备注册id
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	public Result sendForIOS(IOSMessage message, String regid) throws IOException, ParseException {
		return sender.send(message.toMessage(), regid, retries);
	}

	/**
	 * 发送IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @param regids
	 *            设备注册id列表
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForIOS(IOSMessage message, List<String> regids) throws IOException, ParseException {
		return sender.send(message.toMessage(), regids, retries);
	}

	/**
	 * 发送IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @param alias
	 *            别名
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForIOSByAlias(IOSMessage message, String alias) throws IOException, ParseException {
		return sender.sendToAlias(message.toMessage(), alias, retries);
	}

	/**
	 * 发送IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @param alias
	 *            别名列表
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForIOSByAlias(IOSMessage message, List<String> alias) throws IOException, ParseException {
		return sender.sendToAlias(message.toMessage(), alias, retries);
	}

	/**
	 * 发送IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @param userAccount
	 *            账号
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForIOSByUserAccount(IOSMessage message, String userAccount) throws IOException, ParseException {
		return sender.sendToUserAccount(message.toMessage(), userAccount, retries);
	}

	/**
	 * 发送IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @param userAccounts
	 *            账号列表
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForIOSByUserAccount(IOSMessage message, List<String> userAccounts) throws IOException, ParseException {
		return sender.sendToUserAccount(message.toMessage(), userAccounts, retries);
	}

	/**
	 * 发送IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @param topic
	 *            主题
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForIOSByTopic(IOSMessage message, String topic) throws IOException, ParseException {
		return sender.broadcast(message.toMessage(), topic, retries);
	}

	/**
	 * 发送所有用户IOS手机App推送
	 * 
	 * @param message
	 *            IOS消息对象
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result sendForIOSAll(IOSMessage message) throws IOException, ParseException {
		return sender.broadcastAll(message.toMessage(), retries);
	}

	/**
	 * 给小米用户发送消息
	 * 
	 * @param user
	 *            用户id
	 * @param content
	 *            内容
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	public Result send(String user, String content) throws IOException, ParseException {
		AndroidMessage message = buildMessage(content);
		return this.sendForAndriodByAlias(message, user);
	}

	/**
	 * 给小米用户发送消息
	 * 
	 * @param users
	 *            用户id列表
	 * @param content
	 *            内容
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public Result send(List<String> users, String content) throws IOException, ParseException {
		AndroidMessage message = buildMessage(content);
		return this.sendForAndriodByAlias(message, users);
	}

	private AndroidMessage buildMessage(String content) {
		AndroidMessage message = new AndroidMessage();
		message.setTitle(title);
		message.setDescription(content);
		message.setPackageName(apkPackage);
		return message;
	}
}
