package com.simba.xiaomi.model;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.xiaomi.xmpush.server.Message;
import com.xiaomi.xmpush.server.Message.Builder;

/**
 * App推送安卓消息
 * 
 * @author caozhejun
 *
 */
public class AndroidMessage {

	/**
	 * 设置要发送的消息内容payload, 不允许全是空白字符, 长度小于4K, 一个中英文字符均计算为1(透传消息回传给APP, 为必填字段,
	 * 非透传消息可选)
	 */
	private String payload;

	/**
	 * 设置在通知栏展示的通知的标题, 不允许全是空白字符, 长度小于50, 一个中英文字符均计算为1(通知栏消息必填)
	 */
	private String title;

	/**
	 * 设置在通知栏展示的通知描述, 不允许全是空白字符, 长度小于128, 一个中英文字符均计算为1(通知栏消息必填)
	 */
	private String description;

	/**
	 * 设置消息是否通过透传的方式至App, 1表示透传消息, 0表示通知栏消息(默认是通知栏消息)
	 */
	private int passThrough = 0;

	/**
	 * 设置通知类型, type类型(-1, 1-使用默认提示音提示, 2-使用默认震动提示, 3-使用默认led灯光提示)
	 */
	private Integer type = 1;

	/**
	 * 设置app的包名packageName, packageName必须和开发者网站上申请的结果一致
	 */
	private String packageName;

	/**
	 * 设置app的多包名packageNames(多包名发送广播消息), packageNames必须和开发者网站上申请的结果一致, 可以为空,
	 * 为空则默认给所有渠道包名推送(不能同时调用restrictedPackageName方法和restrictedPackageNames方法)
	 */
	private String[] packageNames;

	/**
	 * 可选项, 消息的生命周期, 若用户离线, 设置消息在服务器保存的时间, 单位: ms(服务器默认最长保留两周)
	 */
	private int millisecondsLive;

	/**
	 * 可选项, 定时发送消息, timeToSend是以毫秒为单位的时间戳(仅支持七天内的定时消息)
	 */
	private long millisecondsSend;

	/**
	 * 可选项, 默认情况下, 通知栏只显示一条推送消息, 如果通知栏要显示多条推送消息,
	 * 需要针对不同的消息设置不同的notify_id(相同notify_id的通知栏消息会覆盖之前的)
	 */
	private Integer id;

	/**
	 * 可选项, 控制消息是否需要进行[平滑推送](/message/advanced/flow.html)(qps less 3000/second),
	 * 默认不支持
	 */
	private boolean needFlowControl = false;

	/**
	 * 可选项, 对app提供一些[扩展功能](/message/advance/index.html)(除了这些扩展功能,
	 * 开发者还可以定义一些key和value来控制客户端的行为, 注:key和value的字符数不能超过1024,
	 * 至多可以设置10个key-value键值对)
	 */
	private Map<String, String> extra;

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPassThrough() {
		return passThrough;
	}

	public void setPassThrough(int passThrough) {
		this.passThrough = passThrough;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String[] getPackageNames() {
		return packageNames;
	}

	public void setPackageNames(String[] packageNames) {
		this.packageNames = packageNames;
	}

	public int getMillisecondsLive() {
		return millisecondsLive;
	}

	public void setMillisecondsLive(int millisecondsLive) {
		this.millisecondsLive = millisecondsLive;
	}

	public long getMillisecondsSend() {
		return millisecondsSend;
	}

	public void setMillisecondsSend(long millisecondsSend) {
		this.millisecondsSend = millisecondsSend;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isNeedFlowControl() {
		return needFlowControl;
	}

	public void setNeedFlowControl(boolean needFlowControl) {
		this.needFlowControl = needFlowControl;
	}

	public Map<String, String> getExtra() {
		return extra;
	}

	public void setExtra(Map<String, String> extra) {
		this.extra = extra;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AndroidMessage [payload=");
		builder.append(payload);
		builder.append(", title=");
		builder.append(title);
		builder.append(", description=");
		builder.append(description);
		builder.append(", passThrough=");
		builder.append(passThrough);
		builder.append(", type=");
		builder.append(type);
		builder.append(", packageName=");
		builder.append(packageName);
		builder.append(", packageNames=");
		builder.append(Arrays.toString(packageNames));
		builder.append(", millisecondsLive=");
		builder.append(millisecondsLive);
		builder.append(", millisecondsSend=");
		builder.append(millisecondsSend);
		builder.append(", id=");
		builder.append(id);
		builder.append(", needFlowControl=");
		builder.append(needFlowControl);
		builder.append(", extra=");
		builder.append(extra);
		builder.append("]");
		return builder.toString();
	}

	public Message toMessage() {
		Builder builder = new Message.Builder();
		if (StringUtils.isNotEmpty(payload)) {
			builder.payload(payload);
		}
		builder.title(title);
		builder.description(description);
		builder.passThrough(passThrough);
		builder.notifyType(type);
		if (StringUtils.isNotEmpty(packageName)) {
			builder.restrictedPackageName(packageName);
		} else {
			builder.restrictedPackageNames(packageNames);
		}
		if (millisecondsLive > 0) {
			builder.timeToLive(millisecondsLive);
		}
		if (millisecondsSend > 0) {
			builder.timeToSend(millisecondsSend);
		}
		if (id != null && id > 0) {
			builder.notifyId(id);
		}
		builder.enableFlowControl(needFlowControl);
		if (extra != null && extra.size() > 0) {
			extra.forEach((key, value) -> {
				builder.extra(key, value);
			});
		}
		return builder.build();
	}

}
