package com.simba.xiaomi.model;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.xiaomi.xmpush.server.Message;
import com.xiaomi.xmpush.server.Message.IOSBuilder;

/**
 * App推送IOS消息
 * 
 * @author caozhejun
 *
 */
public class IOSMessage {

	/**
	 * 设置在通知栏展示的通知描述
	 */
	private String description;

	/**
	 * 可选项, 消息的生命周期, 若用户离线, 设置消息在服务器保存的时间, 单位: ms(服务器默认最长保留两周)
	 */
	private int millisecondsLive;

	/**
	 * 可选项, 定时发送消息, timeToSend是以毫秒为单位的时间戳(仅支持七天内的定时消息)
	 */
	private long millisecondsSend;

	/**
	 * 可选项, 自定义消息铃声
	 */
	private String url;

	/**
	 * 可选项, 自定义通知数字角标
	 */
	private int badge;

	/**
	 * 可选项, IOS8推送消息快速回复类别
	 */
	private String category;

	/**
	 * 可选项, 自定义键值对, 控制客户端行为(至多可以设置10个key-value键值对)
	 */
	private Map<String, String> extra;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMillisecondsLive() {
		return millisecondsLive;
	}

	public void setMillisecondsLive(int millisecondsLive) {
		this.millisecondsLive = millisecondsLive;
	}

	public long getMillisecondsSend() {
		return millisecondsSend;
	}

	public void setMillisecondsSend(long millisecondsSend) {
		this.millisecondsSend = millisecondsSend;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getBadge() {
		return badge;
	}

	public void setBadge(int badge) {
		this.badge = badge;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Map<String, String> getExtra() {
		return extra;
	}

	public void setExtra(Map<String, String> extra) {
		this.extra = extra;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IOSMessage [description=");
		builder.append(description);
		builder.append(", millisecondsLive=");
		builder.append(millisecondsLive);
		builder.append(", millisecondsSend=");
		builder.append(millisecondsSend);
		builder.append(", url=");
		builder.append(url);
		builder.append(", badge=");
		builder.append(badge);
		builder.append(", category=");
		builder.append(category);
		builder.append(", extra=");
		builder.append(extra);
		builder.append("]");
		return builder.toString();
	}

	public Message toMessage() {
		IOSBuilder builder = new Message.IOSBuilder();
		builder.description(description);
		if (millisecondsLive > 0) {
			builder.timeToLive(millisecondsLive);
		}
		if (millisecondsSend > 0) {
			builder.timeToSend(millisecondsSend);
		}
		if (StringUtils.isNotEmpty(url)) {
			builder.soundURL(url);
		}
		if (badge > 0) {
			builder.badge(badge);
		}
		if (StringUtils.isNotEmpty(category)) {
			builder.category(category);
		}
		if (extra != null && extra.size() > 0) {
			extra.forEach((key, value) -> {
				builder.extra(key, value);
			});
		}
		return builder.build();
	}

}
