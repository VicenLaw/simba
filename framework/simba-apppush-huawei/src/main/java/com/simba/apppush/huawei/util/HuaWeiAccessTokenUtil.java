package com.simba.apppush.huawei.util;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.apppush.huawei.exception.HuaWeiException;
import com.simba.apppush.huawei.model.HuaWeiAccessToken;
import com.simba.cache.Redis;
import com.simba.exception.SimbaException;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.okhttp.util.OkHttpClientUtil;

import okhttp3.Request;
import okhttp3.Response;

/**
 * 华为accesstoken工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class HuaWeiAccessTokenUtil {

	private static final Log logger = LogFactory.getLog(HuaWeiAccessTokenUtil.class);

	/**
	 * 获取华为accesstoken的url地址
	 */
	private static final String url = "https://login.cloud.huawei.com/oauth2/v2/token";

	private static final String key = "Simba:String:Huawei:AccessToken:";

	@Autowired
	private Redis redisUtil;

	@Value("${huawei.app.id:}")
	private String appid;

	@Value("${huawei.app.secret:}")
	private String appSecret;

	private String accessTokenUrl;

	private String redisKey;

	public HuaWeiAccessTokenUtil() {

	}

	public HuaWeiAccessTokenUtil(String appid, String appSecret) {
		this.appid = appid;
		this.appSecret = appSecret;
	}

	public HuaWeiAccessTokenUtil(String appid, String appSecret, Redis redisUtil) {
		this.appid = appid;
		this.appSecret = appSecret;
		this.redisUtil = redisUtil;
	}

	@PostConstruct
	private void init() {
		accessTokenUrl = url + "?grant_type=client_credentials&client_secret=" + appSecret + "&client_id=" + appid;
		redisKey = key + appid;
	}

	/**
	 * 请求华为accesstoken
	 * 
	 * @throws IOException
	 */
	public void requestAccessToken() throws IOException {
		Request request = OkHttpClientUtil.getInstance().buildPostRequestWithHeaders(accessTokenUrl, null);
		Response res = OkHttpClientUtil.getInstance().getClient().newCall(request).execute();
		if (res.isSuccessful()) {
			String body = res.body().string();
			String status = res.header("NSP_STATUS");
			logger.info("请求华为accesstoken返回:" + body + ",NSP_STATUS:" + status);
			int code = NumberUtils.toInt(status);
			if (code > 0) {
				throw new HuaWeiException("请求华为accesstoken发生系统级错误", code, "NSP_STATUS错误码");
			}
			HuaWeiAccessToken accessToken = FastJsonUtil.toObject(body, HuaWeiAccessToken.class);
			if (StringUtils.isNotEmpty(accessToken.getError())) {
				throw new HuaWeiException("请求华为accesstoken发生业务级错误", NumberUtils.toInt(accessToken.getError()), accessToken.getError_description());
			}
			String token = accessToken.getAccess_token();
			redisUtil.setString(redisKey, token);
		} else {
			throw new SimbaException("请求华为accesstoken发生异常");
		}
	}

	/**
	 * 获取accesstoken
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getAccessToken() throws IOException {
		String token = redisUtil.getString(redisKey);
		if (StringUtils.isEmpty(token)) {
			this.requestAccessToken();
			return this.getAccessToken();
		}
		return token;
	}
}
