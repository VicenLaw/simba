package com.simba.apppush.huawei.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.simba.apppush.huawei.exception.HuaWeiException;
import com.simba.apppush.huawei.util.HuaWeiAccessTokenUtil;

/**
 * 华为App推送工具类的Aspect Aop工具类，用来处理accesstoken过期无效
 * 
 * @author caozhejun
 *
 */
@Aspect
@Component
@ConditionalOnProperty(value = "huawei.access.token.job.enable", havingValue = "true")
public class HuaWeiAppPushUtilAspect {

	private static final Log logger = LogFactory.getLog(HuaWeiAppPushUtilAspect.class);

	private static final int code = 6;

	@Autowired
	private HuaWeiAccessTokenUtil huaWeiAccessTokenUtil;

	@Around(value = "execution(* com.simba.apppush.huawei.util.HuaWeiAppPushUtil.*(..))")
	public Object dealAccessToken(ProceedingJoinPoint pjd) throws Throwable {
		// 获取目标方法签名
		String signature = pjd.getSignature().toString();
		logger.info("调用华为App推送工具类:" + signature);
		long now = System.currentTimeMillis();
		Object result = null;
		try {
			result = pjd.proceed();
		} catch (HuaWeiException e) {
			if (e.getErrcode() == code) {
				logger.info("华为 accesstoken 过期,重新获取accesstoken");
				// 重新获取accesstoken
				huaWeiAccessTokenUtil.requestAccessToken();
				result = pjd.proceed();
			} else {
				throw e;
			}
		}
		long consumeTime = System.currentTimeMillis() - now;
		logger.info("执行" + signature + "消耗的时间为" + consumeTime + "毫秒");
		return result;
	}
}
