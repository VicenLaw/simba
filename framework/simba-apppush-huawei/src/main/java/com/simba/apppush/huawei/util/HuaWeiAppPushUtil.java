package com.simba.apppush.huawei.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.simba.apppush.huawei.exception.HuaWeiException;
import com.simba.apppush.huawei.model.Action;
import com.simba.apppush.huawei.model.Body;
import com.simba.apppush.huawei.model.Ext;
import com.simba.apppush.huawei.model.Hps;
import com.simba.apppush.huawei.model.HuaWeiAppPushPayload;
import com.simba.apppush.huawei.model.HuaWeiAppPushResponse;
import com.simba.apppush.huawei.model.Msg;
import com.simba.apppush.huawei.model.Param;
import com.simba.exception.SimbaException;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.model.constant.ConstantData;
import com.simba.okhttp.util.OkHttpClientUtil;

import okhttp3.Request;
import okhttp3.Response;

/**
 * 华为App推送工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class HuaWeiAppPushUtil {

	private static final Log logger = LogFactory.getLog(HuaWeiAppPushUtil.class);

	private static final String url = "https://api.push.hicloud.com/pushsend.do";

	@Autowired
	private HuaWeiAccessTokenUtil huaWeiAccessTokenUtil;

	@Value("${huawei.app.id:}")
	private String appid;

	@Value("${huawei.app.secret:}")
	private String appSecret;

	@Value("${huawei.app.title:}")
	private String title;

	@Value("${huawei.app.apk.package:}")
	private String apkPackage;

	private String version = "1";

	private boolean enabled = false;

	private String pushUrl;

	private String token;

	public void setToken(String token) {
		this.token = token;
	}

	public HuaWeiAppPushUtil() {

	}

	public HuaWeiAppPushUtil(String appid, String appSecret, String title, String apkPackage) {
		this.appid = appid;
		this.appSecret = appSecret;
		this.title = title;
		this.apkPackage = apkPackage;
	}

	public HuaWeiAppPushUtil(String appid, String appSecret, String title, String apkPackage, HuaWeiAccessTokenUtil huaWeiAccessTokenUtil) {
		this.appid = appid;
		this.appSecret = appSecret;
		this.title = title;
		this.apkPackage = apkPackage;
		this.huaWeiAccessTokenUtil = huaWeiAccessTokenUtil;
	}

	public boolean isEnabled() {
		return enabled;
	}

	@PostConstruct
	public void init() throws UnsupportedEncodingException {
		if (StringUtils.isNotEmpty(appid) && StringUtils.isNotEmpty(appSecret)) {
			enabled = true;
			Map<String, String> params = new HashMap<>(2);
			params.put("ver", version);
			params.put("appId", appid);
			String json = FastJsonUtil.toJson(params);
			String encode = URLEncoder.encode(json, ConstantData.DEFAULT_CHARSET);
			pushUrl = url + "?nsp_ctx=" + encode;
		}
	}

	/**
	 * 发送华为App推送
	 * 
	 * @param token
	 *            设备的token
	 * @param content
	 *            内容
	 * @throws IOException
	 */
	public void send(String token, String content) throws IOException {
		List<String> tokens = new ArrayList<>(1);
		tokens.add(token);
		this.send(tokens, title, content, StringUtils.EMPTY, StringUtils.EMPTY, 3, 3);
	}

	/**
	 * 发送华为App推送
	 * 
	 * @param token
	 *            设备的token
	 * @param title
	 *            标题
	 * @param content
	 *            内容
	 * @throws IOException
	 */
	public void send(String token, String title, String content) throws IOException {
		List<String> tokens = new ArrayList<>(1);
		tokens.add(token);
		this.send(tokens, title, content, StringUtils.EMPTY, StringUtils.EMPTY, 3, 3);
	}

	/**
	 * 发送华为App推送
	 * 
	 * @param tokens
	 *            设备的token列表
	 * @param title
	 *            标题
	 * @param content
	 *            内容
	 * @throws IOException
	 */
	public void send(List<String> tokens, String title, String content) throws IOException {
		this.send(tokens, title, content, StringUtils.EMPTY, StringUtils.EMPTY, 3, 3);
	}

	/**
	 * 发送华为App推送
	 * 
	 * @param tokens
	 *            设备的token列表
	 * @param content
	 *            内容
	 * @throws IOException
	 */
	public void send(List<String> tokens, String content) throws IOException {
		this.send(tokens, title, content, StringUtils.EMPTY, StringUtils.EMPTY, 3, 3);
	}

	/**
	 * 发送华为App推送
	 * 
	 * @param tokens
	 *            设备的token列表
	 * @param title
	 *            标题
	 * @param content
	 *            内容
	 * @param url
	 *            点击之后打开url地址
	 * @param intent
	 *            点击之后打开intent
	 * @param msgType
	 *            取值含义和说明： 1 透传异步消息 3 系统通知栏异步消息 注意：2和4以后为保留后续扩展使用
	 * @param actionType
	 *            类型 1 自定义行为：行为由参数intent定义 2 打开URL：URL地址由参数url定义 3
	 *            打开APP：默认值，打开App的首页
	 * @throws IOException
	 */
	public void send(List<String> tokens, String title, String content, String url, String intent, int msgType, int actionType) throws IOException {
		String accessToken = StringUtils.defaultString(token, huaWeiAccessTokenUtil.getAccessToken());
		String ts = System.currentTimeMillis() / 1000 + "";
		Map<String, String> params = new HashMap<>(6);
		params.put("access_token", accessToken);
		params.put("nsp_ts", ts);
		params.put("nsp_svc", "openpush.message.api.send");
		params.put("device_token_list", FastJsonUtil.toJson(tokens));
		// 封装消息体
		HuaWeiAppPushPayload payload = buildHuaWeiAppPushPayload(title, content, url, intent, msgType, actionType);
		params.put("payload", FastJsonUtil.toJson(payload));
		Request request = OkHttpClientUtil.getInstance().buildPostRequestWithHeaders(pushUrl, params, null);
		Response res = OkHttpClientUtil.getInstance().getClient().newCall(request).execute();
		if (res.isSuccessful()) {
			String responseBody = res.body().string();
			String status = res.header("NSP_STATUS");
			logger.info("发送华为App推送返回:" + responseBody + ",NSP_STATUS:" + status);
			int code = NumberUtils.toInt(status);
			if (code > 0) {
				throw new HuaWeiException("发送华为App推送发生系统级错误", code, "NSP_STATUS错误码");
			}
			HuaWeiAppPushResponse response = FastJsonUtil.toObject(responseBody, HuaWeiAppPushResponse.class);
			if (!"80000000".equals(response.getCode())) {
				throw new HuaWeiException("发送华为App推送发生业务级错误", NumberUtils.toInt(response.getCode()), response.getMsg());
			}
		} else {
			throw new SimbaException("发送华为App推送发生异常");
		}
	}

	private HuaWeiAppPushPayload buildHuaWeiAppPushPayload(String title, String content, String url, String intent, int msgType, int actionType) {
		HuaWeiAppPushPayload payload = new HuaWeiAppPushPayload();
		Hps hps = new Hps();
		payload.setHps(hps);
		Msg msg = new Msg();
		Ext ext = new Ext();
		hps.setMsg(msg);
		hps.setExt(ext);
		msg.setType(msgType);
		Action action = new Action();
		msg.setBody(new Body(title, content));
		msg.setAction(action);
		action.setType(actionType);
		Param param = new Param();
		action.setParam(param);
		param.setAppPkgName(apkPackage);
		param.setIntent(intent);
		param.setUrl(url);
		return payload;
	}

}
