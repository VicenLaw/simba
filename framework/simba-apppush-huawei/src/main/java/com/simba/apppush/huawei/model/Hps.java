package com.simba.apppush.huawei.model;

/**
 * 华为Push消息总结构体
 * 
 * @author caozhejun
 *
 */
public class Hps {

	/**
	 * Push消息定义。 包括： 消息类型 消息内容 消息动作
	 */
	private Msg msg;

	/**
	 * 扩展信息，含BI消息统计等
	 */
	private Ext ext;

	public Msg getMsg() {
		return msg;
	}

	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public Ext getExt() {
		return ext;
	}

	public void setExt(Ext ext) {
		this.ext = ext;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Hps [msg=");
		builder.append(msg);
		builder.append(", ext=");
		builder.append(ext);
		builder.append("]");
		return builder.toString();
	}

}
