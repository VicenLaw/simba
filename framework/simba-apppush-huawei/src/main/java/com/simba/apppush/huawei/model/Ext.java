package com.simba.apppush.huawei.model;

import java.util.List;

/**
 * 扩展信息，含BI消息统计等
 * 
 * @author caozhejun
 *
 */
public class Ext {

	/**
	 * 设置消息标签，如果带了这个标签，会在回执中推送给CP用于检测某种类型消息的到达率和状态。
	 * 注意：BigTag不能携带下面几个保留字符：逗号‘，’，竖线‘|’，长度不能超过100个字符
	 */
	private String biTag;

	/**
	 * 扩展样例：[{"season":"Spring"},{"weather":"raining"}] 说明：这个字段类型必须是JSON
	 * Array，里面是key-value的一组扩展信息
	 */
	private List<Object> customize;

	public String getBiTag() {
		return biTag;
	}

	public void setBiTag(String biTag) {
		this.biTag = biTag;
	}

	public List<Object> getCustomize() {
		return customize;
	}

	public void setCustomize(List<Object> customize) {
		this.customize = customize;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ext [biTag=");
		builder.append(biTag);
		builder.append(", customize=");
		builder.append(customize);
		builder.append("]");
		return builder.toString();
	}

}
