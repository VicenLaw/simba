package com.simba.apppush.huawei.model;

/**
 * 关于消息点击动作的参数
 * 
 * @author caozhejun
 *
 */
public class Param {

	/**
	 * Action的type为1的时候表示自定义行为。
	 * 开发者可以自定义Intent，用户收到通知栏消息后点击通知栏消息打开应用定义的这个Intent页面，该过程通过context.startActivity(intent)实现，具体代码如下：
	 * Intent intent = new Intent(Intent.ACTION_VIEW);
	 * intent.setData(Uri.parse("unitehnreader://nativepage/book/detail?bid=357735?closeback=1&startinner=0"));
	 * intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); String intentUri =
	 * intent.toUri(Intent.URI_INTENT_SCHEME); Log.d("intentUri", intentUri);
	 * 其中intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);是应用必须添加的选项，如果不添加该选项有可能会显示重复的消息，其他为应用自己的配置项。生成的intentUri字符串即为intent参数需要填写的值。
	 */
	private String intent;

	/**
	 * Action的type为2的时候表示打开URL地址
	 */
	private String url;

	/**
	 * 需要拉起的应用包名，必须和注册推送的包名一致
	 */
	private String appPkgName;

	public String getIntent() {
		return intent;
	}

	public void setIntent(String intent) {
		this.intent = intent;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAppPkgName() {
		return appPkgName;
	}

	public void setAppPkgName(String appPkgName) {
		this.appPkgName = appPkgName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Param [intent=");
		builder.append(intent);
		builder.append(", url=");
		builder.append(url);
		builder.append(", appPkgName=");
		builder.append(appPkgName);
		builder.append("]");
		return builder.toString();
	}

}
