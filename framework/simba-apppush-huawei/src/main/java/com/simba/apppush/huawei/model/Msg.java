package com.simba.apppush.huawei.model;

/**
 * Push消息定义。 包括： 消息类型 消息内容 消息动作
 * 
 * @author caozhejun
 *
 */
public class Msg {

	/**
	 * 取值含义和说明： 1 透传异步消息 3 系统通知栏异步消息 注意：2和4以后为保留后续扩展使用
	 */
	private int type = 3;

	/**
	 * 消息内容。 注意：对于透传类的消息可以是字符串，不必是JSON Object
	 */
	private Object body;

	/**
	 * 消息点击动作
	 */
	private Action action;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Msg [type=");
		builder.append(type);
		builder.append(", body=");
		builder.append(body);
		builder.append(", action=");
		builder.append(action);
		builder.append("]");
		return builder.toString();
	}

}
