package com.simba.apppush.huawei.model;

/**
 * 消息内容。 注意：对于透传类的消息可以是字符串，不必是JSON Object
 * 
 * @author caozhejun
 *
 */
public class Body {

	/**
	 * 消息内容体
	 */
	private String content;

	/**
	 * 消息标题
	 */
	private String title;

	public Body() {

	}

	public Body(String title, String content) {
		this.content = content;
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Body [content=");
		builder.append(content);
		builder.append(", title=");
		builder.append(title);
		builder.append("]");
		return builder.toString();
	}

}
