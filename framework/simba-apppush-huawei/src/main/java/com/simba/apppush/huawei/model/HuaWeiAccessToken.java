package com.simba.apppush.huawei.model;

/**
 * 华为accesstoken对象
 * 
 * @author caozhejun
 *
 */
public class HuaWeiAccessToken {

	/**
	 * 要获取的Access Token
	 */
	private String access_token;

	/**
	 * Access Token的有效期，以秒为单位
	 */
	private Integer expires_in;

	/**
	 * Access Token的访问范围，即用户实际授予的权限列表（用户在授权页面时，有可能会取消掉某些请求的权限）
	 */
	private String scope;

	/**
	 * 错误码。详细含义请参见下面“HTTP协议错误码”和“业务级错误码”
	 */
	private String error;

	/**
	 * 错误描述信息，用来帮助理解和解决发生的错误
	 */
	private String error_description;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError_description() {
		return error_description;
	}

	public void setError_description(String error_description) {
		this.error_description = error_description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HuaWeiAccessToken [access_token=");
		builder.append(access_token);
		builder.append(", expires_in=");
		builder.append(expires_in);
		builder.append(", scope=");
		builder.append(scope);
		builder.append(", error=");
		builder.append(error);
		builder.append(", error_description=");
		builder.append(error_description);
		builder.append("]");
		return builder.toString();
	}

}
