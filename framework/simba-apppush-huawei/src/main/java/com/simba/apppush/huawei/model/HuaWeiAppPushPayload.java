package com.simba.apppush.huawei.model;

/**
 * 描述投递消息的JSON结构体，描述PUSH消息的：类型、内容、显示、点击动作、报表统计和扩展信息具体参考下面的详细说明
 * 
 * @author caozhejun
 *
 */
public class HuaWeiAppPushPayload {

	/**
	 * 华为Push消息总结构体
	 */
	private Hps hps;

	public Hps getHps() {
		return hps;
	}

	public void setHps(Hps hps) {
		this.hps = hps;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HuaWeiAppPushPayload [hps=");
		builder.append(hps);
		builder.append("]");
		return builder.toString();
	}

}
