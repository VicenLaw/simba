package com.simba.apppush.huawei.jobs;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.simba.apppush.huawei.util.HuaWeiAccessTokenUtil;

/**
 * 定时获取accesstoken的定时器(每小时执行一次)
 * 
 * @author caozhejun
 *
 */
@Component
@ConditionalOnProperty(value = "huawei.access.token.job.enable", havingValue = "true")
public class AccessTokenJob {

	@Autowired
	private HuaWeiAccessTokenUtil huaWeiAccessTokenUtil;

	@Scheduled(fixedRate = 3600000, initialDelay = 5000)
	public void requestAccessToken() throws IOException {
		huaWeiAccessTokenUtil.requestAccessToken();
	}

}
