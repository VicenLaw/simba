package com.simba.apppush.huawei.model;

/**
 * 消息点击动作
 * 
 * @author caozhejun
 *
 */
public class Action {

	/**
	 * 1 自定义行为：行为由参数intent定义 2 打开URL：URL地址由参数url定义 3 打开APP：默认值，打开App的首页
	 * 注意：富媒体消息开放API不支持
	 */
	private int type = 3;

	/**
	 * 关于消息点击动作的参数
	 */
	private Param param;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Param getParam() {
		return param;
	}

	public void setParam(Param param) {
		this.param = param;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Action [type=");
		builder.append(type);
		builder.append(", param=");
		builder.append(param);
		builder.append("]");
		return builder.toString();
	}

}
