package com.simba.apppush.huawei.model;

/**
 * 华为App推送响应对象
 * 
 * @author caozhejun
 *
 */
public class HuaWeiAppPushResponse {

	/**
	 * 错误码
	 */
	private String code;

	/**
	 * 错误码描述
	 */
	private String msg;

	/**
	 * 请求标识，由PUSH服务器唯一分配
	 */
	private String requestId;

	/**
	 * 扩展信息(暂未使用)
	 */
	private String ext;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HuaWeiAppPushResponse [code=");
		builder.append(code);
		builder.append(", msg=");
		builder.append(msg);
		builder.append(", requestId=");
		builder.append(requestId);
		builder.append(", ext=");
		builder.append(ext);
		builder.append("]");
		return builder.toString();
	}

}
