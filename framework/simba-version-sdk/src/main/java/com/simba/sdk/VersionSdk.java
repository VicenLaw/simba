package com.simba.sdk;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simba.eureka.client.EurekaClientUtil;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.framework.util.json.JsonResult;

/**
 * 版本管理sdk
 * 
 * @author caozhejun
 *
 */
@Service
public class VersionSdk {

	private static final String getVersionUrl = "http://VERSIONUSER/api/app/getVersionInfo";

	private static final String saveBugFeedbackUrl = "http://VERSIONUSER/api/bugFeedback/save";

	private static final String getIosVersionUrl = "http://VERSIONUSER/api/ios/getVersionInfo";

	private static final String saveOpinionFeedbackUrl = "http://VERSIONUSER/api/opinionFeedback/save";

	private static final Log logger = LogFactory.getLog(VersionSdk.class);
	
	@Autowired
	private EurekaClientUtil client;

	/**
	 * 获取版本信息
	 * 
	 * @param typeId
	 *            类型id
	 * @return
	 */
	public JsonResult getVersionInfo(int typeId) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("typeId",typeId+"");
		logger.info("SDK发送请求:--"+getVersionUrl+"--"+typeId);
		String result = client.post(getVersionUrl, map);
		logger.info("api系统返回结果:" + result);
		return FastJsonUtil.toObject(result, JsonResult.class);
	}

	/**
	 * 获取IOS版本信息
	 * 
	 * @param typeId
	 *            类型id
	 * @return
	 */
	public JsonResult getIOSVersionInfo(int typeId) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("typeId",typeId+"");
		logger.info("SDK发送请求:--"+getIosVersionUrl+"--"+typeId);
		String result = client.post(getIosVersionUrl, map);
		logger.info("api系统返回结果:" + result);
		return FastJsonUtil.toObject(result, JsonResult.class);
	}

	/**
	 * 保存bug反馈
	 * 
	 * @param userId
	 *            反馈bug的用户id
	 * @param title
	 *            bug名称
	 * @param text
	 *            bug内容
	 * @param img
	 *            bug截图
	 * @return
	 */
	public JsonResult saveBugFeedback(Integer userId, String title, String text, String img) {
		Map<String, String> param = new HashMap<>();
		param.put("userId", userId + StringUtils.EMPTY);
		param.put("title", title);
		param.put("text", text);
		param.put("img", img);
		String res = client.post(saveBugFeedbackUrl, param);
		JsonResult result = FastJsonUtil.toObject(res, JsonResult.class);
		result.check("保存bug反馈发生异常");
		return result;
	}

	/**
	 * 保存反馈意见
	 * 
	 * @param userId
	 * @param title
	 * @param text
	 * @return
	 */
	public JsonResult saveOpinionFeedback(Integer userId, String title, String text) {
		Map<String, String> param = new HashMap<>();
		param.put("userId", userId + StringUtils.EMPTY);
		param.put("title", title);
		param.put("text", text);
		String res = client.post(saveOpinionFeedbackUrl, param);
		JsonResult result = FastJsonUtil.toObject(res, JsonResult.class);
		result.check("保存反馈意见发生异常");
		return result;
	}
}
