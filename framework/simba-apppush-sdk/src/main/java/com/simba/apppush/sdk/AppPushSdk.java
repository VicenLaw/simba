package com.simba.apppush.sdk;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simba.eureka.client.EurekaClientUtil;
import com.simba.framework.util.json.FastJsonUtil;
import com.simba.framework.util.json.JsonResult;

/**
 * app推送sdk工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class AppPushSdk {

	private static final String sendByIdUrl = "http://APPPUSHAPI/server/api/appPush/sendById";

	private static final String sendByCodeUrl = "http://APPPUSHAPI/server/api/appPush/sendByCode";

	private static final String flagByIdUrl = "http://APPPUSHAPI/server/api/appPush/flagById";

	private static final String flagByCodeUrl = "http://APPPUSHAPI/server/api/appPush/flagByCode";

	@Autowired
	private EurekaClientUtil client;

	/**
	 * 推送App消息给用户
	 * 
	 * @param appPushId
	 *            应用id
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送内容
	 * @return
	 */
	public JsonResult send(long appPushId, String userId, String content) {
		Map<String, String> params = new HashMap<>();
		params.put("appPushId", appPushId + "");
		params.put("userId", userId);
		params.put("content", content);
		String response = client.post(sendByIdUrl, params);
		return FastJsonUtil.toObject(response, JsonResult.class);
	}

	/**
	 * 推送App消息给用户
	 * 
	 * @param appPushCode
	 *            应用编码
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送内容
	 * @return
	 */
	@RequestMapping("/sendByCode")
	public JsonResult send(String appPushCode, String userId, String content) {
		Map<String, String> params = new HashMap<>();
		params.put("appPushCode", appPushCode);
		params.put("userId", userId);
		params.put("content", content);
		String response = client.post(sendByCodeUrl, params);
		return FastJsonUtil.toObject(response, JsonResult.class);
	}

	/**
	 * 标记用户手机类型和token
	 * 
	 * @param appPushId
	 *            应用id
	 * 
	 * @param token
	 *            用户手机推送token
	 * @param mobileType
	 *            用户手机类型
	 * @param userId
	 *            用户id
	 * @return
	 */
	@PostMapping("/flagById")
	public JsonResult flag(long appPushId, String token, String mobileType, String userId) {
		Map<String, String> params = new HashMap<>();
		params.put("userId", userId);
		params.put("token", token);
		params.put("mobileType", mobileType);
		params.put("appPushId", appPushId + "");
		String response = client.post(flagByIdUrl, params);
		return FastJsonUtil.toObject(response, JsonResult.class);
	}

	/**
	 * 标记用户手机类型和token
	 * 
	 * @param appPushCode
	 *            应用编码
	 * 
	 * @param token
	 *            用户手机推送token
	 * @param mobileType
	 *            用户手机类型
	 * @param userId
	 *            用户id
	 * @return
	 */
	@PostMapping("/flagByCode")
	public JsonResult flag(String appPushCode, String token, String mobileType, String userId) {
		Map<String, String> params = new HashMap<>();
		params.put("userId", userId);
		params.put("token", token);
		params.put("mobileType", mobileType);
		params.put("appPushCode", appPushCode);
		String response = client.post(flagByCodeUrl, params);
		return FastJsonUtil.toObject(response, JsonResult.class);
	}

}
