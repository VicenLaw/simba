package com.simba.apppush.controller.api;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simba.apppush.sdk.AppPushSdk;
import com.simba.framework.util.json.JsonResult;

/**
 * 给前端调用的App推送服务sdk api Controller
 * 
 * @author caozhejun
 *
 */
@RestController
@RequestMapping("/api/app/push/service")
public class AppPushServiceApiController {

	@Autowired
	private AppPushSdk sdk;

	/**
	 * 标记用户token和手机类型
	 * 
	 * @param appPushCode
	 *            应用编码
	 * @param token
	 *            用户token
	 * @param mobileType
	 *            用户手机类型
	 * @param session
	 * @return
	 */
	@RequestMapping("/flag")
	public JsonResult flag(String appPushCode, String token, String mobileType, HttpSession session) {
		String userId = (String) session.getAttribute("userId");
		return sdk.flag(appPushCode, token, mobileType, userId);
	}

}
