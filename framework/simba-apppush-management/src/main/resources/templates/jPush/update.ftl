<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title>系统首页</title>
		<#include "../adminlte.ftl"/>
		<script type="text/javascript" src="${base}/js/jPush/jPush.js"></script>
	</head>

	<body>
		<div>
			<!-- Content Wrapper. Contains page content -->
			<div class="">
				<section class="content">
					<div class="row">

						<!-- /.col -->
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">修改极光推送</h3>
								</div>
								<form method="post" role="form" onsubmit="return JPush.checkForm();" id="form" action="${base}/jPush/update">
									<input type="hidden" id="id" name="id" value="${jPush.id}" />
									<div class="box-body">
										<div class="form-group">
											<label for="appPushId">应用推送id</label>
											<input type="text" class="form-control" id="appPushId" name="appPushId" value="${jPush.appPushId}" placeholder="请输入应用推送id">
										</div>
										<div class="form-group">
											<label for="appKey">极光推送Key</label>
											<input type="text" class="form-control" id="appKey" name="appKey" value="${jPush.appKey}" placeholder="请输入极光推送Key">
										</div>
										<div class="form-group">
											<label for="appSecret">极光推送Secret</label>
											<input type="text" class="form-control" id="appSecret" name="appSecret" value="${jPush.appSecret}" placeholder="请输入极光推送Secret">
										</div>
									</div>
									<!-- /.box-body -->

									<div class="box-footer">
										<button type="submit" class="btn btn-success">提交</button>
										<button type="button" class="btn" onclick="JPush.toList();">取消</button>
									</div>
								</form>

							</div>
						</div>
						<!-- /. box -->
					</div>
					<!-- /.col -->
			</div>
			<!-- /.row -->
			</section>
			<!-- /.content -->

		</div>
		<!-- /.content-wrapper -->
		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		</div>
		<!-- ./wrapper -->

	</body>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>

</html>