<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title>系统首页</title>
		<#include "../adminlte.ftl"/>
		<script type="text/javascript" src="${base}/js/appPush/appPush.js"></script>
	</head>

	<body>
		<div>
			<!-- Content Wrapper. Contains page content -->
			<div class="">
				<section class="content">
					<div class="row">

						<!-- /.col -->
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h4 class="box-title">修改App推送</h4>
								</div>
								<form method="post" role="form" onsubmit="return AppPush.checkForm();" id="form" action="${base}/appPush/update">
									<input type="hidden" id="appPush.id" name="appPush.id" value="${vo.appPush.id}" />
									<input type="hidden" id="huaweiPush.id" name="huaweiPush.id" value="${vo.huaweiPush.id}" />
									<input type="hidden" id="huaweiPush.appPushId" name="huaweiPush.appPushId" value="${vo.huaweiPush.appPushId}" />
									<input type="hidden" id="jPush.id" name="jPush.id" value="${vo.jPush.id}" />
									<input type="hidden" id="jPush.appPushId" name="jPush.appPushId" value="${vo.jPush.appPushId}" />
									<input type="hidden" id="oppoPush.id" name="oppoPush.id" value="${vo.oppoPush.id}" />
									<input type="hidden" id="oppoPush.appPushId" name="oppoPush.appPushId" value="${vo.oppoPush.appPushId}" />
									<input type="hidden" id="vivoPush.id" name="vivoPush.id" value="${vo.vivoPush.id}" />
									<input type="hidden" id="vivoPush.appPushId" name="vivoPush.appPushId" value="${vo.vivoPush.appPushId}" />
									<input type="hidden" id="xiaomiPush.id" name="xiaomiPush.id" value="${vo.xiaomiPush.id}" />
									<input type="hidden" id="xiaomiPush.appPushId" name="xiaomiPush.appPushId" value="${vo.xiaomiPush.appPushId}" />
									<div class="box-body">
										<div class="form-group">
											<label for="appPush.name">应用名称</label>
											<input type="text" class="form-control" id="appPush.name" name="appPush.name" placeholder="请输入应用名称" value="${vo.appPush.name}">
										</div>
										<div class="form-group">
											<label for="appPush.code">应用编号</label>
											<input type="text" class="form-control" id="appPush.code" name="appPush.code" placeholder="请输入应用编号" value="${vo.appPush.code}">
										</div>
										<h4>极光推送</h4></br>
										<div class="form-group">
											<label for="jPush.appKey">Key</label>
											<input type="text" class="form-control" id="jPush.appKey" name="jPush.appKey" placeholder="请输入Key" value="${vo.jPush.appKey}">
										</div>
										<div class="form-group">
											<label for="jPush.appSecret">Secret</label>
											<input type="text" class="form-control" id="jPush.appSecret" name="jPush.appSecret" placeholder="请输入Secret" value="${vo.jPush.appSecret}">
										</div>
										<div class="form-group">
											<label for="jPush.appProduct">生产环境</label>
											<select class="form-control" id="jPush.appProduct" name="jPush.appProduct">
												<option value="0" <#if vo.jPush.appProduct ?? &&  vo.jPush.appProduct == 0> selected </#if>   >否</option>
												<option value="1"    <#if vo.jPush.appProduct ?? &&  vo.jPush.appProduct == 1> selected </#if>  >是</option>
											</select>
										</div>
										<h4>华为推送</h4></br>
										<div class="form-group">
											<label for="huaweiPush.appId">AppID</label>
											<input type="text" class="form-control" id="huaweiPush.appId" name="huaweiPush.appId" placeholder="请输入AppID" value="${vo.huaweiPush.appId}">
										</div>
										<div class="form-group">
											<label for="huaweiPush.appSecret">应用秘钥</label>
											<input type="text" class="form-control" id="huaweiPush.appSecret" name="huaweiPush.appSecret" placeholder="请输入应用秘钥" value="${vo.huaweiPush.appSecret}">
										</div>
										<div class="form-group">
											<label for="huaweiPush.title">标题</label>
											<input type="text" class="form-control" id="huaweiPush.title" name="huaweiPush.title" placeholder="请输入标题" value="${vo.huaweiPush.title}">
										</div>
										<div class="form-group">
											<label for="huaweiPush.appPackage">包名</label>
											<input type="text" class="form-control" id="huaweiPush.appPackage" name="huaweiPush.appPackage" placeholder="请输入包名" value="${vo.huaweiPush.appPackage}">
										</div>
										<h4>小米推送</h4></br>
										<div class="form-group">
											<label for="xiaomiPush.appSecret">应用秘钥</label>
											<input type="text" class="form-control" id="xiaomiPush.appSecret" name="xiaomiPush.appSecret" placeholder="请输入应用秘钥" value="${vo.xiaomiPush.appSecret}">
										</div>
										<div class="form-group">
											<label for="xiaomiPush.title">标题</label>
											<input type="text" class="form-control" id="xiaomiPush.title" name="xiaomiPush.title" placeholder="请输入标题" value="${vo.xiaomiPush.title}">
										</div>
										<div class="form-group">
											<label for="xiaomiPush.appPackage">包名</label>
											<input type="text" class="form-control" id="xiaomiPush.appPackage" name="xiaomiPush.appPackage" placeholder="请输入包名" value="${vo.xiaomiPush.appPackage}">
										</div>
										<h4>VIVO推送</h4></br>
										<div class="form-group">
											<label for="vivoPush.appId">AppID</label>
											<input type="text" class="form-control" id="vivoPush.appId" name="vivoPush.appId" placeholder="请输入AppID" value="${vo.vivoPush.appId}">
										</div>
										<div class="form-group">
											<label for="vivoPush.appKey">Key</label>
											<input type="text" class="form-control" id="vivoPush.appKey" name="vivoPush.appKey" placeholder="请输入Key" value="${vo.vivoPush.appKey}">
										</div>
										<div class="form-group">
											<label for="vivoPush.appSecret">应用秘钥</label>
											<input type="text" class="form-control" id="vivoPush.appSecret" name="vivoPush.appSecret" placeholder="请输入应用秘钥" value="${vo.vivoPush.appSecret}">
										</div>
										<div class="form-group">
											<label for="vivoPush.title">标题</label>
											<input type="text" class="form-control" id="vivoPush.title" name="vivoPush.title" placeholder="请输入标题" value="${vo.vivoPush.title}">
										</div>
										<h4>OPPO推送</h4></br>
										<div class="form-group">
											<label for="oppoPush.appKey">Key</label>
											<input type="text" class="form-control" id="oppoPush.appKey" name="oppoPush.appKey" placeholder="请输入Key" value="${vo.oppoPush.appKey}">
										</div>
										<div class="form-group">
											<label for="oppoPush.appSecret">应用秘钥</label>
											<input type="text" class="form-control" id="oppoPush.appSecret" name="oppoPush.appSecret" placeholder="请输入应用秘钥" value="${vo.oppoPush.appSecret}">
										</div>
										<div class="form-group">
											<label for="oppoPush.title">标题</label>
											<input type="text" class="form-control" id="oppoPush.title" name="oppoPush.title" placeholder="请输入标题" value="${vo.oppoPush.title}">
										</div>
										<div class="form-group">
											<label for="oppoPush.subTitle">子标题</label>
											<input type="text" class="form-control" id="oppoPush.subTitle" name="oppoPush.subTitle" placeholder="请输入子标题" value="${vo.oppoPush.subTitle}">
										</div>
									</div>
									<!-- /.box-body -->

									<div class="box-footer">
										<button type="submit" class="btn btn-success">提交</button>
										<button type="button" class="btn" onclick="AppPush.toList();">取消</button>
									</div>
								</form>

							</div>
						</div>
						<!-- /. box -->
					</div>
					<!-- /.col -->
			</div>
			<!-- /.row -->
			</section>
			<!-- /.content -->

		</div>
		<!-- /.content-wrapper -->
		<!-- Add the sidebar's background. This div must be placed
immediately after the control sidebar -->
		</div>
		<!-- ./wrapper -->

	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</html>