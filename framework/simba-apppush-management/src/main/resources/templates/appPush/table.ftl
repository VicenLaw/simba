<#list list as vo>
	<tr>
		<td><input type="checkbox" name="appPush" value="${vo.appPush.id}"></td>
		<td>${vo.appPush.name}</td>
		<td>${vo.appPush.code}</td>
		<td>
			极光：[Key:${vo.jPush.appKey},Secret:${vo.jPush.appSecret},生产环境 : <#if vo.jPush.appProduct??>  <#if vo.jPush.appProduct == 0 >否</#if> <#if vo.jPush.appProduct == 1 >是</#if> </#if>]</br>
			华为：[AppID:${vo.huaweiPush.appId},应用秘钥:${vo.huaweiPush.appSecret},标题:${vo.huaweiPush.title},包名:${vo.huaweiPush.appPackage}]</br>
			小米：[应用秘钥:${vo.xiaomiPush.appSecret},标题:${vo.xiaomiPush.title},包名:${vo.xiaomiPush.appPackage}]</br>
			VIVO：[AppID:${vo.vivoPush.appId},Key:${vo.vivoPush.appKey},应用秘钥:${vo.vivoPush.appSecret},标题:${vo.vivoPush.title}]</br>
			OPPO：[Key:${vo.oppoPush.appKey},应用秘钥:${vo.oppoPush.appSecret},标题:${vo.oppoPush.title},子标题:${vo.oppoPush.subTitle}]</br>
		</td>
		<td>${vo.appPush.createTime}</td>
		<td>
			<button type="button" class="btn btn-default btn-sm" onclick="AppPush.toUpdate(${vo.appPush.id});"><i class="fa fa-pencil-square-o"></i>修改</button>
			<button type="button" class="btn btn-default btn-sm" onclick="AppPush.deleteAppPush(${vo.appPush.id});"><i class="fa fa-remove"></i>删除</button>
		</td>
	</tr>
</#list>