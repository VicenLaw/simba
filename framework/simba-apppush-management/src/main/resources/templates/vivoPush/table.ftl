<#list list as vivoPush>
	<tr>
		<td><input type="checkbox" name="vivoPush" value="${vivoPush.id}"></td>
		<td>${vivoPush.appPushId}</td>
		<td>${vivoPush.appId}</td>
		<td>${vivoPush.appKey}</td>
		<td>${vivoPush.appSecret}</td>
		<td>${vivoPush.title}</td>
		<td>
			<button type="button" class="btn btn-default btn-sm" onclick="VivoPush.toUpdate(${vivoPush.id});"><i class="fa fa-pencil-square-o"></i>修改</button>
			<button type="button" class="btn btn-default btn-sm" onclick="VivoPush.deleteVivoPush(${vivoPush.id});"><i class="fa fa-remove"></i>删除</button>
		</td>
	</tr>
</#list>