<#list list as oppoPush>
	<tr>
		<td><input type="checkbox" name="oppoPush" value="${oppoPush.id}"></td>
		<td>${oppoPush.appPushId}</td>
		<td>${oppoPush.appKey}</td>
		<td>${oppoPush.appSecret}</td>
		<td>${oppoPush.title}</td>
		<td>${oppoPush.subTitle}</td>
		<td>
			<button type="button" class="btn btn-default btn-sm" onclick="OppoPush.toUpdate(${oppoPush.id});"><i class="fa fa-pencil-square-o"></i>修改</button>
			<button type="button" class="btn btn-default btn-sm" onclick="OppoPush.deleteOppoPush(${oppoPush.id});"><i class="fa fa-remove"></i>删除</button>
		</td>
	</tr>
</#list>