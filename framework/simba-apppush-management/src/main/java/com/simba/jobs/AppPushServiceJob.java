package com.simba.jobs;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.simba.apppush.huawei.util.HuaWeiAccessTokenUtil;
import com.simba.apppush.vivo.util.VivoAccessTokenUtil;
import com.simba.util.AppPushServiceStore;

/**
 * App推送服务定时器
 * 
 * @author caozhejun
 *
 */
@Component
public class AppPushServiceJob {

	private static final Log logger = LogFactory.getLog(AppPushServiceJob.class);

	@Scheduled(fixedRate = 3600000, initialDelay = 10000)
	public void requestAccessToken() throws IOException {
		AppPushServiceStore.getHuaweiJobs().forEach((HuaWeiAccessTokenUtil util) -> {
			try {
				util.requestAccessToken();
				logger.info("获取华为App推送的token成功");
			} catch (IOException e) {
				logger.error("获取华为App推送的token发生异常", e);
			}
		});
		AppPushServiceStore.getVivoJobs().forEach((VivoAccessTokenUtil util) -> {
			try {
				util.requestAccessToken();
				logger.info("获取Vivo App推送的token成功");
			} catch (IOException e) {
				logger.error("获取Vivo App推送的token发生异常", e);
			}
		});
	}
}
