package com.simba.distributed;

import java.io.Serializable;

/**
 * App推送配置数据在集群中传递的对象
 * 
 * @author caozj
 *
 */
public class AppPushServiceClusterData implements Serializable {

	private static final long serialVersionUID = 2687616734474020143L;

	/**
	 * 执行的方法,add | remove | update
	 */
	private String method;

	/**
	 * App推送配置主键id
	 */
	private long id;

	public AppPushServiceClusterData() {

	}

	public AppPushServiceClusterData(String method, long id) {
		this.method = method;
		this.id = id;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppPushServiceClusterData [method=");
		builder.append(method);
		builder.append(", id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
