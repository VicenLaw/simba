package com.simba.distributed;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.framework.util.distributed.ClusterExecute;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.service.AppPushService;

/**
 * 注册表集群传递之后 要执行的方法
 * 
 * @author caozj
 *
 */
@Component
public class AppPushServiceClusterExecute implements ClusterExecute {

	private static final Log logger = LogFactory.getLog(AppPushServiceClusterExecute.class);

	@Autowired
	private AppPushService appPushService;

	@Override
	public void execute(Object data) {
		if (!(data instanceof AppPushServiceClusterData)) {
			throw new RuntimeException("类型错误：" + data.getClass().getCanonicalName());
		}
		AppPushServiceClusterData clustData = (AppPushServiceClusterData) data;
		if (clustData.getMethod().equals("add") || clustData.getMethod().equals("update")) {
			try {
				appPushService.initAppPush(clustData.getId());
			} catch (Exception e) {
				logger.error("初始化App推送服务发生异常", e);
			}
		} else if (clustData.getMethod().equals("remove")) {
			List<AppPushServiceInterface> impls = ApplicationContextUtil.getBeansOfType(AppPushServiceInterface.class);
			impls.forEach((AppPushServiceInterface impl) -> {
				impl.delete(clustData.getId());
			});
		}
	}

}
