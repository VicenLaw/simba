package com.simba.impls;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.huawei.util.HuaWeiAccessTokenUtil;
import com.simba.apppush.huawei.util.HuaWeiAppPushUtil;
import com.simba.cache.Redis;
import com.simba.dao.HuaweiPushDao;
import com.simba.exception.SimbaException;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.model.HuaweiPush;
import com.simba.util.AppPushServiceStore;
import com.simba.util.UserAppPushServiceUtil;

/**
 * 华为推送服务实现类
 * 
 * @author caozhejun
 *
 */
@Component
public class HuaweiAppPushServiceImpl implements AppPushServiceInterface {

	private static final Log logger = LogFactory.getLog(HuaweiAppPushServiceImpl.class);

	@Autowired
	private HuaweiPushDao huaweiPushDao;

	@Autowired
	private UserAppPushServiceUtil userAppPushServiceUtil;

	@Override
	public String getMobileType() {
		return "huawei";
	}

	@Override
	public void send(long appPushId, String userId, String content) {
		HuaWeiAppPushUtil huaWeiAppPushUtil = AppPushServiceStore.getHuawei(appPushId);
		if (huaWeiAppPushUtil == null) {
			throw new SimbaException(appPushId + "没有实现华为发送App推送");
		}
		String token = userAppPushServiceUtil.getUserToken(appPushId, userId);
		try {
			huaWeiAppPushUtil.send(token, content);
		} catch (IOException e) {
			logger.error("发送华为App推送发生异常", e);
		}
	}

	@Override
	public boolean enable(long appPushId) {
		HuaWeiAppPushUtil huaWeiAppPushUtil = AppPushServiceStore.getHuawei(appPushId);
		return huaWeiAppPushUtil != null && huaWeiAppPushUtil.isEnabled();
	}

	@Override
	public void init(long appPushId) {
		HuaweiPush huaweiPush = huaweiPushDao.getBy("appPushId", appPushId);
		Redis redisUtil = ApplicationContextUtil.getBean(Redis.class);
		HuaWeiAccessTokenUtil huaWeiAccessTokenUtil = new HuaWeiAccessTokenUtil(huaweiPush.getAppId(), huaweiPush.getAppSecret(), redisUtil);
		HuaWeiAppPushUtil huaWeiAppPushUtil = new HuaWeiAppPushUtil(huaweiPush.getAppId(), huaweiPush.getAppSecret(), huaweiPush.getTitle(), huaweiPush.getAppPackage(), huaWeiAccessTokenUtil);
		if (huaWeiAppPushUtil.isEnabled()) {
			AppPushServiceStore.addHuawei(appPushId, huaWeiAppPushUtil);
			AppPushServiceStore.addHuaweiJob(appPushId, huaWeiAccessTokenUtil);
			logger.info("初始化华为App推送成功[" + appPushId + "]");
		} else {
			this.delete(appPushId);
		}
	}

	@Override
	public void delete(long appPushId) {
		AppPushServiceStore.removeHuawei(appPushId);
		AppPushServiceStore.removeHuaweiJob(appPushId);
		logger.info("删除华为App推送成功[" + appPushId + "]");
	}

}
