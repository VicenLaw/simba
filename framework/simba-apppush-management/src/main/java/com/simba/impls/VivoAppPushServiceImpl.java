package com.simba.impls;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.vivo.util.VivoAccessTokenUtil;
import com.simba.apppush.vivo.util.VivoAppPushUtil;
import com.simba.dao.VivoPushDao;
import com.simba.exception.SimbaException;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.model.VivoPush;
import com.simba.util.AppPushServiceStore;

/**
 * VIVO推送服务实现类
 * 
 * @author caozhejun
 *
 */
@Component
public class VivoAppPushServiceImpl implements AppPushServiceInterface {

	private static final Log logger = LogFactory.getLog(VivoAppPushServiceImpl.class);

	@Autowired
	private VivoPushDao vivoPushDao;

	@Override
	public String getMobileType() {
		return "vivo";
	}

	@Override
	public void send(long appPushId, String userId, String content) {
		VivoAppPushUtil vivoAppPushUtil = AppPushServiceStore.getVivoAppPushUtil(appPushId);
		if (vivoAppPushUtil == null) {
			throw new SimbaException(appPushId + "没有实现Vivo发送App推送");
		}
		try {
			vivoAppPushUtil.send(userId, content);
		} catch (IOException e) {
			logger.error("发送Vivo手机App推送发生异常", e);
		}
	}

	@Override
	public boolean enable(long appPushId) {
		VivoAppPushUtil vivoAppPushUtil = AppPushServiceStore.getVivoAppPushUtil(appPushId);
		return vivoAppPushUtil != null;
	}

	@Override
	public void init(long appPushId) {
		VivoPush vivoPush = vivoPushDao.getBy("appPushId", appPushId);
		VivoAccessTokenUtil vivoAccessTokenUtil = new VivoAccessTokenUtil(vivoPush.getAppId(), vivoPush.getAppKey(), vivoPush.getAppSecret());
		vivoAccessTokenUtil.init();
		if (vivoAccessTokenUtil.isEnable()) {
			VivoAppPushUtil vivoAppPushUtil = new VivoAppPushUtil(vivoAccessTokenUtil, vivoPush.getTitle());
			AppPushServiceStore.addVivo(appPushId, vivoAppPushUtil);
			AppPushServiceStore.addVivoJob(appPushId, vivoAccessTokenUtil);
			logger.info("初始化Vivo App推送成功[" + appPushId + "]");
		} else {
			this.delete(appPushId);
		}
	}

	@Override
	public void delete(long appPushId) {
		AppPushServiceStore.removeVivo(appPushId);
		AppPushServiceStore.removeVivoJob(appPushId);
		logger.info("删除Vivo App推送成功[" + appPushId + "]");
	}

}
