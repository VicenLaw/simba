package com.simba.impls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.dao.XiaomiPushDao;
import com.simba.exception.SimbaException;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.model.XiaomiPush;
import com.simba.util.AppPushServiceStore;
import com.simba.xiaomi.util.XiaoMiAppPushUtil;

/**
 * 小米推送服务实现类
 * 
 * @author caozhejun
 *
 */
@Component
public class XiaomiAppPushServiceImpl implements AppPushServiceInterface {

	private static final Log logger = LogFactory.getLog(XiaomiAppPushServiceImpl.class);

	@Autowired
	private XiaomiPushDao xiaomiPushDao;

	@Override
	public String getMobileType() {
		return "xiaomi";
	}

	@Override
	public void send(long appPushId, String userId, String content) {
		XiaoMiAppPushUtil xiaoMiAppPushUtil = AppPushServiceStore.getXiaoMiAppPushUtil(appPushId);
		if (xiaoMiAppPushUtil == null) {
			throw new SimbaException(appPushId + "没有实现小米发送App推送");
		}
		try {
			xiaoMiAppPushUtil.send(userId, content);
		} catch (Exception e) {
			logger.error("发送小米App推送发生异常", e);
		}
	}

	@Override
	public boolean enable(long appPushId) {
		XiaoMiAppPushUtil xiaoMiAppPushUtil = AppPushServiceStore.getXiaoMiAppPushUtil(appPushId);
		return xiaoMiAppPushUtil != null && xiaoMiAppPushUtil.isEnable();
	}

	@Override
	public void init(long appPushId) {
		XiaomiPush xiaomiPush = xiaomiPushDao.getBy("appPushId", appPushId);
		XiaoMiAppPushUtil xiaoMiAppPushUtil = new XiaoMiAppPushUtil(xiaomiPush.getAppSecret(), xiaomiPush.getTitle(), xiaomiPush.getAppPackage());
		xiaoMiAppPushUtil.init();
		if (xiaoMiAppPushUtil.isEnable()) {
			AppPushServiceStore.addXiaomi(appPushId, xiaoMiAppPushUtil);
			logger.info("初始化小米App推送成功[" + appPushId + "]");
		} else {
			this.delete(appPushId);
		}
	}

	@Override
	public void delete(long appPushId) {
		AppPushServiceStore.removeXiaomi(appPushId);
		logger.info("删除小米App推送成功[" + appPushId + "]");
	}

}
