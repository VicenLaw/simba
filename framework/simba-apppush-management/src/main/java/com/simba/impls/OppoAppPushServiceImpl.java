package com.simba.impls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.oppo.util.OppoAppPushUtil;
import com.simba.dao.OppoPushDao;
import com.simba.exception.SimbaException;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.model.OppoPush;
import com.simba.util.AppPushServiceStore;
import com.simba.util.UserAppPushServiceUtil;

/**
 * OPPO推送服务实现类
 * 
 * @author caozhejun
 *
 */
@Component
public class OppoAppPushServiceImpl implements AppPushServiceInterface {

	private static final Log logger = LogFactory.getLog(OppoAppPushServiceImpl.class);

	@Autowired
	private OppoPushDao oppoPushDao;

	@Autowired
	private UserAppPushServiceUtil userAppPushServiceUtil;

	@Override
	public String getMobileType() {
		return "oppo";
	}

	@Override
	public void send(long appPushId, String userId, String content) {
		OppoAppPushUtil oppoAppPushUtil = AppPushServiceStore.getOppoAppPushUtil(appPushId);
		if (oppoAppPushUtil == null) {
			throw new SimbaException(appPushId + "没有实现oppo发送App推送");
		}
		String token = userAppPushServiceUtil.getUserToken(appPushId, userId);
		try {
			oppoAppPushUtil.send(token, content);
		} catch (Exception e) {
			logger.error("发送oppo手机App推送发生异常", e);
		}
	}

	@Override
	public boolean enable(long appPushId) {
		OppoAppPushUtil oppoAppPushUtil = AppPushServiceStore.getOppoAppPushUtil(appPushId);
		return oppoAppPushUtil != null && oppoAppPushUtil.isEnable();
	}

	@Override
	public void init(long appPushId) {
		OppoPush oppoPush = oppoPushDao.getBy("appPushId", appPushId);
		OppoAppPushUtil oppoAppPushUtil = new OppoAppPushUtil(oppoPush.getAppKey(), oppoPush.getAppSecret(), oppoPush.getTitle(), oppoPush.getSubTitle());
		try {
			oppoAppPushUtil.init();
		} catch (Exception e) {
			logger.error("初始化oppo App推送发生异常", e);
		}
		if (oppoAppPushUtil.isEnable()) {
			AppPushServiceStore.addOppo(appPushId, oppoAppPushUtil);
			logger.info("初始化Oppo App推送成功[" + appPushId + "]");
		} else {
			this.delete(appPushId);
		}
	}

	@Override
	public void delete(long appPushId) {
		AppPushServiceStore.removeOppo(appPushId);
		logger.info("删除Oppo App推送成功[" + appPushId + "]");
	}

}
