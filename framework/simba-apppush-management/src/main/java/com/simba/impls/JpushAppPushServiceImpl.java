package com.simba.impls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.apppush.util.AppPushConstants;
import com.simba.dao.JPushDao;
import com.simba.exception.SimbaException;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.jpush.app.util.JpushAppUtil;
import com.simba.model.JPush;
import com.simba.util.AppPushServiceStore;

/**
 * 极光推送服务实现类
 * 
 * @author caozhejun
 *
 */
@Component
public class JpushAppPushServiceImpl implements AppPushServiceInterface {

	private static final Log logger = LogFactory.getLog(JpushAppPushServiceImpl.class);

	@Autowired
	private JPushDao jPushDao;

	@Override
	public String getMobileType() {
		return AppPushConstants.defaultMobileType;
	}

	@Override
	public void send(long appPushId, String userId, String content) {
		JpushAppUtil jpushAppUtil = AppPushServiceStore.getJpushAppUtil(appPushId);
		if (jpushAppUtil == null) {
			throw new SimbaException(appPushId + "没有实现极光推送");
		}
		try {
			jpushAppUtil.sendNotification(userId, content);
			logger.info("使用极光推送完成[appPushId:" + appPushId + "][userId:" + userId + "][content:" + content + "]");
		} catch (Exception e) {
			logger.error("发送极光App推送发生异常", e);
		}
	}

	@Override
	public boolean enable(long appPushId) {
		JpushAppUtil jpushAppUtil = AppPushServiceStore.getJpushAppUtil(appPushId);
		return jpushAppUtil != null && jpushAppUtil.isEnabled();
	}

	@Override
	public void init(long appPushId) {
		JPush jPush = jPushDao.getBy("appPushId", appPushId);
		JpushAppUtil jpushAppUtil = new JpushAppUtil(jPush.getAppKey(), jPush.getAppSecret(), jPush.getAppProduct() != 0);
		jpushAppUtil.init();
		if (jpushAppUtil.isEnabled()) {
			AppPushServiceStore.addJpush(appPushId, jpushAppUtil);
			logger.info("初始化极光App推送成功[" + appPushId + "]");
		} else {
			this.delete(appPushId);
		}
	}

	@Override
	public void delete(long appPushId) {
		AppPushServiceStore.removeJpush(appPushId);
		logger.info("删除极光App推送成功[" + appPushId + "]");
	}

}
