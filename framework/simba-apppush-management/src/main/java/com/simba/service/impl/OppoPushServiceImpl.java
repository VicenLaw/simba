package com.simba.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.simba.dao.OppoPushDao;
import com.simba.framework.util.jdbc.Pager;
import com.simba.model.OppoPush;
import com.simba.service.OppoPushService;
/**
 * Oppo推送 Service实现类
 * 
 * @author caozj
 * 
 */
@Service
@Transactional
public class OppoPushServiceImpl implements OppoPushService {

	@Autowired
	private OppoPushDao oppoPushDao;

	@Override
	public void add(OppoPush oppoPush) {
		oppoPushDao.add(oppoPush);
	}

	@Override
	public void delete(Long id) {
		oppoPushDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public OppoPush get(Long id) {
		return oppoPushDao.get(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> page(Pager page) {
		return oppoPushDao.page(page);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long count() {
		return oppoPushDao.count();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countBy(String field, Object value){
		return oppoPushDao.countBy(field,value);
	}
	
	@Override
	public void deleteBy(String field, Object value){
		oppoPushDao.deleteBy(field,value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> listAll() {
		return oppoPushDao.listAll();
	}

	@Override
	public void update(OppoPush oppoPush) {
		oppoPushDao.update(oppoPush);
	}
	
	@Override
	public void batchDelete(List<Long> idList) {
		for (Long id : idList) {
			this.delete(id);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public OppoPush getBy(String field, Object value) {
		return oppoPushDao.getBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public OppoPush getByAnd(String field1, Object value1, String field2, Object value2) {
		return oppoPushDao.getByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public OppoPush getByOr(String field1, Object value1, String field2, Object value2) {
		return oppoPushDao.getByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> listBy(String field, Object value) {
		return oppoPushDao.listBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		return oppoPushDao.listByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> listByOr(String field1, Object value1, String field2, Object value2) {
		return oppoPushDao.listByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> pageBy(String field, Object value, Pager page) {
		return oppoPushDao.pageBy(field, value, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		return oppoPushDao.pageByAnd(field1, value1, field2, value2, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<OppoPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		return oppoPushDao.pageByOr(field1, value1, field2, value2, page);
	}
	
	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2){
		oppoPushDao.deleteByAnd(field1,value1,field2,value2);
	}
	
	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2){
		oppoPushDao.deleteByOr(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByAnd(String field1, Object value1, String field2, Object value2){
		return oppoPushDao.countByAnd(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByOr(String field1, Object value1, String field2, Object value2){
		return oppoPushDao.countByOr(field1,value1,field2,value2);
	}
}
