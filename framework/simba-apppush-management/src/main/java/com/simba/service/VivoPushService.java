package  com.simba.service;

import java.util.List;

import com.simba.framework.util.jdbc.Pager;
import com.simba.model.VivoPush;
/**
 *Vivo推送 Service
 * 
 * @author caozj
 * 
 */
public interface VivoPushService {

	void add(VivoPush vivoPush);

	void update(VivoPush vivoPush);

	void delete(Long id);

	List<VivoPush> listAll();
		
	Long count();
	
	Long countBy(String field, Object value);
	
	Long countByAnd(String field1, Object value1, String field2, Object value2);
	
	Long countByOr(String field1, Object value1, String field2, Object value2);
	
	void deleteBy(String field, Object value);
	
	void deleteByAnd(String field1, Object value1, String field2, Object value2);
	
	void deleteByOr(String field1, Object value1, String field2, Object value2);
	
	List<VivoPush> page(Pager page);
	
	VivoPush get(Long id);
	
	void batchDelete(List<Long> idList);

	VivoPush getBy(String field, Object value);

	VivoPush getByAnd(String field1, Object value1, String field2, Object value2);

	VivoPush getByOr(String field1, Object value1, String field2, Object value2);

	List<VivoPush> listBy(String field, Object value);

	List<VivoPush> listByAnd(String field1, Object value1, String field2, Object value2);

	List<VivoPush> listByOr(String field1, Object value1, String field2, Object value2);

	List<VivoPush> pageBy(String field, Object value, Pager page);

	List<VivoPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<VivoPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);

}
