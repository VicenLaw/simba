package com.simba.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.simba.dao.XiaomiPushDao;
import com.simba.framework.util.jdbc.Pager;
import com.simba.model.XiaomiPush;
import com.simba.service.XiaomiPushService;
/**
 * 小米推送 Service实现类
 * 
 * @author caozj
 * 
 */
@Service
@Transactional
public class XiaomiPushServiceImpl implements XiaomiPushService {

	@Autowired
	private XiaomiPushDao xiaomiPushDao;

	@Override
	public void add(XiaomiPush xiaomiPush) {
		xiaomiPushDao.add(xiaomiPush);
	}

	@Override
	public void delete(Long id) {
		xiaomiPushDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public XiaomiPush get(Long id) {
		return xiaomiPushDao.get(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> page(Pager page) {
		return xiaomiPushDao.page(page);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long count() {
		return xiaomiPushDao.count();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countBy(String field, Object value){
		return xiaomiPushDao.countBy(field,value);
	}
	
	@Override
	public void deleteBy(String field, Object value){
		xiaomiPushDao.deleteBy(field,value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> listAll() {
		return xiaomiPushDao.listAll();
	}

	@Override
	public void update(XiaomiPush xiaomiPush) {
		xiaomiPushDao.update(xiaomiPush);
	}
	
	@Override
	public void batchDelete(List<Long> idList) {
		for (Long id : idList) {
			this.delete(id);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public XiaomiPush getBy(String field, Object value) {
		return xiaomiPushDao.getBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public XiaomiPush getByAnd(String field1, Object value1, String field2, Object value2) {
		return xiaomiPushDao.getByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public XiaomiPush getByOr(String field1, Object value1, String field2, Object value2) {
		return xiaomiPushDao.getByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> listBy(String field, Object value) {
		return xiaomiPushDao.listBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		return xiaomiPushDao.listByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> listByOr(String field1, Object value1, String field2, Object value2) {
		return xiaomiPushDao.listByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> pageBy(String field, Object value, Pager page) {
		return xiaomiPushDao.pageBy(field, value, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		return xiaomiPushDao.pageByAnd(field1, value1, field2, value2, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<XiaomiPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		return xiaomiPushDao.pageByOr(field1, value1, field2, value2, page);
	}
	
	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2){
		xiaomiPushDao.deleteByAnd(field1,value1,field2,value2);
	}
	
	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2){
		xiaomiPushDao.deleteByOr(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByAnd(String field1, Object value1, String field2, Object value2){
		return xiaomiPushDao.countByAnd(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByOr(String field1, Object value1, String field2, Object value2){
		return xiaomiPushDao.countByOr(field1,value1,field2,value2);
	}
}
