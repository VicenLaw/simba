package com.simba.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simba.controller.vo.AppPushVo;
import com.simba.dao.AppPushDao;
import com.simba.dao.HuaweiPushDao;
import com.simba.dao.JPushDao;
import com.simba.dao.OppoPushDao;
import com.simba.dao.VivoPushDao;
import com.simba.dao.XiaomiPushDao;
import com.simba.distributed.AppPushServiceClusterData;
import com.simba.distributed.AppPushServiceClusterExecute;
import com.simba.framework.distributed.DistributedUtil;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.framework.util.distributed.ClusterMessage;
import com.simba.framework.util.jdbc.Pager;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.model.AppPush;
import com.simba.model.HuaweiPush;
import com.simba.model.JPush;
import com.simba.model.OppoPush;
import com.simba.model.VivoPush;
import com.simba.model.XiaomiPush;
import com.simba.service.AppPushService;
import com.simba.util.MobileTypePushServiceUtil;
import com.simba.util.UserAppPushServiceUtil;

/**
 * App推送 Service实现类
 * 
 * @author caozj
 * 
 */
@Service
@Transactional
public class AppPushServiceImpl implements AppPushService {

	private static final Log logger = LogFactory.getLog(AppPushServiceImpl.class);

	@Autowired
	private AppPushDao appPushDao;

	@Autowired
	private HuaweiPushDao huaweiPushDao;

	@Autowired
	private JPushDao jPushDao;

	@Autowired
	private OppoPushDao oppoPushDao;

	@Autowired
	private VivoPushDao vivoPushDao;

	@Autowired
	private XiaomiPushDao xiaomiPushDao;

	@Autowired
	private DistributedUtil distributedUtil;

	@Autowired
	private UserAppPushServiceUtil userAppPushServiceUtil;

	@Override
	public void add(AppPush appPush) {
		appPushDao.add(appPush);
	}

	@Override
	public void delete(Long id) {
		appPushDao.delete(id);
		xiaomiPushDao.deleteBy("appPushId", id);
		vivoPushDao.deleteBy("appPushId", id);
		oppoPushDao.deleteBy("appPushId", id);
		jPushDao.deleteBy("appPushId", id);
		huaweiPushDao.deleteBy("appPushId", id);
		distributedUtil.executeInCluster(new ClusterMessage(AppPushServiceClusterExecute.class.getCanonicalName(), new AppPushServiceClusterData("remove", id)));
	}

	@Override
	@Transactional(readOnly = true)
	public AppPush get(Long id) {
		return appPushDao.get(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> page(Pager page) {
		return appPushDao.page(page);
	}

	@Override
	@Transactional(readOnly = true)
	public Long count() {
		return appPushDao.count();
	}

	@Override
	@Transactional(readOnly = true)
	public Long countBy(String field, Object value) {
		return appPushDao.countBy(field, value);
	}

	@Override
	public void deleteBy(String field, Object value) {
		appPushDao.deleteBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> listAll() {
		return appPushDao.listAll();
	}

	@Override
	public void update(AppPush appPush) {
		appPushDao.update(appPush);
	}

	@Override
	public void batchDelete(List<Long> idList) {
		for (Long id : idList) {
			this.delete(id);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public AppPush getBy(String field, Object value) {
		return appPushDao.getBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public AppPush getByAnd(String field1, Object value1, String field2, Object value2) {
		return appPushDao.getByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public AppPush getByOr(String field1, Object value1, String field2, Object value2) {
		return appPushDao.getByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> listBy(String field, Object value) {
		return appPushDao.listBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		return appPushDao.listByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> listByOr(String field1, Object value1, String field2, Object value2) {
		return appPushDao.listByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> pageBy(String field, Object value, Pager page) {
		return appPushDao.pageBy(field, value, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		return appPushDao.pageByAnd(field1, value1, field2, value2, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AppPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		return appPushDao.pageByOr(field1, value1, field2, value2, page);
	}

	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2) {
		appPushDao.deleteByAnd(field1, value1, field2, value2);
	}

	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2) {
		appPushDao.deleteByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public Long countByAnd(String field1, Object value1, String field2, Object value2) {
		return appPushDao.countByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public Long countByOr(String field1, Object value1, String field2, Object value2) {
		return appPushDao.countByOr(field1, value1, field2, value2);
	}

	@Override
	public void update(AppPushVo appPushVo) {
		AppPush appPush = appPushVo.getAppPush();
		appPush.setCreateTime(new Date());
		this.update(appPush);
		huaweiPushDao.update(appPushVo.getHuaweiPush());
		jPushDao.update(appPushVo.getjPush());
		oppoPushDao.update(appPushVo.getOppoPush());
		vivoPushDao.update(appPushVo.getVivoPush());
		xiaomiPushDao.update(appPushVo.getXiaomiPush());
		distributedUtil.executeInCluster(new ClusterMessage(AppPushServiceClusterExecute.class.getCanonicalName(), new AppPushServiceClusterData("update", appPush.getId())));
	}

	@Override
	public void add(AppPushVo appPushVo) {
		AppPush appPush = appPushVo.getAppPush();
		appPush.setCreateTime(new Date());
		this.add(appPush);
		HuaweiPush huaweiPush = appPushVo.getHuaweiPush();
		huaweiPush.setAppPushId(appPush.getId());
		huaweiPushDao.add(huaweiPush);
		JPush jPush = appPushVo.getjPush();
		jPush.setAppPushId(appPush.getId());
		jPushDao.add(jPush);
		OppoPush oppoPush = appPushVo.getOppoPush();
		oppoPush.setAppPushId(appPush.getId());
		oppoPushDao.add(oppoPush);
		VivoPush vivoPush = appPushVo.getVivoPush();
		vivoPush.setAppPushId(appPush.getId());
		vivoPushDao.add(vivoPush);
		XiaomiPush xiaomiPush = appPushVo.getXiaomiPush();
		xiaomiPush.setAppPushId(appPush.getId());
		xiaomiPushDao.add(xiaomiPush);
		distributedUtil.executeInCluster(new ClusterMessage(AppPushServiceClusterExecute.class.getCanonicalName(), new AppPushServiceClusterData("add", appPush.getId())));
	}

	@Override
	public void initAllAppPush() throws Exception {
		List<AppPush> allAppPush = this.listAll();
		List<AppPushServiceInterface> impls = ApplicationContextUtil.getBeansOfType(AppPushServiceInterface.class);
		for (AppPush push : allAppPush) {
			long id = push.getId();
			impls.forEach((AppPushServiceInterface impl) -> {
				impl.init(id);
			});
		}
	}

	@Override
	public void initAppPush(long id) throws Exception {
		List<AppPushServiceInterface> impls = ApplicationContextUtil.getBeansOfType(AppPushServiceInterface.class);
		impls.forEach((AppPushServiceInterface impl) -> {
			impl.init(id);
		});
	}

	@Override
	public void send(long appPushId, String userId, String content) {
		String mobileType = userAppPushServiceUtil.getUserMobileType(appPushId, userId);
		logger.info("用户[appPushId:" + appPushId + "][userId:" + userId + "]的手机类型为" + mobileType);
		AppPushServiceInterface appPushService = MobileTypePushServiceUtil.get(mobileType);
		if (appPushService == null || !appPushService.enable(appPushId)) {
			appPushService = MobileTypePushServiceUtil.getDefault();
		}
		logger.info("用户[appPushId:" + appPushId + "][userId:" + userId + "]的App推送类型为" + appPushService.getMobileType());
		appPushService.send(appPushId, userId, content);
	}

}
