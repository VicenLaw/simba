package com.simba.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.simba.dao.JPushDao;
import com.simba.framework.util.jdbc.Pager;
import com.simba.model.JPush;
import com.simba.service.JPushService;
/**
 * 极光推送 Service实现类
 * 
 * @author caozj
 * 
 */
@Service
@Transactional
public class JPushServiceImpl implements JPushService {

	@Autowired
	private JPushDao jPushDao;

	@Override
	public void add(JPush jPush) {
		jPushDao.add(jPush);
	}

	@Override
	public void delete(Long id) {
		jPushDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public JPush get(Long id) {
		return jPushDao.get(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> page(Pager page) {
		return jPushDao.page(page);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long count() {
		return jPushDao.count();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countBy(String field, Object value){
		return jPushDao.countBy(field,value);
	}
	
	@Override
	public void deleteBy(String field, Object value){
		jPushDao.deleteBy(field,value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> listAll() {
		return jPushDao.listAll();
	}

	@Override
	public void update(JPush jPush) {
		jPushDao.update(jPush);
	}
	
	@Override
	public void batchDelete(List<Long> idList) {
		for (Long id : idList) {
			this.delete(id);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public JPush getBy(String field, Object value) {
		return jPushDao.getBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public JPush getByAnd(String field1, Object value1, String field2, Object value2) {
		return jPushDao.getByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public JPush getByOr(String field1, Object value1, String field2, Object value2) {
		return jPushDao.getByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> listBy(String field, Object value) {
		return jPushDao.listBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		return jPushDao.listByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> listByOr(String field1, Object value1, String field2, Object value2) {
		return jPushDao.listByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> pageBy(String field, Object value, Pager page) {
		return jPushDao.pageBy(field, value, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		return jPushDao.pageByAnd(field1, value1, field2, value2, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<JPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		return jPushDao.pageByOr(field1, value1, field2, value2, page);
	}
	
	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2){
		jPushDao.deleteByAnd(field1,value1,field2,value2);
	}
	
	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2){
		jPushDao.deleteByOr(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByAnd(String field1, Object value1, String field2, Object value2){
		return jPushDao.countByAnd(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByOr(String field1, Object value1, String field2, Object value2){
		return jPushDao.countByOr(field1,value1,field2,value2);
	}
}
