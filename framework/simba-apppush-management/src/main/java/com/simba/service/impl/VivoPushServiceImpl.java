package com.simba.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.simba.dao.VivoPushDao;
import com.simba.framework.util.jdbc.Pager;
import com.simba.model.VivoPush;
import com.simba.service.VivoPushService;
/**
 * Vivo推送 Service实现类
 * 
 * @author caozj
 * 
 */
@Service
@Transactional
public class VivoPushServiceImpl implements VivoPushService {

	@Autowired
	private VivoPushDao vivoPushDao;

	@Override
	public void add(VivoPush vivoPush) {
		vivoPushDao.add(vivoPush);
	}

	@Override
	public void delete(Long id) {
		vivoPushDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public VivoPush get(Long id) {
		return vivoPushDao.get(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> page(Pager page) {
		return vivoPushDao.page(page);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long count() {
		return vivoPushDao.count();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countBy(String field, Object value){
		return vivoPushDao.countBy(field,value);
	}
	
	@Override
	public void deleteBy(String field, Object value){
		vivoPushDao.deleteBy(field,value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> listAll() {
		return vivoPushDao.listAll();
	}

	@Override
	public void update(VivoPush vivoPush) {
		vivoPushDao.update(vivoPush);
	}
	
	@Override
	public void batchDelete(List<Long> idList) {
		for (Long id : idList) {
			this.delete(id);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public VivoPush getBy(String field, Object value) {
		return vivoPushDao.getBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public VivoPush getByAnd(String field1, Object value1, String field2, Object value2) {
		return vivoPushDao.getByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public VivoPush getByOr(String field1, Object value1, String field2, Object value2) {
		return vivoPushDao.getByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> listBy(String field, Object value) {
		return vivoPushDao.listBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		return vivoPushDao.listByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> listByOr(String field1, Object value1, String field2, Object value2) {
		return vivoPushDao.listByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> pageBy(String field, Object value, Pager page) {
		return vivoPushDao.pageBy(field, value, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		return vivoPushDao.pageByAnd(field1, value1, field2, value2, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<VivoPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		return vivoPushDao.pageByOr(field1, value1, field2, value2, page);
	}
	
	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2){
		vivoPushDao.deleteByAnd(field1,value1,field2,value2);
	}
	
	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2){
		vivoPushDao.deleteByOr(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByAnd(String field1, Object value1, String field2, Object value2){
		return vivoPushDao.countByAnd(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByOr(String field1, Object value1, String field2, Object value2){
		return vivoPushDao.countByOr(field1,value1,field2,value2);
	}
}
