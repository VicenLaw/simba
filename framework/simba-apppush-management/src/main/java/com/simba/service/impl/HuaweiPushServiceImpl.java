package com.simba.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.simba.dao.HuaweiPushDao;
import com.simba.framework.util.jdbc.Pager;
import com.simba.model.HuaweiPush;
import com.simba.service.HuaweiPushService;
/**
 * 华为推送 Service实现类
 * 
 * @author caozj
 * 
 */
@Service
@Transactional
public class HuaweiPushServiceImpl implements HuaweiPushService {

	@Autowired
	private HuaweiPushDao huaweiPushDao;

	@Override
	public void add(HuaweiPush huaweiPush) {
		huaweiPushDao.add(huaweiPush);
	}

	@Override
	public void delete(Long id) {
		huaweiPushDao.delete(id);
	}

	@Override
	@Transactional(readOnly = true)
	public HuaweiPush get(Long id) {
		return huaweiPushDao.get(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> page(Pager page) {
		return huaweiPushDao.page(page);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long count() {
		return huaweiPushDao.count();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countBy(String field, Object value){
		return huaweiPushDao.countBy(field,value);
	}
	
	@Override
	public void deleteBy(String field, Object value){
		huaweiPushDao.deleteBy(field,value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> listAll() {
		return huaweiPushDao.listAll();
	}

	@Override
	public void update(HuaweiPush huaweiPush) {
		huaweiPushDao.update(huaweiPush);
	}
	
	@Override
	public void batchDelete(List<Long> idList) {
		for (Long id : idList) {
			this.delete(id);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public HuaweiPush getBy(String field, Object value) {
		return huaweiPushDao.getBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public HuaweiPush getByAnd(String field1, Object value1, String field2, Object value2) {
		return huaweiPushDao.getByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public HuaweiPush getByOr(String field1, Object value1, String field2, Object value2) {
		return huaweiPushDao.getByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> listBy(String field, Object value) {
		return huaweiPushDao.listBy(field, value);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		return huaweiPushDao.listByAnd(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> listByOr(String field1, Object value1, String field2, Object value2) {
		return huaweiPushDao.listByOr(field1, value1, field2, value2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> pageBy(String field, Object value, Pager page) {
		return huaweiPushDao.pageBy(field, value, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		return huaweiPushDao.pageByAnd(field1, value1, field2, value2, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<HuaweiPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		return huaweiPushDao.pageByOr(field1, value1, field2, value2, page);
	}
	
	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2){
		huaweiPushDao.deleteByAnd(field1,value1,field2,value2);
	}
	
	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2){
		huaweiPushDao.deleteByOr(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByAnd(String field1, Object value1, String field2, Object value2){
		return huaweiPushDao.countByAnd(field1,value1,field2,value2);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long countByOr(String field1, Object value1, String field2, Object value2){
		return huaweiPushDao.countByOr(field1,value1,field2,value2);
	}
}
