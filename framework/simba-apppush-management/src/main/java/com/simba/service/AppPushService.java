package com.simba.service;

import java.util.List;

import com.simba.controller.vo.AppPushVo;
import com.simba.framework.util.jdbc.Pager;
import com.simba.model.AppPush;

/**
 * App推送 Service
 * 
 * @author caozj
 * 
 */
public interface AppPushService {

	void add(AppPush appPush);

	void update(AppPush appPush);

	void delete(Long id);

	List<AppPush> listAll();

	Long count();

	Long countBy(String field, Object value);

	Long countByAnd(String field1, Object value1, String field2, Object value2);

	Long countByOr(String field1, Object value1, String field2, Object value2);

	void deleteBy(String field, Object value);

	void deleteByAnd(String field1, Object value1, String field2, Object value2);

	void deleteByOr(String field1, Object value1, String field2, Object value2);

	List<AppPush> page(Pager page);

	AppPush get(Long id);

	void batchDelete(List<Long> idList);

	AppPush getBy(String field, Object value);

	AppPush getByAnd(String field1, Object value1, String field2, Object value2);

	AppPush getByOr(String field1, Object value1, String field2, Object value2);

	List<AppPush> listBy(String field, Object value);

	List<AppPush> listByAnd(String field1, Object value1, String field2, Object value2);

	List<AppPush> listByOr(String field1, Object value1, String field2, Object value2);

	List<AppPush> pageBy(String field, Object value, Pager page);

	List<AppPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<AppPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);

	void update(AppPushVo appPushVo);

	void add(AppPushVo appPushVo);

	/**
	 * 初始化App推送的Bean
	 */
	void initAllAppPush() throws Exception;

	/**
	 * 初始化App推送
	 * 
	 * @param id
	 * @throws Exception
	 */
	void initAppPush(long id) throws Exception;

	/**
	 * 推送App消息给用户
	 * 
	 * @param appPushId
	 * @param userId
	 * @param content
	 */
	void send(long appPushId, String userId, String content);

}
