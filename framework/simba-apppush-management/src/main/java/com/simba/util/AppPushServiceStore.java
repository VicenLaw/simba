package com.simba.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.simba.apppush.huawei.util.HuaWeiAccessTokenUtil;
import com.simba.apppush.huawei.util.HuaWeiAppPushUtil;
import com.simba.apppush.oppo.util.OppoAppPushUtil;
import com.simba.apppush.vivo.util.VivoAccessTokenUtil;
import com.simba.apppush.vivo.util.VivoAppPushUtil;
import com.simba.jpush.app.util.JpushAppUtil;
import com.simba.xiaomi.util.XiaoMiAppPushUtil;

/**
 * App推送服务工具存储类
 * 
 * @author caozhejun
 *
 */
public class AppPushServiceStore {

	private static Map<Long, JpushAppUtil> jpushMap = new HashMap<>();

	private static Map<Long, OppoAppPushUtil> oppoMap = new HashMap<>();

	private static Map<Long, VivoAppPushUtil> vivoMap = new HashMap<>();

	private static Map<Long, XiaoMiAppPushUtil> xiaomiMap = new HashMap<>();

	private static Map<Long, HuaWeiAppPushUtil> huaweiMap = new HashMap<>();

	private static Map<Long, VivoAccessTokenUtil> vivoJobMap = new HashMap<>();

	private static Map<Long, HuaWeiAccessTokenUtil> huaweiJobMap = new HashMap<>();

	public static Collection<VivoAccessTokenUtil> getVivoJobs() {
		return vivoJobMap.values();
	}

	public static Collection<HuaWeiAccessTokenUtil> getHuaweiJobs() {
		return huaweiJobMap.values();
	}

	public static void addHuaweiJob(long id, HuaWeiAccessTokenUtil huaWeiAccessTokenUtil) {
		huaweiJobMap.put(id, huaWeiAccessTokenUtil);
	}

	public static void removeHuaweiJob(long id) {
		huaweiJobMap.remove(id);
	}

	public static void addVivoJob(long id, VivoAccessTokenUtil accessTokenUtil) {
		vivoJobMap.put(id, accessTokenUtil);
	}

	public static void removeVivoJob(long id) {
		vivoJobMap.remove(id);
	}

	public static void addJpush(long id, JpushAppUtil jpush) {
		jpushMap.put(id, jpush);
	}

	public static void removeJpush(long id) {
		jpushMap.remove(id);
	}

	public static JpushAppUtil getJpushAppUtil(long id) {
		return jpushMap.get(id);
	}

	public static void addOppo(long id, OppoAppPushUtil oppo) {
		oppoMap.put(id, oppo);
	}

	public static void removeOppo(long id) {
		oppoMap.remove(id);
	}

	public static OppoAppPushUtil getOppoAppPushUtil(long id) {
		return oppoMap.get(id);
	}

	public static void addVivo(long id, VivoAppPushUtil vivo) {
		vivoMap.put(id, vivo);
	}

	public static void removeVivo(long id) {
		vivoMap.remove(id);
	}

	public static VivoAppPushUtil getVivoAppPushUtil(long id) {
		return vivoMap.get(id);
	}

	public static void addXiaomi(long id, XiaoMiAppPushUtil xiaomi) {
		xiaomiMap.put(id, xiaomi);
	}

	public static void removeXiaomi(long id) {
		xiaomiMap.remove(id);
	}

	public static XiaoMiAppPushUtil getXiaoMiAppPushUtil(long id) {
		return xiaomiMap.get(id);
	}

	public static void addHuawei(long id, HuaWeiAppPushUtil huawei) {
		huaweiMap.put(id, huawei);
	}

	public static void removeHuawei(long id) {
		huaweiMap.remove(id);
	}

	public static HuaWeiAppPushUtil getHuawei(long id) {
		return huaweiMap.get(id);
	}

}
