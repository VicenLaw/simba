package com.simba.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.druid.util.StringUtils;
import com.simba.cache.Redis;

/**
 * 用户App推送工具类
 * 
 * @author caozhejun
 *
 */
@Component
public class UserAppPushServiceUtil {

	@Autowired
	private Redis redisUtil;

	private static final String tokenKey = "Simba:String:AppPushService:Token:";

	private static final String mobileTypeKey = "Simba:String:AppPushService:MobileType:";

	/**
	 * 保存用户发送App推送的Token
	 * 
	 * @param appPushId
	 * @param userId
	 * @param token
	 */
	public void saveUserToken(long appPushId, String userId, String token) {
		if (StringUtils.isEmpty(token)) {
			return;
		}
		String key = tokenKey + appPushId + ":" + userId;
		redisUtil.setString(key, token);
	}

	/**
	 * 获取用户发送App推送的Token
	 * 
	 * @param appPushId
	 * @param userId
	 * @return
	 */
	public String getUserToken(long appPushId, String userId) {
		String key = tokenKey + appPushId + ":" + userId;
		return redisUtil.getString(key);
	}

	/**
	 * 保存用户手机类型
	 * 
	 * @param appPushId
	 * @param userId
	 * @param mobileType
	 */
	public void saveUserMobileType(long appPushId, String userId, String mobileType) {
		String key = mobileTypeKey + appPushId + ":" + userId;
		redisUtil.setString(key, mobileType);
	}

	/**
	 * 获取用户手机类型
	 * 
	 * @param appPushId
	 * @param userId
	 * @return
	 */
	public String getUserMobileType(long appPushId, String userId) {
		String key = mobileTypeKey + appPushId + ":" + userId;
		return redisUtil.getString(key);
	}
}
