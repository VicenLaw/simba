package com.simba.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simba.apppush.util.AppPushConstants;
import com.simba.exception.SimbaException;
import com.simba.interfaces.AppPushServiceInterface;

/**
 * 手机App推送类型工具类
 * 
 * @author caozhejun
 *
 */
public class MobileTypePushServiceUtil {

	private static Map<String, AppPushServiceInterface> appPushMap;

	/**
	 * 初始化
	 * 
	 * @param impls
	 */
	public static void init(List<AppPushServiceInterface> impls) {
		appPushMap = new HashMap<>(impls.size());
		impls.forEach((AppPushServiceInterface impl) -> {
			appPushMap.put(impl.getMobileType(), impl);
		});
	}

	/**
	 * 获取对应手机类型的App推送实现类
	 * 
	 * @param mobileType
	 * @return
	 */
	public static AppPushServiceInterface get(String mobileType) {
		AppPushServiceInterface impl = appPushMap.get(mobileType);
		if (impl == null) {
			impl = appPushMap.get(AppPushConstants.defaultMobileType);
		}
		if (impl == null) {
			throw new SimbaException(mobileType + "没有找到对应的App推送实现类");
		}
		return impl;
	}

	/**
	 * 获取默认的App推送实现类
	 * 
	 * @return
	 */
	public static AppPushServiceInterface getDefault() {
		AppPushServiceInterface impl = appPushMap.get(AppPushConstants.defaultMobileType);
		if (impl == null) {
			throw new SimbaException("没有找到默认的App推送实现类");
		}
		return impl;
	}

}
