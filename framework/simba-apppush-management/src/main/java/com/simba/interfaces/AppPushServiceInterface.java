package com.simba.interfaces;

/**
 * App推送服务接口
 * 
 * @author caozhejun
 *
 */
public interface AppPushServiceInterface {

	/**
	 * 获取处理的手机类型
	 * 
	 * @return
	 */
	String getMobileType();

	/**
	 * 发送手机App推送
	 * 
	 * @param appPushId
	 * @param userId
	 * @param content
	 */
	void send(long appPushId, String userId, String content);

	/**
	 * 是否启用
	 * 
	 * @return
	 */
	boolean enable(long appPushId);

	/**
	 * 服务启动的时候初始化(管理界面新增修改的时候，也会执行)
	 * 
	 * @param appPushId
	 */
	void init(long appPushId);

	/**
	 * 管理界面删除的时候调用
	 * 
	 * @param appPushId
	 */
	void delete(long appPushId);

}
