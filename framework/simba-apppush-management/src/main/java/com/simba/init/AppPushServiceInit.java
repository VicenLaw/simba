package com.simba.init;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.simba.framework.util.applicationcontext.ApplicationContextInit;
import com.simba.framework.util.applicationcontext.ApplicationContextUtil;
import com.simba.interfaces.AppPushServiceInterface;
import com.simba.service.AppPushService;
import com.simba.util.MobileTypePushServiceUtil;

/**
 * 服务启动之后初始化App推送服务
 * 
 * @author caozhejun
 *
 */
@Component
public class AppPushServiceInit implements ApplicationContextInit {

	private static final Log logger = LogFactory.getLog(AppPushServiceInit.class);

	@Autowired
	private AppPushService appPushService;

	@Override
	public void init() {
		try {
			appPushService.initAllAppPush();
		} catch (Exception e) {
			logger.error("初始化App推送服务发生异常", e);
		}
		List<AppPushServiceInterface> impls = ApplicationContextUtil.getBeansOfType(AppPushServiceInterface.class);
		MobileTypePushServiceUtil.init(impls);
	}

	@Override
	public int sort() {
		return 2000;
	}

}
