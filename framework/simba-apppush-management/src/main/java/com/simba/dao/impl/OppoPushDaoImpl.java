package com.simba.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simba.dao.OppoPushDao;
import com.simba.framework.util.jdbc.Jdbc;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.jdbc.StatementParameter;
import com.simba.model.OppoPush;
/**
 * Oppo推送 Dao实现类
 * 
 * @author caozj
 *  
 */
@Repository
public class OppoPushDaoImpl implements OppoPushDao {

	@Autowired
	private Jdbc jdbc;

	private static final String table = "oppoPush";

	@Override
	public void add(OppoPush oppoPush) {
		String sql = "insert into " + table + "( appPushId, appKey, appSecret, title, subTitle) values(?,?,?,?,?)";
		jdbc.updateForBoolean(sql, oppoPush.getAppPushId(),oppoPush.getAppKey(),oppoPush.getAppSecret(),oppoPush.getTitle(),oppoPush.getSubTitle());
	}

	@Override
	public void update(OppoPush oppoPush) {
		String sql = "update " + table + " set  appPushId = ? , appKey = ? , appSecret = ? , title = ? , subTitle = ?  where id = ?  ";
		jdbc.updateForBoolean(sql,oppoPush.getAppPushId(),oppoPush.getAppKey(),oppoPush.getAppSecret(),oppoPush.getTitle(),oppoPush.getSubTitle(), oppoPush.getId());
	}

	@Override
	public void delete(Long id) {
		String sql = "delete from " + table + " where id = ? ";
		jdbc.updateForBoolean(sql, id);
	}

	@Override
	public List<OppoPush> page(Pager page) {
		String sql = "select * from " + table;
		return jdbc.queryForPage(sql, OppoPush.class, page);
	}
	@Override
	public List<OppoPush> listAll(){
		String sql = "select * from " + table;
		return jdbc.queryForList(sql, OppoPush.class);
	}
	
	public Long count(){
		String sql = "select count(*) from " + table;
		return jdbc.queryForLong(sql); 
	}
	
	@Override
	public OppoPush get(Long id) {
		String sql = "select * from " + table + " where id = ? ";
		return jdbc.query(sql, OppoPush.class, id);
	}
	
	@Override
	public OppoPush getBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.query(sql, OppoPush.class, value);
	}

	@Override
	public OppoPush getByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.query(sql, OppoPush.class, value1, value2);
	}

	@Override
	public OppoPush getByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.query(sql, OppoPush.class, value1, value2);
	}

	@Override
	public List<OppoPush> listBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.queryForList(sql, OppoPush.class, value);
	}

	@Override
	public List<OppoPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.queryForList(sql, OppoPush.class, value1, value2);
	}

	@Override
	public List<OppoPush> listByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.queryForList(sql, OppoPush.class, value1, value2);
	}

	@Override
	public List<OppoPush> pageBy(String field, Object value, Pager page) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value);
		return jdbc.queryForPage(sql, OppoPush.class, page, param);
	}

	@Override
	public List<OppoPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, OppoPush.class, page, param);
	}

	@Override
	public List<OppoPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, OppoPush.class, page, param);
	}

	@Override
	public Long countBy(String field, Object value) {
		String sql = "select count(*) from " + table + " where " + field + " = ? ";
		return jdbc.queryForLong(sql, value);
	}
	
	@Override
	public void deleteBy(String field, Object value) {
		String sql = "delete from " + table + " where " + field + " = ? ";
		jdbc.updateForBoolean(sql, value);
	}

	@Override
	public Long countByOr(String field1, Object value1, String field2, Object value2){
		String sql = "select count(*) from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.queryForLong(sql, value1, value2);
	}
	
	@Override
	public Long countByAnd(String field1, Object value1, String field2, Object value2){
		String sql = "select count(*) from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.queryForLong(sql, value1, value2);
	}
	
	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2){
		String sql = "delete from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		jdbc.updateForBoolean(sql, value1, value2);
	}
	
	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2){
		String sql = "delete from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		jdbc.updateForBoolean(sql, value1, value2);
	}
}
