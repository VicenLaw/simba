package com.simba.dao;

import java.util.List;

import com.simba.framework.util.jdbc.Pager;
import com.simba.model.XiaomiPush;
/**
 * 小米推送 Dao
 * 
 * @author caozj
 * 
 */
public interface XiaomiPushDao {

	void add(XiaomiPush xiaomiPush);

	void update(XiaomiPush xiaomiPush);

	void delete(Long id);

	List<XiaomiPush> listAll();
	
	Long count();
	
	Long countBy(String field, Object value);
	
	Long countByAnd(String field1, Object value1, String field2, Object value2);
	
	Long countByOr(String field1, Object value1, String field2, Object value2);
	
	void deleteBy(String field, Object value);
	
	void deleteByAnd(String field1, Object value1, String field2, Object value2);
	
	void deleteByOr(String field1, Object value1, String field2, Object value2);
	
	List<XiaomiPush> page(Pager page);
	
	XiaomiPush get(Long id);
	
	XiaomiPush getBy(String field, Object value);

	XiaomiPush getByAnd(String field1, Object value1, String field2, Object value2);

	XiaomiPush getByOr(String field1, Object value1, String field2, Object value2);

	List<XiaomiPush> listBy(String field, Object value);

	List<XiaomiPush> listByAnd(String field1, Object value1, String field2, Object value2);

	List<XiaomiPush> listByOr(String field1, Object value1, String field2, Object value2);

	List<XiaomiPush> pageBy(String field, Object value, Pager page);

	List<XiaomiPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<XiaomiPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);
	

}
