package com.simba.dao;

import java.util.List;

import com.simba.framework.util.jdbc.Pager;
import com.simba.model.OppoPush;
/**
 * Oppo推送 Dao
 * 
 * @author caozj
 * 
 */
public interface OppoPushDao {

	void add(OppoPush oppoPush);

	void update(OppoPush oppoPush);

	void delete(Long id);

	List<OppoPush> listAll();
	
	Long count();
	
	Long countBy(String field, Object value);
	
	Long countByAnd(String field1, Object value1, String field2, Object value2);
	
	Long countByOr(String field1, Object value1, String field2, Object value2);
	
	void deleteBy(String field, Object value);
	
	void deleteByAnd(String field1, Object value1, String field2, Object value2);
	
	void deleteByOr(String field1, Object value1, String field2, Object value2);
	
	List<OppoPush> page(Pager page);
	
	OppoPush get(Long id);
	
	OppoPush getBy(String field, Object value);

	OppoPush getByAnd(String field1, Object value1, String field2, Object value2);

	OppoPush getByOr(String field1, Object value1, String field2, Object value2);

	List<OppoPush> listBy(String field, Object value);

	List<OppoPush> listByAnd(String field1, Object value1, String field2, Object value2);

	List<OppoPush> listByOr(String field1, Object value1, String field2, Object value2);

	List<OppoPush> pageBy(String field, Object value, Pager page);

	List<OppoPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<OppoPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);
	

}
