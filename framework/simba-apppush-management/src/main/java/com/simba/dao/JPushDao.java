package com.simba.dao;

import java.util.List;

import com.simba.framework.util.jdbc.Pager;
import com.simba.model.JPush;
/**
 * 极光推送 Dao
 * 
 * @author caozj
 * 
 */
public interface JPushDao {

	void add(JPush jPush);

	void update(JPush jPush);

	void delete(Long id);

	List<JPush> listAll();
	
	Long count();
	
	Long countBy(String field, Object value);
	
	Long countByAnd(String field1, Object value1, String field2, Object value2);
	
	Long countByOr(String field1, Object value1, String field2, Object value2);
	
	void deleteBy(String field, Object value);
	
	void deleteByAnd(String field1, Object value1, String field2, Object value2);
	
	void deleteByOr(String field1, Object value1, String field2, Object value2);
	
	List<JPush> page(Pager page);
	
	JPush get(Long id);
	
	JPush getBy(String field, Object value);

	JPush getByAnd(String field1, Object value1, String field2, Object value2);

	JPush getByOr(String field1, Object value1, String field2, Object value2);

	List<JPush> listBy(String field, Object value);

	List<JPush> listByAnd(String field1, Object value1, String field2, Object value2);

	List<JPush> listByOr(String field1, Object value1, String field2, Object value2);

	List<JPush> pageBy(String field, Object value, Pager page);

	List<JPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<JPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);
	

}
