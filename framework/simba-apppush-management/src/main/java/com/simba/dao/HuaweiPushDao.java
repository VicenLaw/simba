package com.simba.dao;

import java.util.List;

import com.simba.framework.util.jdbc.Pager;
import com.simba.model.HuaweiPush;
/**
 * 华为推送 Dao
 * 
 * @author caozj
 * 
 */
public interface HuaweiPushDao {

	void add(HuaweiPush huaweiPush);

	void update(HuaweiPush huaweiPush);

	void delete(Long id);

	List<HuaweiPush> listAll();
	
	Long count();
	
	Long countBy(String field, Object value);
	
	Long countByAnd(String field1, Object value1, String field2, Object value2);
	
	Long countByOr(String field1, Object value1, String field2, Object value2);
	
	void deleteBy(String field, Object value);
	
	void deleteByAnd(String field1, Object value1, String field2, Object value2);
	
	void deleteByOr(String field1, Object value1, String field2, Object value2);
	
	List<HuaweiPush> page(Pager page);
	
	HuaweiPush get(Long id);
	
	HuaweiPush getBy(String field, Object value);

	HuaweiPush getByAnd(String field1, Object value1, String field2, Object value2);

	HuaweiPush getByOr(String field1, Object value1, String field2, Object value2);

	List<HuaweiPush> listBy(String field, Object value);

	List<HuaweiPush> listByAnd(String field1, Object value1, String field2, Object value2);

	List<HuaweiPush> listByOr(String field1, Object value1, String field2, Object value2);

	List<HuaweiPush> pageBy(String field, Object value, Pager page);

	List<HuaweiPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page);

	List<HuaweiPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page);
	

}
