package com.simba.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simba.dao.AppPushDao;
import com.simba.framework.util.jdbc.Jdbc;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.jdbc.StatementParameter;
import com.simba.model.AppPush;

/**
 * App推送 Dao实现类
 * 
 * @author caozj
 * 
 */
@Repository
public class AppPushDaoImpl implements AppPushDao {

	@Autowired
	private Jdbc jdbc;

	private static final String table = "appPush";

	@Override
	public void add(AppPush appPush) {
		String sql = "insert into " + table + "( name, code, createTime) values(?,?,?)";
		Number number = jdbc.updateForGeneratedKey(sql, appPush.getName(), appPush.getCode(), appPush.getCreateTime());
		appPush.setId(number.longValue());
	}

	@Override
	public void update(AppPush appPush) {
		String sql = "update " + table + " set  name = ? , code = ? , createTime = ?  where id = ?  ";
		jdbc.updateForBoolean(sql, appPush.getName(), appPush.getCode(), appPush.getCreateTime(), appPush.getId());
	}

	@Override
	public void delete(Long id) {
		String sql = "delete from " + table + " where id = ? ";
		jdbc.updateForBoolean(sql, id);
	}

	@Override
	public List<AppPush> page(Pager page) {
		String sql = "select * from " + table;
		return jdbc.queryForPage(sql, AppPush.class, page);
	}

	@Override
	public List<AppPush> listAll() {
		String sql = "select * from " + table;
		return jdbc.queryForList(sql, AppPush.class);
	}

	public Long count() {
		String sql = "select count(*) from " + table;
		return jdbc.queryForLong(sql);
	}

	@Override
	public AppPush get(Long id) {
		String sql = "select * from " + table + " where id = ? ";
		return jdbc.query(sql, AppPush.class, id);
	}

	@Override
	public AppPush getBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.query(sql, AppPush.class, value);
	}

	@Override
	public AppPush getByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.query(sql, AppPush.class, value1, value2);
	}

	@Override
	public AppPush getByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.query(sql, AppPush.class, value1, value2);
	}

	@Override
	public List<AppPush> listBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.queryForList(sql, AppPush.class, value);
	}

	@Override
	public List<AppPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.queryForList(sql, AppPush.class, value1, value2);
	}

	@Override
	public List<AppPush> listByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.queryForList(sql, AppPush.class, value1, value2);
	}

	@Override
	public List<AppPush> pageBy(String field, Object value, Pager page) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value);
		return jdbc.queryForPage(sql, AppPush.class, page, param);
	}

	@Override
	public List<AppPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, AppPush.class, page, param);
	}

	@Override
	public List<AppPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, AppPush.class, page, param);
	}

	@Override
	public Long countBy(String field, Object value) {
		String sql = "select count(*) from " + table + " where " + field + " = ? ";
		return jdbc.queryForLong(sql, value);
	}

	@Override
	public void deleteBy(String field, Object value) {
		String sql = "delete from " + table + " where " + field + " = ? ";
		jdbc.updateForBoolean(sql, value);
	}

	@Override
	public Long countByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select count(*) from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.queryForLong(sql, value1, value2);
	}

	@Override
	public Long countByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select count(*) from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.queryForLong(sql, value1, value2);
	}

	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "delete from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		jdbc.updateForBoolean(sql, value1, value2);
	}

	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "delete from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		jdbc.updateForBoolean(sql, value1, value2);
	}
}
