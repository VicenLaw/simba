package com.simba.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simba.dao.VivoPushDao;
import com.simba.framework.util.jdbc.Jdbc;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.jdbc.StatementParameter;
import com.simba.model.VivoPush;

/**
 * Vivo推送 Dao实现类
 * 
 * @author caozj
 * 
 */
@Repository
public class VivoPushDaoImpl implements VivoPushDao {

	@Autowired
	private Jdbc jdbc;

	private static final String table = "vivoPush";

	@Override
	public void add(VivoPush vivoPush) {
		String sql = "insert into " + table + "( appPushId, appId, appKey, appSecret, title) values(?,?,?,?,?)";
		jdbc.updateForBoolean(sql, vivoPush.getAppPushId(), vivoPush.getAppId(), vivoPush.getAppKey(), vivoPush.getAppSecret(), vivoPush.getTitle());
	}

	@Override
	public void update(VivoPush vivoPush) {
		String sql = "update " + table + " set  appPushId = ? , appId = ? , appKey = ? , appSecret = ? , title = ?  where id = ?  ";
		jdbc.updateForBoolean(sql, vivoPush.getAppPushId(), vivoPush.getAppId(), vivoPush.getAppKey(), vivoPush.getAppSecret(), vivoPush.getTitle(), vivoPush.getId());
	}

	@Override
	public void delete(Long id) {
		String sql = "delete from " + table + " where id = ? ";
		jdbc.updateForBoolean(sql, id);
	}

	@Override
	public List<VivoPush> page(Pager page) {
		String sql = "select * from " + table;
		return jdbc.queryForPage(sql, VivoPush.class, page);
	}

	@Override
	public List<VivoPush> listAll() {
		String sql = "select * from " + table;
		return jdbc.queryForList(sql, VivoPush.class);
	}

	public Long count() {
		String sql = "select count(*) from " + table;
		return jdbc.queryForLong(sql);
	}

	@Override
	public VivoPush get(Long id) {
		String sql = "select * from " + table + " where id = ? ";
		return jdbc.query(sql, VivoPush.class, id);
	}

	@Override
	public VivoPush getBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.query(sql, VivoPush.class, value);
	}

	@Override
	public VivoPush getByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.query(sql, VivoPush.class, value1, value2);
	}

	@Override
	public VivoPush getByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.query(sql, VivoPush.class, value1, value2);
	}

	@Override
	public List<VivoPush> listBy(String field, Object value) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		return jdbc.queryForList(sql, VivoPush.class, value);
	}

	@Override
	public List<VivoPush> listByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.queryForList(sql, VivoPush.class, value1, value2);
	}

	@Override
	public List<VivoPush> listByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.queryForList(sql, VivoPush.class, value1, value2);
	}

	@Override
	public List<VivoPush> pageBy(String field, Object value, Pager page) {
		String sql = "select * from " + table + " where " + field + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value);
		return jdbc.queryForPage(sql, VivoPush.class, page, param);
	}

	@Override
	public List<VivoPush> pageByAnd(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, VivoPush.class, page, param);
	}

	@Override
	public List<VivoPush> pageByOr(String field1, Object value1, String field2, Object value2, Pager page) {
		String sql = "select * from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		StatementParameter param = new StatementParameter();
		param.set(value1);
		param.set(value2);
		return jdbc.queryForPage(sql, VivoPush.class, page, param);
	}

	@Override
	public Long countBy(String field, Object value) {
		String sql = "select count(*) from " + table + " where " + field + " = ? ";
		return jdbc.queryForLong(sql, value);
	}

	@Override
	public void deleteBy(String field, Object value) {
		String sql = "delete from " + table + " where " + field + " = ? ";
		jdbc.updateForBoolean(sql, value);
	}

	@Override
	public Long countByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "select count(*) from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		return jdbc.queryForLong(sql, value1, value2);
	}

	@Override
	public Long countByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "select count(*) from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		return jdbc.queryForLong(sql, value1, value2);
	}

	@Override
	public void deleteByAnd(String field1, Object value1, String field2, Object value2) {
		String sql = "delete from " + table + " where " + field1 + " = ? and " + field2 + " = ? ";
		jdbc.updateForBoolean(sql, value1, value2);
	}

	@Override
	public void deleteByOr(String field1, Object value1, String field2, Object value2) {
		String sql = "delete from " + table + " where " + field1 + " = ? or " + field2 + " = ? ";
		jdbc.updateForBoolean(sql, value1, value2);
	}
}
