package com.simba.model;
/***********************************************************************
 * Module:  HuaweiPush.java
 * Author:  caozhejun
 * Purpose: Defines the Class HuaweiPush
 ***********************************************************************/

import com.simba.annotation.DescAnnotation;


/**
 * 华为推送
 * */
@DescAnnotation(desc = "华为推送")
public class HuaweiPush {
   /** */
   @DescAnnotation(desc = "")
	private long id;

   /**
    * 应用推送id
    * */
   @DescAnnotation(desc = "应用推送id")
	private long appPushId;

   /**
    * 华为应用id
    * */
   @DescAnnotation(desc = "华为应用id")
	private String appId;

   /**
    * 华为应用秘钥
    * */
   @DescAnnotation(desc = "华为应用秘钥")
	private String appSecret;

   /**
    * 通知标题
    * */
   @DescAnnotation(desc = "通知标题")
	private String title;

   /**
    * 应用包名
    * */
   @DescAnnotation(desc = "应用包名")
	private String appPackage;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAppPushId() {
        return appPushId;
    }

    public void setAppPushId(long appPushId) {
        this.appPushId = appPushId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    @Override
    public String toString() {
        return "HuaweiPush{" +
		"id=" + id +
		", appPushId=" + appPushId + 
		", appId='" + appId + '\'' + 
		", appSecret='" + appSecret + '\'' + 
		", title='" + title + '\'' + 
		", appPackage='" + appPackage + '\'' + 
		'}';
    }

}