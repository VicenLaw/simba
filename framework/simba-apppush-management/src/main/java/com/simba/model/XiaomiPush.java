package com.simba.model;
/***********************************************************************
 * Module:  XiaomiPush.java
 * Author:  caozhejun
 * Purpose: Defines the Class XiaomiPush
 ***********************************************************************/

import com.simba.annotation.DescAnnotation;


/**
 * 小米推送
 * */
@DescAnnotation(desc = "小米推送")
public class XiaomiPush {
   /** */
   @DescAnnotation(desc = "")
	private long id;

   /**
    * 应用推送id
    * */
   @DescAnnotation(desc = "应用推送id")
	private long appPushId;

   /**
    * 小米秘钥
    * */
   @DescAnnotation(desc = "小米秘钥")
	private String appSecret;

   /**
    * 通知标题
    * */
   @DescAnnotation(desc = "通知标题")
	private String title;

   /**
    * 应用包名
    * */
   @DescAnnotation(desc = "应用包名")
	private String appPackage;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAppPushId() {
        return appPushId;
    }

    public void setAppPushId(long appPushId) {
        this.appPushId = appPushId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    @Override
    public String toString() {
        return "XiaomiPush{" +
		"id=" + id +
		", appPushId=" + appPushId + 
		", appSecret='" + appSecret + '\'' + 
		", title='" + title + '\'' + 
		", appPackage='" + appPackage + '\'' + 
		'}';
    }

}