package com.simba.model;
/***********************************************************************
 * Module:  JPush.java
 * Author:  caozhejun
 * Purpose: Defines the Class JPush
 ***********************************************************************/

import com.simba.annotation.DescAnnotation;

/**
 * 极光推送
 */
@DescAnnotation(desc = "极光推送")
public class JPush {
	/** */
	@DescAnnotation(desc = "")
	private long id;

	/**
	 * 应用推送id
	 */
	@DescAnnotation(desc = "应用推送id")
	private long appPushId;

	/**
	 * 极光推送Key
	 */
	@DescAnnotation(desc = "极光推送Key")
	private String appKey;

	/**
	 * 极光推送Secret
	 */
	@DescAnnotation(desc = "极光推送Secret")
	private String appSecret;

	/**
	 * 是否是ios的生产环境
	 */
	private int appProduct;

	public int getAppProduct() {
		return appProduct;
	}

	public void setAppProduct(int appProduct) {
		this.appProduct = appProduct;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAppPushId() {
		return appPushId;
	}

	public void setAppPushId(long appPushId) {
		this.appPushId = appPushId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JPush [id=");
		builder.append(id);
		builder.append(", appPushId=");
		builder.append(appPushId);
		builder.append(", appKey=");
		builder.append(appKey);
		builder.append(", appSecret=");
		builder.append(appSecret);
		builder.append(", appProduct=");
		builder.append(appProduct);
		builder.append("]");
		return builder.toString();
	}

}