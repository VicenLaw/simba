package com.simba.model;
/***********************************************************************
 * Module:  AppPush.java
 * Author:  caozhejun
 * Purpose: Defines the Class AppPush
 ***********************************************************************/

import com.simba.annotation.DescAnnotation;
import java.util.*;

/**
 * App推送
 * */
@DescAnnotation(desc = "App推送")
public class AppPush {
   /** */
   @DescAnnotation(desc = "")
	private long id;

   /**
    * 应用名称
    * */
   @DescAnnotation(desc = "应用名称")
	private String name;

   /**
    * 应用编号
    * */
   @DescAnnotation(desc = "应用编号")
	private String code;

   /**
    * 创建时间
    * */
   @DescAnnotation(desc = "创建时间")
	private Date createTime;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "AppPush{" +
		"id=" + id +
		", name='" + name + '\'' + 
		", code='" + code + '\'' + 
		", createTime=" + createTime + 
		'}';
    }

}