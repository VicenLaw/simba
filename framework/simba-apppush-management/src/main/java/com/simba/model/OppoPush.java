package com.simba.model;
/***********************************************************************
 * Module:  OppoPush.java
 * Author:  caozhejun
 * Purpose: Defines the Class OppoPush
 ***********************************************************************/

import com.simba.annotation.DescAnnotation;


/**
 * Oppo推送
 * */
@DescAnnotation(desc = "Oppo推送")
public class OppoPush {
   /** */
   @DescAnnotation(desc = "")
	private long id;

   /**
    * 应用推送id
    * */
   @DescAnnotation(desc = "应用推送id")
	private long appPushId;

   /**
    * oppo应用key
    * */
   @DescAnnotation(desc = "oppo应用key")
	private String appKey;

   /**
    * oppo应用秘钥
    * */
   @DescAnnotation(desc = "oppo应用秘钥")
	private String appSecret;

   /**
    * 通知标题
    * */
   @DescAnnotation(desc = "通知标题")
	private String title;

   /**
    * 通知子标题
    * */
   @DescAnnotation(desc = "通知子标题")
	private String subTitle;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAppPushId() {
        return appPushId;
    }

    public void setAppPushId(long appPushId) {
        this.appPushId = appPushId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    @Override
    public String toString() {
        return "OppoPush{" +
		"id=" + id +
		", appPushId=" + appPushId + 
		", appKey='" + appKey + '\'' + 
		", appSecret='" + appSecret + '\'' + 
		", title='" + title + '\'' + 
		", subTitle='" + subTitle + '\'' + 
		'}';
    }

}