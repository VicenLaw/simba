package com.simba.model;
/***********************************************************************
 * Module:  VivoPush.java
 * Author:  caozhejun
 * Purpose: Defines the Class VivoPush
 ***********************************************************************/

import com.simba.annotation.DescAnnotation;


/**
 * Vivo推送
 * */
@DescAnnotation(desc = "Vivo推送")
public class VivoPush {
   /** */
   @DescAnnotation(desc = "")
	private long id;

   /**
    * 应用推送id
    * */
   @DescAnnotation(desc = "应用推送id")
	private long appPushId;

   /**
    * vivo应用id
    * */
   @DescAnnotation(desc = "vivo应用id")
	private String appId;

   /**
    * vivo应用key
    * */
   @DescAnnotation(desc = "vivo应用key")
	private String appKey;

   /**
    * vivo引用秘钥
    * */
   @DescAnnotation(desc = "vivo引用秘钥")
	private String appSecret;

   /**
    * 通知标题
    * */
   @DescAnnotation(desc = "通知标题")
	private String title;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAppPushId() {
        return appPushId;
    }

    public void setAppPushId(long appPushId) {
        this.appPushId = appPushId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "VivoPush{" +
		"id=" + id +
		", appPushId=" + appPushId + 
		", appId='" + appId + '\'' + 
		", appKey='" + appKey + '\'' + 
		", appSecret='" + appSecret + '\'' + 
		", title='" + title + '\'' + 
		'}';
    }

}