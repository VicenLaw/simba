package com.simba.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.model.HuaweiPush;
import com.simba.service.HuaweiPushService;
/**
 * 华为推送控制器
 * 
 * @author caozj
 * 
 */
@Controller
@RequestMapping("/huaweiPush")
public class HuaweiPushController {

	@Autowired
	private HuaweiPushService huaweiPushService;

	@RequestMapping("/list")
	public String list() {
		return "huaweiPush/list";
	}
	
	@RequestMapping("/getList")
	public String getList(Pager pager,ModelMap model){
		List<HuaweiPush> list = huaweiPushService.page(pager);
		model.put("list", list);
		return "huaweiPush/table";
	}
	
	@ResponseBody
	@RequestMapping("/count")
	public JsonResult count() {
		Long count = huaweiPushService.count();
		return new JsonResult(count, "", 200);
	}
	
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "huaweiPush/add";
	}

	@RequestMapping("/add")
	public String add(HuaweiPush huaweiPush) {
		huaweiPushService.add(huaweiPush);
		return "redirect:/huaweiPush/list";
	}

	@RequestMapping("/toUpdate")
	public String toUpdate(Long id, ModelMap model) {
		HuaweiPush huaweiPush = huaweiPushService.get(id);
		model.put("huaweiPush", huaweiPush);
		return "huaweiPush/update";
	}

	@RequestMapping("/update")
	public String update(HuaweiPush huaweiPush) {
		huaweiPushService.update(huaweiPush);
		return "redirect:/huaweiPush/list";
	}

	@ResponseBody
	@RequestMapping("/delete")
	public JsonResult delete(Long id, ModelMap model) {
		huaweiPushService.delete(id);
		return new JsonResult();
	}

	@ResponseBody
	@RequestMapping("/batchDelete")
	public JsonResult batchDelete(Long[] id, ModelMap model) {
		huaweiPushService.batchDelete(Arrays.asList(id));
		return new JsonResult();
	}

	

}
