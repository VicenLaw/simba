package com.simba.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.model.OppoPush;
import com.simba.service.OppoPushService;
/**
 * Oppo推送控制器
 * 
 * @author caozj
 * 
 */
@Controller
@RequestMapping("/oppoPush")
public class OppoPushController {

	@Autowired
	private OppoPushService oppoPushService;

	@RequestMapping("/list")
	public String list() {
		return "oppoPush/list";
	}
	
	@RequestMapping("/getList")
	public String getList(Pager pager,ModelMap model){
		List<OppoPush> list = oppoPushService.page(pager);
		model.put("list", list);
		return "oppoPush/table";
	}
	
	@ResponseBody
	@RequestMapping("/count")
	public JsonResult count() {
		Long count = oppoPushService.count();
		return new JsonResult(count, "", 200);
	}
	
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "oppoPush/add";
	}

	@RequestMapping("/add")
	public String add(OppoPush oppoPush) {
		oppoPushService.add(oppoPush);
		return "redirect:/oppoPush/list";
	}

	@RequestMapping("/toUpdate")
	public String toUpdate(Long id, ModelMap model) {
		OppoPush oppoPush = oppoPushService.get(id);
		model.put("oppoPush", oppoPush);
		return "oppoPush/update";
	}

	@RequestMapping("/update")
	public String update(OppoPush oppoPush) {
		oppoPushService.update(oppoPush);
		return "redirect:/oppoPush/list";
	}

	@ResponseBody
	@RequestMapping("/delete")
	public JsonResult delete(Long id, ModelMap model) {
		oppoPushService.delete(id);
		return new JsonResult();
	}

	@ResponseBody
	@RequestMapping("/batchDelete")
	public JsonResult batchDelete(Long[] id, ModelMap model) {
		oppoPushService.batchDelete(Arrays.asList(id));
		return new JsonResult();
	}

	

}
