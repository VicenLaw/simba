package com.simba.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.model.XiaomiPush;
import com.simba.service.XiaomiPushService;
/**
 * 小米推送控制器
 * 
 * @author caozj
 * 
 */
@Controller
@RequestMapping("/xiaomiPush")
public class XiaomiPushController {

	@Autowired
	private XiaomiPushService xiaomiPushService;

	@RequestMapping("/list")
	public String list() {
		return "xiaomiPush/list";
	}
	
	@RequestMapping("/getList")
	public String getList(Pager pager,ModelMap model){
		List<XiaomiPush> list = xiaomiPushService.page(pager);
		model.put("list", list);
		return "xiaomiPush/table";
	}
	
	@ResponseBody
	@RequestMapping("/count")
	public JsonResult count() {
		Long count = xiaomiPushService.count();
		return new JsonResult(count, "", 200);
	}
	
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "xiaomiPush/add";
	}

	@RequestMapping("/add")
	public String add(XiaomiPush xiaomiPush) {
		xiaomiPushService.add(xiaomiPush);
		return "redirect:/xiaomiPush/list";
	}

	@RequestMapping("/toUpdate")
	public String toUpdate(Long id, ModelMap model) {
		XiaomiPush xiaomiPush = xiaomiPushService.get(id);
		model.put("xiaomiPush", xiaomiPush);
		return "xiaomiPush/update";
	}

	@RequestMapping("/update")
	public String update(XiaomiPush xiaomiPush) {
		xiaomiPushService.update(xiaomiPush);
		return "redirect:/xiaomiPush/list";
	}

	@ResponseBody
	@RequestMapping("/delete")
	public JsonResult delete(Long id, ModelMap model) {
		xiaomiPushService.delete(id);
		return new JsonResult();
	}

	@ResponseBody
	@RequestMapping("/batchDelete")
	public JsonResult batchDelete(Long[] id, ModelMap model) {
		xiaomiPushService.batchDelete(Arrays.asList(id));
		return new JsonResult();
	}

	

}
