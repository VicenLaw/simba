package com.simba.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.model.JPush;
import com.simba.service.JPushService;
/**
 * 极光推送控制器
 * 
 * @author caozj
 * 
 */
@Controller
@RequestMapping("/jPush")
public class JPushController {

	@Autowired
	private JPushService jPushService;

	@RequestMapping("/list")
	public String list() {
		return "jPush/list";
	}
	
	@RequestMapping("/getList")
	public String getList(Pager pager,ModelMap model){
		List<JPush> list = jPushService.page(pager);
		model.put("list", list);
		return "jPush/table";
	}
	
	@ResponseBody
	@RequestMapping("/count")
	public JsonResult count() {
		Long count = jPushService.count();
		return new JsonResult(count, "", 200);
	}
	
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "jPush/add";
	}

	@RequestMapping("/add")
	public String add(JPush jPush) {
		jPushService.add(jPush);
		return "redirect:/jPush/list";
	}

	@RequestMapping("/toUpdate")
	public String toUpdate(Long id, ModelMap model) {
		JPush jPush = jPushService.get(id);
		model.put("jPush", jPush);
		return "jPush/update";
	}

	@RequestMapping("/update")
	public String update(JPush jPush) {
		jPushService.update(jPush);
		return "redirect:/jPush/list";
	}

	@ResponseBody
	@RequestMapping("/delete")
	public JsonResult delete(Long id, ModelMap model) {
		jPushService.delete(id);
		return new JsonResult();
	}

	@ResponseBody
	@RequestMapping("/batchDelete")
	public JsonResult batchDelete(Long[] id, ModelMap model) {
		jPushService.batchDelete(Arrays.asList(id));
		return new JsonResult();
	}

	

}
