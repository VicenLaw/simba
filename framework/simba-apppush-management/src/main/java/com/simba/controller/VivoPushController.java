package com.simba.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.model.VivoPush;
import com.simba.service.VivoPushService;
/**
 * Vivo推送控制器
 * 
 * @author caozj
 * 
 */
@Controller
@RequestMapping("/vivoPush")
public class VivoPushController {

	@Autowired
	private VivoPushService vivoPushService;

	@RequestMapping("/list")
	public String list() {
		return "vivoPush/list";
	}
	
	@RequestMapping("/getList")
	public String getList(Pager pager,ModelMap model){
		List<VivoPush> list = vivoPushService.page(pager);
		model.put("list", list);
		return "vivoPush/table";
	}
	
	@ResponseBody
	@RequestMapping("/count")
	public JsonResult count() {
		Long count = vivoPushService.count();
		return new JsonResult(count, "", 200);
	}
	
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "vivoPush/add";
	}

	@RequestMapping("/add")
	public String add(VivoPush vivoPush) {
		vivoPushService.add(vivoPush);
		return "redirect:/vivoPush/list";
	}

	@RequestMapping("/toUpdate")
	public String toUpdate(Long id, ModelMap model) {
		VivoPush vivoPush = vivoPushService.get(id);
		model.put("vivoPush", vivoPush);
		return "vivoPush/update";
	}

	@RequestMapping("/update")
	public String update(VivoPush vivoPush) {
		vivoPushService.update(vivoPush);
		return "redirect:/vivoPush/list";
	}

	@ResponseBody
	@RequestMapping("/delete")
	public JsonResult delete(Long id, ModelMap model) {
		vivoPushService.delete(id);
		return new JsonResult();
	}

	@ResponseBody
	@RequestMapping("/batchDelete")
	public JsonResult batchDelete(Long[] id, ModelMap model) {
		vivoPushService.batchDelete(Arrays.asList(id));
		return new JsonResult();
	}

	

}
