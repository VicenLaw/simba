package com.simba.controller.server.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simba.framework.util.json.JsonResult;
import com.simba.model.AppPush;
import com.simba.service.AppPushService;
import com.simba.util.UserAppPushServiceUtil;

/**
 * App推送服务Controller
 * 
 * @author caozhejun
 *
 */
@RestController
@RequestMapping("/server/api/appPush")
public class AppPushServerApiController {

	private static final Log logger = LogFactory.getLog(AppPushServerApiController.class);

	@Autowired
	private AppPushService appPushService;

	@Autowired
	private UserAppPushServiceUtil userAppPushServiceUtil;

	/**
	 * 推送App消息给用户
	 * 
	 * @param appPushId
	 *            应用id
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送内容
	 * @return
	 */
	@RequestMapping("/sendById")
	public JsonResult send(long appPushId, String userId, String content) {
		logger.info("接收到推送任务:[appPushId:" + appPushId + "][userId:" + userId + "][content:" + content + "]");
		appPushService.send(appPushId, userId, content);
		return new JsonResult();
	}

	/**
	 * 推送App消息给用户
	 * 
	 * @param appPushCode
	 *            应用编码
	 * @param userId
	 *            用户id
	 * @param content
	 *            推送内容
	 * @return
	 */
	@RequestMapping("/sendByCode")
	public JsonResult send(String appPushCode, String userId, String content) {
		logger.info("接收到推送任务:[appPushCode:" + appPushCode + "][userId:" + userId + "][content:" + content + "]");
		AppPush push = appPushService.getBy("code", appPushCode);
		return this.send(push.getId(), userId, content);
	}

	/**
	 * 标记用户手机类型和token
	 * 
	 * @param appPushId
	 *            应用id
	 * @param userId
	 *            用户id
	 * @param token
	 *            用户手机推送token
	 * @param mobileType
	 *            用户手机类型
	 * @return
	 */
	@PostMapping("/flagById")
	public JsonResult flag(long appPushId, String userId, String token, String mobileType) {
		logger.info("接收到标记用户手机类型和token任务:[appPushId:" + appPushId + "][userId:" + userId + "][mobileType:" + mobileType + "][token:" + token + "]");
		userAppPushServiceUtil.saveUserMobileType(appPushId, userId, mobileType);
		userAppPushServiceUtil.saveUserToken(appPushId, userId, token);
		return new JsonResult();
	}

	/**
	 * 标记用户手机类型和token
	 * 
	 * @param appPushCode
	 *            应用编码
	 * @param userId
	 *            用户id
	 * @param token
	 *            用户手机推送token
	 * @param mobileType
	 *            用户手机类型
	 * @return
	 */
	@PostMapping("/flagByCode")
	public JsonResult flag(String appPushCode, String userId, String token, String mobileType) {
		logger.info("接收到标记用户手机类型和token任务:[appPushCode:" + appPushCode + "][userId:" + userId + "][mobileType:" + mobileType + "][token:" + token + "]");
		AppPush push = appPushService.getBy("code", appPushCode);
		return this.flag(push.getId(), userId, token, mobileType);
	}

}
