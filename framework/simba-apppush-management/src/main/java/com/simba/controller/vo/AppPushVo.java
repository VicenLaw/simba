package com.simba.controller.vo;

import com.simba.model.AppPush;
import com.simba.model.HuaweiPush;
import com.simba.model.JPush;
import com.simba.model.OppoPush;
import com.simba.model.VivoPush;
import com.simba.model.XiaomiPush;

/**
 * App推送页面视图
 * 
 * @author caozhejun
 *
 */
public class AppPushVo {

	private AppPush appPush;

	private HuaweiPush huaweiPush;

	private JPush jPush;

	private OppoPush oppoPush;

	private VivoPush vivoPush;

	private XiaomiPush xiaomiPush;

	public AppPush getAppPush() {
		return appPush;
	}

	public void setAppPush(AppPush appPush) {
		this.appPush = appPush;
	}

	public HuaweiPush getHuaweiPush() {
		return huaweiPush;
	}

	public void setHuaweiPush(HuaweiPush huaweiPush) {
		this.huaweiPush = huaweiPush;
	}

	public JPush getjPush() {
		return jPush;
	}

	public void setjPush(JPush jPush) {
		this.jPush = jPush;
	}

	public OppoPush getOppoPush() {
		return oppoPush;
	}

	public void setOppoPush(OppoPush oppoPush) {
		this.oppoPush = oppoPush;
	}

	public VivoPush getVivoPush() {
		return vivoPush;
	}

	public void setVivoPush(VivoPush vivoPush) {
		this.vivoPush = vivoPush;
	}

	public XiaomiPush getXiaomiPush() {
		return xiaomiPush;
	}

	public void setXiaomiPush(XiaomiPush xiaomiPush) {
		this.xiaomiPush = xiaomiPush;
	}

}
