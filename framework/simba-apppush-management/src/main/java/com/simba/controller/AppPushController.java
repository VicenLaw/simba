package com.simba.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simba.controller.vo.AppPushVo;
import com.simba.framework.util.jdbc.Pager;
import com.simba.framework.util.json.JsonResult;
import com.simba.model.AppPush;
import com.simba.service.AppPushService;
import com.simba.service.HuaweiPushService;
import com.simba.service.JPushService;
import com.simba.service.OppoPushService;
import com.simba.service.VivoPushService;
import com.simba.service.XiaomiPushService;

/**
 * App推送控制器
 * 
 * @author caozj
 * 
 */
@Controller
@RequestMapping("/appPush")
public class AppPushController {

	@Autowired
	private AppPushService appPushService;

	@Autowired
	private HuaweiPushService huaweiPushService;

	@Autowired
	private JPushService jPushService;

	@Autowired
	private OppoPushService oppoPushService;

	@Autowired
	private VivoPushService vivoPushService;

	@Autowired
	private XiaomiPushService xiaomiPushService;

	@RequestMapping("/list")
	public String list() {
		return "appPush/list";
	}

	@RequestMapping("/getList")
	public String getList(Pager pager, ModelMap model) {
		List<AppPush> list = appPushService.page(pager);
		List<AppPushVo> voList = new ArrayList<>(list.size());
		list.forEach((AppPush push) -> {
			AppPushVo vo = new AppPushVo();
			vo.setAppPush(push);
			long id = push.getId();
			vo.setHuaweiPush(huaweiPushService.getBy("appPushId", id));
			vo.setjPush(jPushService.getBy("appPushId", id));
			vo.setOppoPush(oppoPushService.getBy("appPushId", id));
			vo.setVivoPush(vivoPushService.getBy("appPushId", id));
			vo.setXiaomiPush(xiaomiPushService.getBy("appPushId", id));
			voList.add(vo);
		});
		model.put("list", voList);
		return "appPush/table";
	}

	@ResponseBody
	@RequestMapping("/count")
	public JsonResult count() {
		Long count = appPushService.count();
		return new JsonResult(count, "", 200);
	}

	@RequestMapping("/toAdd")
	public String toAdd() {
		return "appPush/add";
	}

	@RequestMapping("/add")
	public String add(AppPushVo AppPushVo) {
		appPushService.add(AppPushVo);
		return "redirect:/appPush/list";
	}

	@RequestMapping("/toUpdate")
	public String toUpdate(Long id, ModelMap model) {
		AppPushVo vo = new AppPushVo();
		AppPush appPush = appPushService.get(id);
		vo.setAppPush(appPush);
		vo.setHuaweiPush(huaweiPushService.getBy("appPushId", id));
		vo.setjPush(jPushService.getBy("appPushId", id));
		vo.setOppoPush(oppoPushService.getBy("appPushId", id));
		vo.setVivoPush(vivoPushService.getBy("appPushId", id));
		vo.setXiaomiPush(xiaomiPushService.getBy("appPushId", id));
		model.put("vo", vo);
		return "appPush/update";
	}

	@RequestMapping("/update")
	public String update(AppPushVo AppPushVo) {
		appPushService.update(AppPushVo);
		return "redirect:/appPush/list";
	}

	@ResponseBody
	@RequestMapping("/delete")
	public JsonResult delete(Long id, ModelMap model) {
		appPushService.delete(id);
		return new JsonResult();
	}

	@ResponseBody
	@RequestMapping("/batchDelete")
	public JsonResult batchDelete(Long[] id, ModelMap model) {
		appPushService.batchDelete(Arrays.asList(id));
		return new JsonResult();
	}

}
